'use strict'
import React , {Component} from 'react';
import{
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Navigator,
  Image,
  TouchableOpacity,
  Dimensions,
  ListView,
  Alert,
  Picker,
  InteractionManager,
  ActivityIndicator,
  UIManager,
  LayoutAnimation,
  Animated,
} from 'react-native';
import Toast from 'react-native-simple-toast';
var {height, width} = Dimensions.get('window');
const height = height-55;

import Icon from 'react-native-vector-icons/Foundation';
import Icon2 from 'react-native-vector-icons/Entypo';
import Icon3 from 'react-native-vector-icons/FontAwesome';
import Icon4 from 'react-native-vector-icons/MaterialIcons';
import Icon5 from 'react-native-vector-icons/EvilIcons';
import Icon6 from 'react-native-vector-icons/Octicons';
import districtData from './getDistrict.js';
var  Immutable = require('immutable');
var DATA=[
  {
   location:'Tseung Kwan O',
   salary:'$9999',
   sex:'男',
   duration:'1.5hours',
   coursePerWeek:'1',
   event:'basketball',
   age:'20',
   requirement:'有相關經驗，教練牌'
 },
 {
  location:'Tung Tyng',
  salary:'$200',
  sex:'女',
  duration:'3hours',
  coursePerWeek:'3',
  event:'乳無球',
  age:'33',
  requirement:'有相關經驗,教練牌,女導師,asfasf'
},
{
 location:'pk ja',
 salary:'$9999',
 sex:'女',
 duration:'3hours',
 coursePerWeek:'3',
 event:'football',
 age:'55',
 requirement:'有相關經驗，教練牌,女導師'
},
];



var styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:"#ededed",
    marginTop:55,
  },
  titleContainer:{
    height:25,

  },
  listviewContainer:{
    flex:1,
    borderWidth:1,

  },
  oneViewContainer:{
    width:width,
    alignItems:'center',
    marginTop:10,
  },
  caseTitleContainer:{
    width:width*0.9,
    backgroundColor:'#e89425',
    flexDirection:'row',
    justifyContent:'space-between',
    paddingLeft:5,
    paddingRight:5,
    borderTopLeftRadius:3,
    borderTopRightRadius:3,

  },
  locationStyle:{
    height:height*0.5/7*0.8,
    width:width*0.3,
  },
  textBoxContainer:{
      width:width*0.9,
      borderBottomWidth:1/2,
      flexDirection:'row',
      alignItems:"center",
      padding:5,
      paddingLeft:10,
      paddingRight:10,
      backgroundColor:'#fff',
      borderColor:"#d4d3d3",
  },
  imageStyle:{
    height:height*0.5/7,
    width:height*0.5/7,
  },
  textStyle:{
    flex:1,
    textAlign:'center',
    fontSize:15,
  },
  applyButtonContainer:{
    height:height*0.5/7,
    width:width*0.9,
    flexDirection:'row',
    justifyContent:'center',
    alignItems:"center",
    backgroundColor:'#e89425',
    borderBottomLeftRadius:3,
    borderBottomRightRadius:3,

},
applyButton:{
  flex:1,
  flexDirection:"row",
  alignItems:'center',
  justifyContent:"center",

},
applyButtonImageStyle:{
  height:height*0.5/7,
  width:height*0.5/7,
},
descriptionBoxContainer:{
    height:height*0.5/7*2,
    width:width*0.9,

    flexDirection:'row',
    alignItems:"center",
    backgroundColor:'#fff',

},
descriptionImageStyle:{
  height:height*0.5/7*2,
  width:height*0.5/7,
},
selectionContainer:{
  height:height*0.14,
  paddingRight:5,
  paddingBottom:5,
},
selectionBar:{
  flex:1,
  flexDirection:'row',
  alignItems:'center',
  paddingTop:5,
},
picker:{
  flex:1,
},
pickerContainer:{
  flex:1,
  borderWidth:1/2,
  paddingTop:-10,
  borderRadius:5,
  marginLeft:5,
  borderColor:"#9c9c9c",
  backgroundColor:"#fff",

},
searchButton:{
  backgroundColor:"#fe4f21",
  padding:5,
  paddingLeft:7,
  justifyContent:"center",
  alignItems:"center",
  marginLeft:5,
  borderRadius:6,
},
})

var getCategory = [];
var getDistrict = districtData;
var isCategoryLoaded = false;
var isCasesDataLoaded = false;

var toQueryString = (obj)=> {
    return obj ? Object.keys(obj).sort().map(function (key) {
        var val = obj[key];
        if (Array.isArray(val)) {
            return val.sort().map(function (val2) {
                return key + '=' + val2;
            }).join('&');
        }
        return key + '=' + val;
    }).join('&') : '';
}




export default class CasesListPage extends Component{
  constructor(porps){
    super(porps);

    this.state={
      param:this.props.param,
      firstLoading:true,
    };

  }



  shouldComponentUpdate(nextProps,nextState){
    //console.log(nextState);
    if(this.state.firstLoading){
      return true;
    }
    else if(nextState.param){
      if(getCategory.length < 1 && SelectionBar)
        InteractionManager.runAfterInteractions(() => {
          this.refs.SelectionBar.reGetCategory();
        });
      if(this.refs.CasesList)
        InteractionManager.runAfterInteractions(() => {
          if(this.refs.CasesList.refs.ListView)
            this.refs.CasesList.refs.ListView.scrollTo({y: 0});
          this.refs.CasesList.getCasesData(nextState.param);
        });
    }
    return false;
  }
  componentDidMount(){
    InteractionManager.runAfterInteractions(() => {
      getCategory = (() => {
        var request = new XMLHttpRequest();
        var result = [];
        request.onreadystatechange = (e) =>{
          if (request.readyState !== 4) {
            return;
          }
          if(request.status === 200){
            result.push.apply(result,JSON.parse(request.responseText));
            //console.log(JSON.parse(request.responseText));
            isCategoryLoaded = true;
            getCategory = result;
            LayoutAnimation.configureNext(this.CustomLayoutLinear);
            this.setState({firstLoading:!isCategoryLoaded});
          }
        }
        request.open('POST', 'http://54.179.154.17/getCategory.php', true);
        request.send();
        return result;
      })();
    });
  }
  CustomLayoutLinear = {
    duration: 150,
    create: {
      type: LayoutAnimation.Types.linear,
      property: LayoutAnimation.Properties.opacity,
    },
    update: {
      type: LayoutAnimation.Types.linear,
    },
  };
  render(){
        return(
          this.state.firstLoading ?
          (<View style={[styles.container,{justifyContent:"center",alignItems:"center"}]}>
            <ActivityIndicator
              size="large"
            />
          </View>
          )
          :
          <View style={styles.container}>
            <SelectionBar
              ref="SelectionBar"
              p={this}
            />
            <CasesList
              ref="CasesList"
              p={this}
            />
          </View>
        )
      }
  }


class CasesRow extends Component{
  constructor(props){
    super(props);
    this.state={
      fadeAnim : new Animated.Value(0),
    }
  }
  componentDidMount(){
    Animated.timing(
      this.state.fadeAnim,
      {
        toValue: 1,
        duration: 350,
      }
    ).start();
  }
  render(){
    return(
      <Animated.View style={[styles.oneViewContainer,{opacity:this.state.fadeAnim}]}>
        <View style={styles.caseTitleContainer}>
          <View style={styles.locationStyle}>
            <Text style={{fontSize:15,top:height*0.01,}}>{this.props.cases.location_name}</Text>
          </View>

          <Text style={{fontSize:15,top:height*0.01,}}>時薪:{this.props.cases.salary}</Text>
        </View>

        <View style={styles.textBoxContainer}>
          <Icon2
            name="blackboard"
            color="gray"
            size={30}
          />
        <Text style={styles.textStyle}>{this.props.cases.event_name}</Text>
        </View>
        <View style={styles.textBoxContainer}>
          <Icon3
            name="group"
            color="gray"
            size={30}
          />
        <Text style={styles.textStyle}>{this.props.cases.size == 'P' ? "個人":"團體"}</Text>
        </View>
        <View style={styles.textBoxContainer}>
          <Icon3
            name="calendar"
            color="gray"
            size={30}
          />
          <Text style={styles.textStyle}>{this.props.cases.period}</Text>
        </View>

        <View style={styles.textBoxContainer}>
          <Icon
            name="clock"
            color="gray"
            size={35}
          />
        <Text style={styles.textStyle}>{this.props.cases.hour}</Text>
        </View>
        <View style={styles.textBoxContainer}>
        <Icon
          name="male-female"
          color="gray"
          size={32}
        />
        <Text style={styles.textStyle}>{this.props.cases.age}歲，{this.props.cases.gender == "M" ? '男':(this.props.cases.gender == "F" ?"女":"有男有女")}</Text>
        </View>

        <View style={styles.textBoxContainer}>
          <Icon6
            name="checklist"
            color="gray"
            size={30}
          />
        <Text style={[styles.textStyle,{padding:5,textAlign:'center'}]}>{this.props.cases.requirement}</Text>
        </View>
        <ApplyButton
          case_id = {this.props.cases.case_id}
          p={this}
          pp={this.props.p}
          ppp={this.props.pp}
        />
      </Animated.View>
    );
  }
}

class CasesList extends Component{
  constructor(props){
    super(props)
    var ds = new ListView.DataSource({
      rowHasChanged: (row1, row2) =>{
        return !Immutable.is(Immutable.Map(row1),Immutable.Map(row2));
      },
    });
    this.state={
      dataSource: ds.cloneWithRows([]),
      length:null,
    }
  }



  componentDidMount(){
    InteractionManager.runAfterInteractions(() => {
      this.getCasesData();
    });

  }
  getCasesData = (param) => {
    var request = new XMLHttpRequest();
    request.onreadystatechange = () => {
      if(request.readyState !== 4)
        return;
      if(request.status === 200){
        var result = request.responseText;
        //console.log(result);
        //console.log(request.status);
        if(result){
          result = JSON.parse(result);
        }else {
          result = JSON.parse('[]');
        }
        isCasesDataLoaded = true;
        LayoutAnimation.configureNext(this.CustomLayoutLinear);
        this.setState({length:result.length,dataSource:this.state.dataSource.cloneWithRows(result)});
      }
    }
    request.open('POST','http://54.179.154.17/search.php',true);
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    if(param){
      var param = toQueryString(param);
    }
    else {
      var param = toQueryString(this.props.p.state.param);
    }
    //console.log(param);
    request.send(param);
  }

  CustomLayoutLinear = {
    duration: 150,
    create: {
      type: LayoutAnimation.Types.linear,
      property: LayoutAnimation.Properties.opacity,
    },
    update: {
      type: LayoutAnimation.Types.linear,
    },
  };

  render(){
    return(
        this.state.length != null && this.state.length < 1 ?
        <View style={{flex:1,justifyContent:"center",alignItems:"center"}}>
          <Text style={{fontSize:25}}>
            沒有找到任何資料
          </Text>
        </View>
        :
        <ListView
          ref="ListView"
          enableEmptySections={true}
          style={styles.listviewContainer}
          contentContainerStyle={{paddingBottom:15}}
          dataSource={this.state.dataSource}
          renderRow={(cases) => <CasesRow p={this} pp={this.props.p} cases={cases}/>}
        />

    );
  }
}

class ApplyButton extends Component{
  constructor(props){
    super(props)
  }
  reGetCategory = () => {
    var request = new XMLHttpRequest();
    var result = [];
    request.onreadystatechange = (e) =>{
      if (request.readyState !== 4) {
        return;
      }
      if(request.status === 200){
        result.push.apply(result,JSON.parse(request.responseText));
        //console.log(JSON.parse(request.responseText));
        isCategoryLoaded = true;
        getCategory = result;
        this.setState({});
      }
    }
    request.open('POST', 'http://54.179.154.17/getCategory.php', true);
    request.send();
  }


  applyCase = (case_id) =>{
    if(!this.props.ppp.props.p.state.userData)
    {
      Toast.show('登入後才可申請。', Toast.SHORT);
      return;
    }
    else if(this.props.ppp.props.p.state.userData.user_type != 2){
      Toast.show('只有導師才可以申請。', Toast.SHORT);
      return;
    }
    var request = new XMLHttpRequest();
    request.onload= () => {

      if(request.status === 200){
        var result = request.responseText;

        if(result.includes('success')){
          Toast.show('已提交申請。', Toast.SHORT);
        }else{
          Toast.show(result, Toast.SHORT);
        }
        //console.log(result);
          //LayoutAnimation.configureNext(LayoutAnimation.Presets.linear);
        //this.setState({dataSource:this.ds.cloneWithRows(result)});
      }
    }
    request.onerror = (e) => {
      //console.log(e);
    }
    request.open("POST","http://54.179.154.17/sendApplyRequest.php",true);
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    request.send(`case_id=${case_id}`);
  }

  render(){
    return(
      <TouchableOpacity
        onPress={() => {
          this.applyCase(this.props.case_id)
        }}
        activeOpacity={0.8}
        style={styles.applyButtonContainer}>


        <Text style={{fontSize:20}}>立即申請</Text>

      </TouchableOpacity>
    )
  }
}

class SelectionBar extends Component {
  constructor(props){
    super(props);
    this.state={
      type:1,
      category1:-1,
      category2:-1,
      category3:-1,
      district1:-1,
      district2:-1,
      sortByRating:false,
    };

    this.category1=[];
    this.category2=[];
    this.category3=[];
    this.district1=[];
    this.district2=[];


    this.district1.push(<Picker.Item key={0} label="地區" value={-1} />);
    this.district2.push(<Picker.Item key={0} label="分區" value={-1} />);
    //this.district3.push(<Picker.Item key={0} label="選區" value={-1} />);

    this.category1.push(<Picker.Item key={0} label="大分類" value={-1} />);
    this.category2.push(<Picker.Item key={0} label="小分類" value={-1} />);
    this.category3.push(<Picker.Item key={0} label="項目" value={-1} />);


  }

  search = () =>{
    if (this.state.category1 == -1 || this.state.category2 == -1 || this.state.category3 == -1
        || this.state.district1 == -1 || this.state.district2 == -1){
          Alert.alert("揀晒野先search到嫁！")
    }else {
      this.props.p.state.param = this.state;
      this.props.p.refs.CasesList.getCasesData(this.state);
    }

  }


  getCategoryElement = (pid,items) =>{
    var itemsView = [];
    if(pid == -1)
      return itemsView;
    for(var i = 0;i < items.length;i++){
      itemsView.push(<Picker.Item label={items[i]['name']} value={items[i]['category_id']} key={`${pid}`+`${i}`}/>);
    }
    return itemsView;
  }

  getDistrictElement = (pid,items) =>{
    var itemsView = [];
    if(pid == -1)
      return itemsView;
    for(var i = 0;i < items.length;i++){
      itemsView.push(<Picker.Item label={items[i]['name']} value={items[i]['district_id']} key={i}/>);
    }
    return itemsView;
  }
  render(){
    //console.log(getDistrict);
    this.category1=[];
    this.district1=[];
    this.district1.push(<Picker.Item key={0} label="地區" value={-1} />);
    this.category1.push(<Picker.Item key={0} label="大分類" value={-1} />);

      this.category1.push.apply(this.category1,this.getCategoryElement(0,getCategory.filter((item) => {
        return item.parent_id == 0;
      })));

      this.district1.push.apply(this.district1,this.getDistrictElement(0,getDistrict.filter((item) => {
        return item.parent_id == 0;
      })));

      return(<View style={styles.selectionContainer}>

        <View style={styles.selectionBar}>
        <View style={styles.pickerContainer}>
          <Picker
            style={styles.picker}
            selectedValue={this.state.category1}
            mode="dropdown"
            onValueChange={(category1) => {
              this.category2.length = 0;
              this.category2.push(<Picker.Item key={0} label="小分類" value={-1} />);
              this.category2.push.apply(this.category2,this.getCategoryElement(category1,getCategory.filter((item) => {
                return item.parent_id == category1;
              })));
              console.log(this.category2);
              this.setState({category1});
            }}
          >
            {this.category1}
          </Picker>
        </View>

        <View style={styles.pickerContainer}>
          <Picker
            style={styles.picker}
            selectedValue={this.state.category2}
            mode="dropdown"
            onValueChange={(category2) => {
              this.category3.length = 0;
              this.category3.push(<Picker.Item key={0} label="項目" value={-1} />);
              this.category3.push.apply(this.category3,this.getCategoryElement(category2,getCategory.filter((item) => {
                return item.parent_id == category2;
              })));
              this.setState({category2});
            }}
          >
            {this.category2}
          </Picker>
        </View>
        <View style={styles.pickerContainer}>
          <Picker
            style={styles.picker}
            selectedValue={this.state.category3}
            mode="dropdown"
            onValueChange={(category3) => this.setState({category3})}
          >
          {this.category3}
          </Picker>
        </View>

        </View>
        <View style={styles.selectionBar}>
          <View style={styles.pickerContainer}>
            <Picker
              style={styles.picker}
              selectedValue={this.state.district1}
              mode="dropdown"
              onValueChange={(district1) => {
                this.district2.length = 0;
                this.district2.push(<Picker.Item key={0} label="分區" value={-1} />);
                this.district2.push.apply(this.district2,this.getDistrictElement(district1,getDistrict.filter((item) => {
                  return item.parent_id == district1;
                })));
                this.setState({district1});
              }}
            >
              {this.district1}

            </Picker>
          </View>

          <View style={styles.pickerContainer}>
            <Picker
              style={styles.picker}
              selectedValue={this.state.district2}
              mode="dropdown"
              onValueChange={(district2) => {
                this.setState({district2});
              }}
            >
              {this.district2}
            </Picker>
          </View>
          <TouchableOpacity
            style={styles.searchButton}
            onPress={() => this.search()}
            activeOpacity={0.8}
          >
            <Icon3
              name="search"
              color="#fff"
              size={20}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
