'use strict'

import  React, {Component} from 'react';
import{
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Navigator,
  Image,
  TouchableOpacity,
  Dimensions,
  TextInput,
  Picker,
  ScrollView,

} from 'react-native';


var {height, width} = Dimensions.get('window');
const height = height-55;

var styles = StyleSheet.create({
  container: {
    flex: 1,
    top:55,

  },
  fieldContainer:{
    flex:1,
  },
  picker: {
    flex:1,
    opacity:0.5,
  },
  nextPageContainer:{
    flex:0.2,
    borderWidth:1,
    flexDirection:"row",
  },
  nextPageButtonContainer:{
    height:height*0.1,
    width:width*0.4,
    left:width*0.6,
    borderWidth:1,
    flexDirection:'row',
  },
  nextPageImage:{
    height:height*0.1,
    width:width*0.17,
  },
  questionContainer:{
    height:height*0.08,
    width:width,
    flexDirection:'row',
  },
  textStyle:{
    fontSize:15,
    top:height*0.025,
  },
  textInputStyle:{
    flex:1,
  },

})

  export default class StudentEmploymentPage extends Component{
    constructor(props){
      super(props);
      this.state ={
        selected:0,
        sex:0,

      };
    }
    render(){
        return(
          <View style={styles.container}>
              <ScrollView style={styles.fieldContainer}>
              <Image
                style={{flex:1}}
                resizeMode="contain"
                source={require('./image/blue.jpg')}
                >
                <View style={styles.questionContainer}>
                  <Text
                    style={styles.textStyle}>學生姓別 :</Text>
                  <Picker
                      style={styles.picker}
                      mode="dropdown"
                      onValueChange={(value)=> this.setState({sex:value})}
                      selectedValue={this.state.sex}
                    >
                    <Picker.Item label="男" value={0} />
                    <Picker.Item label='女' value={1} />
                    </Picker>
                </View>

                  <Question name='學生年齡: '/>
                  <Question name='時薪：'/>
                  <Question name='課堂時段：'
                    placeHolder='四時至六時'
                  />
                  <Question name='課堂時間：'
                    placeHolder='一小時 /  1.5小時'
                  />
                  <Question name='每週堂數：'
                    placeHolder='每週一堂 '
                  />
                  <Question name="上課地區:"/>
                  <Question name="興趣:"/>


                  </Image>
                </ScrollView>
                    <Button />

          </View>

        )
    }
}


class Button extends Component{
      constructor(props){
        super(props);
      }

     /*handlePress =() =>{
       if(this.state.enabled){
         this.props.p.refs.pictureViwer.back();
       }
     };*/

     render(){
       return(
         <View style={styles.nextPageContainer}>
            <Image
            style={{flex:1,}}
            source={require('./image/blue.jpg')}
            resizeMode="contain"
            >
            <TouchableOpacity style={styles.nextPageButtonContainer}>
                  <Image
                  style={styles.nextPageImage}
                  source={require('./image/basicRegistrationPage/bookmarkTrue.png')}
                  resizeMode="contain"
                  />
                  <Text style={{fontSize:25,top:height*0.025}}>尋找導師</Text>
                  </TouchableOpacity>

              </Image>
              </View>

       )
     }
}


class Question extends Component{
    constructor(props){
      super(props);
      this.state={

      };
    }
  render(){
    return(
        <View style={styles.questionContainer}>
          <Text
            style={styles.textStyle}
          >{this.props.name}</Text>
          <TextInput
          style={styles.textInputStyle}
          placeholder={this.props.placeHolder}
          />

        </View>

    )

  }
}
