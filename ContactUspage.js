'use strict'

import  React, {Component} from 'react';
import{
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Navigator,
  Image,
  TouchableOpacity,
  Dimensions,
  TextInput,
  Alert,
  ScrollView,
  TouchableHighlight,

} from 'react-native';


var {height, width} = Dimensions.get('window');
const height = height-55;

var styles = StyleSheet.create({
  container: {
    flex: 1,
    top:55,
    alignItems:"center",
    backgroundColor:"white"
  },
  table:{
    width:width*0.8,
    height:height*0.4,
    borderWidth:1,
    borderRadius:10,
  },
  titleContainer:{
    width:width*0.8,
    height:height*0.08,
    margin:2,
  },
  title:{
    fontSize:20,
    padding:5,
  },
  submitTextContainer:{
    width:width*0.8,
    height:height*0.08,
    margin:10,
    borderWidth:1,
    borderRadius:15,
    alignItems:"center",
    backgroundColor:"#0282c9"
  },
  submitText:{
    fontSize:30,
    color:"#fff"
  }



})
import Toast from 'react-native-simple-toast';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/Entypo';

export default class ContactUspage extends Component{
    constructor(props){
      super(props);
      this.state = {
        email:"",
        name:"",
        advice:"",
    };
    }

    focusNextField = (nextField) => {
    this.refs[nextField].focus();
  };

    clearField = () =>{
      this.refs[1].clear();
      this.refs[2].clear();
      this.refs[3].clear();
    }

    submitForm = ()=>{
      var email = this.state.email;
      var advice = this.state.advice;
      var name = this.state.name;
      var reAdvice =/^[\s\S]{0,500}$/;
      var reEmail=/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if (name == "" ){
        alert("請輸入您的名字");
      }
      else if (name.length >= 45) {
        alert("名字不能過長");
      }
      else if (email == "") {
        alert("請輸入電郵");
      }
      else if (email.length >= 45) {
        alert("電郵不能過長");
      }
      else if (!reEmail.test(email)) {
        alert("請輸入正確電郵格式");
      }
      else if (!reAdvice.test(advice)) {
        alert("建議不能多於五百字");
      }
      else {
        var request = new XMLHttpRequest();
        request.onreadystatechange = (e) =>{
          if (request.readyState !== 4) {
            //console.log("fuck you");
            //console.log(request.status);
            return;
          }
          if(request.status === 200){
            var result = request.responseText;
            var success = result.includes("Successfully");
            if (success){
              Toast.show('已接收您的意見，我們會盡快回覆，謝謝', Toast.SHORT);
              this.clearField();
            }
        }
      }
        request.open('POST', 'http://54.179.154.17/advice.php', true);
        request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        var param = `advice=${advice}&email=${email}&name=${name}`;
        request.send(param);
      }//end else
    }



    render(){
        return(
            <View style={styles.container}>
              <View style={styles.titleContainer}>
                  <Text style={styles.title}>請輸入您資料及意見</Text>
              </View>
              <View style={styles.table}>
                <TextInput
                placeholder="您的名稱"
                ref="1"
                onSubmitEditing={() => this.focusNextField('2')}
                onChangeText={(name) => this.setState({name})}
                >
                </TextInput>
                <TextInput
                placeholder="您的電郵"
                keyboardType="email-address"
                ref="2"
                onSubmitEditing={() => this.focusNextField('3')}
                onChangeText={(email) => this.setState({email})}
                >
                </TextInput>
                <TextInput
                ref="3"
                placeholder="您的意見"
                multiline = {true}
                numberOfLines = {4}
                style={{textAlign: 'left', textAlignVertical: 'top',letterSpacing:3}}
                onChangeText={(advice) => this.setState({advice})}
                >
                </TextInput>
              </View>
              <TouchableHighlight
                style={styles.submitTextContainer}
                onPress={this.submitForm}
                >
                <Text style={styles.submitText}>提交意見</Text>
              </TouchableHighlight>
            </View>
        )
    }
}
