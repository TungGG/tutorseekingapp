'use strict'

import  React, {Component} from 'react';
import{
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Navigator,
  Image,
  TouchableOpacity,
  Dimensions,
  TextInput,
  Picker,
  ScrollView,

} from 'react-native';


var {height, width} = Dimensions.get('window');
const height = height-55;

var styles = StyleSheet.create({
  container: {
    flex: 1,
    top:55,

  },
  fieldContainer:{
    flex:1,
  },
  picker: {
    flex:1,
    opacity:0.5,
  },
  questionContainer:{
    height:height*0.08,
    width:width,
    flexDirection:'row',
  },
  textStyle:{
    fontSize:15,
    top:height*0.025,
  },
  textInputStyle:{
    flex:1,
  },
  nextPageContainer:{
    flex:0.2,
    borderWidth:1,
    flexDirection:"row",
  },
  nextPageButtonContainer:{
    height:height*0.1,
    width:width*0.4,
    left:width*0.6,
    borderWidth:1,
    flexDirection:'row',
  },
  nextPageImage:{
    height:height*0.1,
    width:width*0.17,
  },

})

  export default class InstitutioRegistrationPage extends Component{
    constructor(props){
      super(props);
      this.state ={
        sex:0,
        role:0,

      };
    }
    render(){
        return(
          <View style={styles.container}>



              <ScrollView style={styles.fieldContainer}>
              <Image
                style={{flex:1}}
                resizeMode="contain"
                source={require('./image/blue.jpg')}
                >
                  <Question
                    name='機構全名 :'
                    placeHolder='please input your name'
                  />
                  <Question
                    name='聯絡人全名 :'
                    placeHolder='please input your name'
                  />

                  <Question
                    name='地區 :'
                    placeHolder='please input your name'
                  />
                  <Question
                    name='專業項目 :'
                    placeHolder='please input your name'
                  />
                  <Question
                    name='教學年資 :'
                    placeHolder='please input your name'
                  />
                  <Question
                    name='教學形式 :'
                    placeHolder='一對一 / 小班教學'
                  />
                  </Image>
                </ScrollView>
                    <Button />

          </View>

        )
    }
}


class Button extends Component{
      constructor(props){
        super(props);
      }

     /*handlePress =() =>{
       if(this.state.enabled){
         this.props.p.refs.pictureViwer.back();
       }
     };*/

     render(){
       return(
         <View style={styles.nextPageContainer}>
            <Image
            style={{flex:1,}}
            source={require('./image/blue.jpg')}
            resizeMode="contain"
            >
            <TouchableOpacity style={styles.nextPageButtonContainer}>
                  <Image
                  style={styles.nextPageImage}
                  source={require('./image/basicRegistrationPage/bookmarkTrue.png')}
                  resizeMode="contain"
                  />
                  <Text style={{fontSize:25,top:height*0.025}}>Next</Text>
                  </TouchableOpacity>

              </Image>
              </View>

       )
     }
}

class Question extends Component{
    constructor(props){
      super(props);
      this.state={

      };
    }
  render(){
    return(
        <View style={styles.questionContainer}>
          <Text
            style={styles.textStyle}
          >{this.props.name}</Text>
          <TextInput
          style={styles.textInputStyle}
          placeholder={this.props.placeHolder}
          />

        </View>

    )

  }
}
