'use strict'
import React , {Component} from 'react';
import{
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Navigator,
  Image,
  TouchableOpacity,
  Dimensions,
  ListView,
  Alert,
  Picker,
  InteractionManager,
  ActivityIndicator,
  UIManager,
  LayoutAnimation,

  Animated,
} from 'react-native';
import Toast from 'react-native-simple-toast';
var {height, width} = Dimensions.get('window');
const height = height-55;
import Icon from 'react-native-vector-icons/Foundation';
import Icon2 from 'react-native-vector-icons/Entypo';
import Icon3 from 'react-native-vector-icons/FontAwesome';
import Icon4 from 'react-native-vector-icons/MaterialIcons';
import Icon5 from 'react-native-vector-icons/EvilIcons';
import districtData from './getDistrict.js';


var  Immutable = require('immutable');

var styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:"#ededed",
    marginTop:55,
  },
  titleContainer:{
    height:25,

  },
  listviewContainer:{
    flex:1,
  },
  oneViewContainer:{
    width:width,
    alignItems:'center',
    marginTop:15,

  },
  caseTitleContainer:{
    height:height*0.5/7*0.8,
    width:width*0.9,
    backgroundColor:'#0789ac',
    flexDirection:'row',
    justifyContent:'space-between',
    paddingLeft:5,
    paddingRight:5,
    borderTopLeftRadius:3,
    borderTopRightRadius:3,

  },
  locationStyle:{
    height:height*0.5/7*0.8,
    width:width*0.3,
  },
  textBoxContainer:{
      height:height*0.5/7,
      width:width*0.9,
      borderBottomWidth:1/2,

      flexDirection:'row',
      alignItems:"center",
      padding:5,
      backgroundColor:'#fff',
      borderColor:"#d4d3d3"
  },
  imageStyle:{
    height:height*0.5/7,
    width:height*0.5/7,
  },
  textStyle:{
    flex:1,
    textAlign:'center',
    fontSize:15,
  },
  applyButtonContainer:{
    height:height*0.5/7,
    width:width*0.9,
    flexDirection:'row',
    justifyContent:'center',
    alignItems:"center",
    backgroundColor:'#0789ac',
    borderBottomLeftRadius:3,
    borderBottomRightRadius:3,

},
applyButton:{
  flex:1,
  flexDirection:"row",
  alignItems:'center',
  justifyContent:"center",

},
applyButtonImageStyle:{
  height:height*0.5/7,
  width:height*0.5/7,
},
descriptionBoxContainer:{
    height:height*0.5/7*2,
    width:width*0.9,

    flexDirection:'row',
    alignItems:"center",
    backgroundColor:'#fff',

},
descriptionImageStyle:{
  height:height*0.5/7*2,
  width:height*0.5/7,
},
arrow:{
  position:"absolute",
  right:-10,
  bottom:150,
},
selectionContainer:{
  height:height*0.14,
  paddingRight:5,
  paddingBottom:5,
},
selectionBar:{
  flex:1,
  flexDirection:'row',
  alignItems:'center',
  paddingTop:5,
},
picker:{
  flex:1,
},
pickerContainer:{
  flex:1,
  borderWidth:1/2,
  paddingTop:-10,
  borderRadius:5,
  marginLeft:5,
  borderColor:"#9c9c9c",
  backgroundColor:"#fff",

},
searchButton:{
  backgroundColor:"#fe4f21",
  padding:5,
  paddingLeft:7,
  justifyContent:"center",
  alignItems:"center",
  marginLeft:5,
  borderRadius:6,
},
})

var getCategory = [];
var getDistrict = districtData;
var isCategoryLoaded = false;
var isCoachDataLoaded = false;

var toQueryString = (obj)=> {
    return obj ? Object.keys(obj).sort().map(function (key) {
        var val = obj[key];
        if (Array.isArray(val)) {
            return val.sort().map(function (val2) {
                return key + '=' + val2;
            }).join('&');
        }
        return key + '=' + val;
    }).join('&') : '';
}

export default class CoachListPage extends Component{
  constructor(porps){
    super(porps);

    this.state={
      firstLoading:true,
      param:this.props.param,
    };
    ////console.log(this.props.param);
    UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
  }


  shouldComponentUpdate(nextProps,nextState){
    if(this.state.firstLoading){
      this.componentDidMount();
      return true;
    }
    else if(nextState.param){
      if(getCategory.length < 1 && SelectionBar)
        InteractionManager.runAfterInteractions(() => {
          this.refs.SelectionBar.reGetCategory();
        });
      if(this.refs.CoachList)
        InteractionManager.runAfterInteractions(() => {
          if(this.refs.CoachList.refs.ListView)
            this.refs.CoachList.refs.ListView.scrollTo({y: 0});
          this.refs.CoachList.getCoachData(nextState.param);
        });
    }
    return false;
  }
  componentDidMount(){
    InteractionManager.runAfterInteractions(() => {
      getCategory = (() => {
        var request = new XMLHttpRequest();
        var result = [];
        request.onreadystatechange = (e) =>{
          if (request.readyState !== 4) {
            return;
          }
          if(request.status === 200){
            result.push.apply(result,JSON.parse(request.responseText));
            ////console.log(JSON.parse(request.responseText));
            isCategoryLoaded = true;
            getCategory = result;
            LayoutAnimation.configureNext(LayoutAnimation.Presets.linear);
            this.setState({firstLoading:!isCategoryLoaded});
          }
        }
        request.open('POST', 'http://54.179.154.17/getCategory.php', true);
        request.send();
        return result;
      })();
    });
  }

  render(){
        return(
          this.state.firstLoading ?
          (<View style={[styles.container,{justifyContent:"center",alignItems:"center"}]}>
            <ActivityIndicator
              size="large"
            />
          </View>
          )
          :
          <View style={styles.container}>
            <SelectionBar
              ref="SelectionBar"
              p={this}
            />
            <CoachList
              ref="CoachList"
              p={this}
            />
          </View>
        )
      }
}

class CoachRow extends Component{
  constructor(props){
    super(props);
    this.state={
      fadeAnim : new Animated.Value(0),
    }
  }
  convertLang = (langString) => {
    var lang =" ";
    if(langString){
      if(langString.includes("C"))
        lang = lang + "廣東話";
      if(langString.includes("M"))
        lang = lang + ",普通話";
      if(langString.includes("E"))
        lang = lang + ",英文";
    }
    return lang;
  }
  componentDidMount(){
    Animated.timing(
      this.state.fadeAnim,
      {
        toValue: 1,
        duration: 350,
      }
    ).start();
  }
  render(){
    return(
      <Animated.View
        style={[styles.oneViewContainer,{opacity:this.state.fadeAnim}]}
      >
        <View style={styles.caseTitleContainer}>
          <View style={styles.locationStyle}>
            <Text style={{fontSize:15,top:height*0.01,color:"#fff"}}>{this.props.coach.username}</Text>
          </View>

          <Text style={{fontSize:15,top:height*0.01,color:"#fff"}}>期望時薪:{this.props.coach.expected_fee}</Text>
        </View>

        <View style={styles.textBoxContainer}>
        <Icon
          name="male-female"
          color="gray"
          size={30}
        />
        <Text style={styles.textStyle}>{this.props.coach.gender == "M" ? '男':'女'}</Text>
        </View>

        <View style={styles.textBoxContainer}>
        <Icon3
          name="language"
          color="gray"
          size={30}
        />
        <Text style={styles.textStyle}>{this.convertLang(this.props.coach.teaching_language)}</Text>
        </View>


        <View style={styles.textBoxContainer}>
        <Icon2
          name="sports-club"
          color="gray"
          size={30}
        />
        <Text style={styles.textStyle}>{this.props.coach.expert_name}</Text>
        </View>

        <View style={styles.textBoxContainer}>
        <Icon2
          name="blackboard"
          color="gray"
          size={30}
        />
        <Text style={styles.textStyle}>{this.props.coach.experience}年經驗</Text>
        </View>
        <ApplyButton
          p={this}
          pp={this.props.p}
          ppp={this.props.pp}
          coach_id = {this.props.coach.coach_id}
        />
      </Animated.View>
    );
  }
}

class CoachList extends Component{
  constructor(props){
    super(props)
    var ds = new ListView.DataSource({
      rowHasChanged: (row1, row2) => {
        return !Immutable.is(Immutable.Map(row1),Immutable.Map(row2));
      }
    });
    this.state={
      dataSource: ds.cloneWithRows([]),
      length:null,
    }
  }

    componentDidMount(){
      InteractionManager.runAfterInteractions(() => {
        this.getCoachData();
      });

    }
  getCoachData = (param) => {
    var request = new XMLHttpRequest();
    request.onreadystatechange = () => {
      if(request.readyState !== 4)
        return;
      if(request.status === 200){
        var result = request.responseText;
        //console.log(result);
        if(result.length > 0){
          result = JSON.parse(result);
        }else {
          result = JSON.parse('[]');
        }
        isCoachDataLoaded = true;
        //LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        this.setState({length:result.length,dataSource:this.state.dataSource.cloneWithRows(result)});
      }
    }
    request.open('POST','http://54.179.154.17/search.php',true);
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    if(param){
      var param = toQueryString(param);
    }
    else {
      var param = toQueryString(this.props.p.state.param);
    }
    //console.log(param);
    request.send(param);
  }


  render(){
    return(
        this.state.length != null && this.state.length < 1 ?
        <View style={{flex:1,justifyContent:"center",alignItems:"center"}}>
          <Text style={{fontSize:25}}>
            沒有找到任何資料
          </Text>
        </View>
        :
        <ListView
          ref="ListView"
          enableEmptySections={true}
          style={styles.listviewContainer}
          contentContainerStyle={{paddingBottom:15}}
          dataSource={this.state.dataSource}
          renderRow={ (coach) => <CoachRow p={this} pp={this.props.p} coach={coach}/>}
        />

    );
  }
}



class ApplyButton extends Component{
  constructor(props){
    super(props)
  }
  reGetCategory = () => {
    var request = new XMLHttpRequest();
    var result = [];
    request.onreadystatechange = (e) =>{
      if (request.readyState !== 4) {
        return;
      }
      if(request.status === 200){
        result.push.apply(result,JSON.parse(request.responseText));
        ////console.log(JSON.parse(request.responseText));
        isCategoryLoaded = true;
        getCategory = result;

        this.setState({});
      }
    }
    request.open('POST', 'http://54.179.154.17/getCategory.php', true);
    request.send();
  }

  onPress = (coach_id) => {
    this.props.ppp.props.p.gotoPage({title:'InfoPage',navBarTitle:"導師資料",param:{coach_id:coach_id}});
  }

  choose =(coach_id)=>{
  if (this.props.ppp.props.p.state.userData == null) {
    Toast.show('請先登入，再選擇導師！', Toast.SHORT);
  }else {
    if(this.props.ppp.props.p.state.userData.user_type == 2 ){
      Toast.show('導師不能選擇導師!', Toast.SHORT);
    }else if (this.props.ppp.props.p.state.userData.user_type == 1) {
      var request = new XMLHttpRequest();

      request.onreadystatechange = (e) =>{
        console.log(request.status);
        if (request.readyState !== 4) {
          return;
        }else{
        if(request.status === 200){
          var result = request.responseText;
          console.log(request.responseText);
          if(result.includes("Success")){
            Toast.show('成功選擇導師!請等候專人聯繫', Toast.SHORT);
        }else if(result.includes("already exist")){
          Toast.show('你己經成功選擇導師!請等候專人聯繫', Toast.LONG);
        }
      }
    }
  }
  request.open('POST','http://54.179.154.17/waitingForPairUp.php',true);
  request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  request.send(`coach_id=${coach_id}`);
    }
  }
}


  render(){
    return(
      <View style={styles.applyButtonContainer}>
        <TouchableOpacity
          onPress={() => this.onPress(this.props.coach_id)}
          activeOpacity={0.7}
          style={[styles.applyButton,{borderRightWidth:1,borderColor:"#ffffff"}]}>
          <Text style={{fontSize:18,color:"#fff"}}>查看資料</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={()=> this.choose(this.props.coach_id)}
          activeOpacity={0.7}
          style={styles.applyButton}>
          <Text style={{fontSize:18,color:"#fff"}}>選擇教練</Text>
        </TouchableOpacity>

      </View>
    )
  }
}

class SelectionBar extends Component {
  constructor(props){
    super(props);
    this.state={
      type:2,
      category1:-1,
      category2:-1,
      category3:-1,
      district1:-1,
      district2:-1,
      sortByRating:false,
    };

    this.category1=[];
    this.category2=[];
    this.category3=[];
    this.district1=[];
    this.district2=[];


    this.district1.push(<Picker.Item key={0} label="地區" value={-1} />);
    this.district2.push(<Picker.Item key={0} label="分區" value={-1} />);
    //this.district3.push(<Picker.Item key={0} label="選區" value={-1} />);
    this.category1.push(<Picker.Item key={0} label="大分類" value={-1} />);
    this.category2.push(<Picker.Item key={0} label="小分類" value={-1} />);
    this.category3.push(<Picker.Item key={0} label="項目" value={-1} />);


  }

  search = () =>{
    if (this.state.category1 == -1 || this.state.category2 == -1 || this.state.category3 == -1
        || this.state.district1 == -1 || this.state.district2 == -1){
          Alert.alert("揀晒野先search到嫁！")
    }else {
      this.props.p.state.param = this.state;
      this.props.p.refs.CoachList.getCoachData(this.state);
    }

  }

  getCategoryElement = (pid,items) =>{
    var itemsView = [];
    if(pid == -1)
      return itemsView;
    for(var i = 0;i < items.length;i++){
      itemsView.push(<Picker.Item label={items[i]['name']} value={items[i]['category_id']} key={`${pid}`+`${i}`}/>);
    }
    return itemsView;
  }

  getDistrictElement = (pid,items) =>{
    var itemsView = [];
    if(pid == -1)
      return itemsView;
    for(var i = 0;i < items.length;i++){
      itemsView.push(<Picker.Item label={items[i]['name']} value={items[i]['district_id']} key={i}/>);
    }
    return itemsView;
  }

  render(){
    ////console.log(getDistrict);
    this.category1=[];
    this.district1=[];
    this.district1.push(<Picker.Item key={0} label="地區" value={-1} />);
    this.category1.push(<Picker.Item key={0} label="大分類" value={-1} />);
    this.category1.push.apply(this.category1,this.getCategoryElement(0,getCategory.filter((item) => {
      return item.parent_id == 0;
    })));

    this.district1.push.apply(this.district1,this.getDistrictElement(0,getDistrict.filter((item) => {
      return item.parent_id == 0;
    })));
    return(

      <View style={styles.selectionContainer}>

        <View style={styles.selectionBar}>
        <View style={styles.pickerContainer}>
          <Picker
            ref="c1"
            style={styles.picker}
            selectedValue={this.state.category1}
            mode="dropdown"
            onValueChange={(category1) => {
              this.category2.length = 0;
              this.category2.push(<Picker.Item key={0} label="小分類" value={-1} />);
              this.category2.push.apply(this.category2,this.getCategoryElement(category1,getCategory.filter((item) => {
                return item.parent_id == category1;
              })));
              ////console.log(this.category2);
              this.setState({category1});
            }}
          >
            {this.category1}
          </Picker>
        </View>

        <View style={styles.pickerContainer}>
          <Picker
            ref="c2"
            style={styles.picker}
            selectedValue={this.state.category2}
            mode="dropdown"
            onValueChange={(category2) => {
              this.category3.length = 0;
              this.category3.push(<Picker.Item key={0} label="項目" value={-1} />);
              this.category3.push.apply(this.category3,this.getCategoryElement(category2,getCategory.filter((item) => {
                return item.parent_id == category2;
              })));
              this.setState({category2});
            }}
          >
            {this.category2}
          </Picker>
        </View>
        <View style={styles.pickerContainer}>
          <Picker
            ref="c3"
            style={styles.picker}
            selectedValue={this.state.category3}
            mode="dropdown"
            onValueChange={(category3) => this.setState({category3})}
          >
          {this.category3}
          </Picker>
        </View>

        </View>
        <View style={styles.selectionBar}>
          <View style={styles.pickerContainer}>
            <Picker
              ref="d1"
              style={styles.picker}
              selectedValue={this.state.district1}
              mode="dropdown"
              onValueChange={(district1) => {
                this.district2.length = 0;
                this.district2.push(<Picker.Item key={0} label="分區" value={-1} />);
                this.district2.push.apply(this.district2,this.getDistrictElement(district1,getDistrict.filter((item) => {
                  return item.parent_id == district1;
                })));
                this.setState({district1});
              }}
            >
              {this.district1}

            </Picker>
          </View>

          <View style={styles.pickerContainer}>
            <Picker
              ref="d2"
              style={styles.picker}
              selectedValue={this.state.district2}
              mode="dropdown"
              onValueChange={(district2) => {
                this.setState({district2});
              }}
            >
              {this.district2}
            </Picker>
          </View>
          <TouchableOpacity
            onPress={() => this.search() }
            style={styles.searchButton}
            activeOpacity={0.8}
          >
            <Icon3
              name="search"
              color="#fff"
              size={20}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
