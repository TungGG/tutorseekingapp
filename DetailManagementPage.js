'use strict';

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Navigator,
  Dimensions,
  BackAndroid,
  TouchableOpacity,
  PixelRatio,
  Alert,
  ListView,
  ScrollView,
  Image,
  Modal,
  TextInput,
  Platform,
  InteractionManager,
  UIManager,
  LayoutAnimation
} from 'react-native';

import ImagePicker from 'react-native-image-picker';
const {height, width} = Dimensions.get('window');
import Icon from 'react-native-vector-icons/MaterialIcons';
import Icon2 from 'react-native-vector-icons/Octicons';


export default class DetailManagementPage extends Component{
  constructor(props){
    super(props);
    this.ds = new ListView.DataSource({
      rowHasChanged: (p1, p2) => p1 !== p2,
    });
    this.state={
      dataSource:this.ds.cloneWithRows([]),
      numOfitem: 0,
    }
  }

  CustomLayoutLinear = {
    duration: 200,
    create: {
      type: LayoutAnimation.Types.linear,
      property: LayoutAnimation.Properties.opacity,
    },
    update: {
      type: LayoutAnimation.Types.linear,
    },
  };
  getDetailData = () =>{
    var request = new XMLHttpRequest();
    request.onreadystatechange = (e) =>{
      if(request.readyState !== 4){
        return;
      }
      if(request.status === 200){
        var result = request.responseText;
        if(result.length > 0){
          result = JSON.parse(result);
          LayoutAnimation.configureNext(this.CustomLayoutLinear);
          this.setState({dataSource:this.ds.cloneWithRows(result),numOfitem:result.length});
        }
      }
    }
    request.open('POST', 'http://54.179.154.17/getCoachDetail.php', true);
    request.send();
  }

  renderRow = (rowData) => {
    return(
      <View style={styles.detailRootContainer}>
      <TouchableOpacity
        onPress={() => this.refs.editModal.setState({
          visibleModal:true,
          photoSource:"http://54.179.154.17/upload/coachDetail/"+rowData.filename,
          description:rowData.description,
          detail_id:rowData.detail_id
        })}
        activeOpacity={0.7}
        style={styles.detailContainer}>
        <Image
          style={styles.detailImg}
          source={{uri:"http://54.179.154.17/upload/coachDetail/"+rowData.filename}}
          resizeMode="cover"
        />
        <View style={styles.detailTextContainer}>
            <Text

              style={{flex:1}}
              numberOfLines={4}
            >描述：
            {"\n"+rowData.description}
            </Text>

        </View>


      </TouchableOpacity>
        <TouchableOpacity
          onPress={ () => this.deleteDetail(rowData.detail_id,rowData.filename)}
          activeOpacity={0.6}
          style={styles.deleteButton}
        >
          <Icon2
            name="remove-close"
            color="#fff"
            size={15}
          />
        </TouchableOpacity>
      </View>

    );
  }
  deleteDetail = (id,filename) => {
    Alert.alert("咪住先","求下你唔好剷我！！！",[
      {
        text:"唔理佢照剷",
        onPress: () => {
          var deleteRequest = new XMLHttpRequest();
          deleteRequest.onreadystatechange = () => {
            if(deleteRequest.readyState !== 4)
              return;
            if(deleteRequest.status === 200){
              var result = deleteRequest.responseText;
              if(result.includes("success")){
                this.getDetailData();
              }
            }
          }
          deleteRequest.open('POST','http://54.179.154.17/deleteDetail.php', true);
          deleteRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
          var parem = `unwantedFile=${filename}&detail_id=${id}`;
          deleteRequest.send(parem);
        }
      },
      {
        text:"都係唔剷算",
        onPress:null
      },
    ]);
  }
  onPressAdd = () => {
    if(this.state.numOfitem < 10){
       this.refs.addDetail.setState({visibleModal:true});
    }
    else{
      Alert.alert("錯誤","介紹數量不能超過10");
    }
  }
  renderFooter = () => {
    return(
      this.state.numOfitem < 10 ?
      <TouchableOpacity
        onPress={ () => this.onPressAdd()}
        style={styles.addButton}>
        <Icon
          name="add-a-photo"
          color="#ccc"
          size={80}
        />
        <Text style={{color:"#ccc",fontSize:25}}>
          按此新增介紹
        </Text>
      </TouchableOpacity>
      :
      null
    )


  }
  componentDidMount() {
    InteractionManager.runAfterInteractions(() => {
      this.getDetailData();
    });

  }
  render(){
    return(
      <View style={styles.rootContainer}>
        <ListView
          enableEmptySections={true}
          contentContainerStyle={{justifyContent:'center',alignItems:'center',paddingBottom:20}}
          dataSource={this.state.dataSource}
          renderRow={(dataRow) => this.renderRow(dataRow)}
          renderFooter={() => this.renderFooter()}
        />
        <AddDetail
          p={this}
          ref="addDetail"
        />
        <EditModal
          p={this}
          ref="editModal"
        />
      </View>

    );
  }
}

class AddDetail extends Component{
  constructor(props){
    super(props);
    this.state={
      visibleModal:false,
      description:"",
      photoSource:null,
    }
  }
  selectPhotoTapped() {
    const options = {
      title:"選擇相片",
      cancelButtonTitle:"取消",
      takePhotoButtonTitle:"拍照",
      chooseFromLibraryButtonTitle:"從圖庫中選取",
      quality: 0.8,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
      skipBackup: true
    }
  };
  ImagePicker.showImagePicker(options, (response) => {
    //console.log('Response = ', response);

    if (response.didCancel) {
      //console.log('User cancelled photo picker');
    }
    else if (response.error) {
      //console.log('ImagePicker Error: ', response.error);
    }
    else if (response.customButton) {
      //console.log('User tapped custom button: ', response.customButton);
    }
    else {
      var source;

      // You can display the image using either:
      //source = {uri: 'data:image/jpeg;base64,' + response.data, isStatic: true};

      //Or:
      if (Platform.OS === 'android') {
        source = {uri: response.uri,type:response.type,name:response.fileName, isStatic: true};
      } else {
        source = {uri: response.uri.replace('file://', ''), isStatic: true};
      }
      //console.log(source);
      this.setState({
        photoSource: source
      });
    }
  });
}
onPressUpload = () => {
  if(!this.state.photoSource){
    Alert.alert("錯誤","請選擇圖片。");
    return;
  }
  var uploadRequest = new XMLHttpRequest();
  var data = new FormData();
  data.append("img1",this.state.photoSource);
  data.append("text1",this.state.description);
  uploadRequest.onreadystatechange = () => {
    if(uploadRequest.readyState !== 4)
      return;
    if(uploadRequest.status === 200){
      var result = uploadRequest.responseText;

      if(result.includes("success")){
        this.setState({visibleModal:false,photoSource:null,description:""});
        this.props.p.getDetailData();
      }
      else if(result.includes("exceeds")){
        Alert.alert("錯誤","圖片超過200kb。");
      }
    }else{
      Alert.alert("錯誤","請求發送失敗");
    }
  }
  uploadRequest.open('POST','http://54.179.154.17/uploadDetail.php', true);
  uploadRequest.send(data);


}
  render(){
    return(
        <Modal
          contentContainerStyle={styles.addModalContainer}
          animationType={"slide"}
          visible={this.state.visibleModal}
          transparent={true}
          onRequestClose={ () => null}
        >
          <View
            style={styles.addModalRootContainer}
          >
            <View
              style={styles.addModalContainer}
            >
              <View style={styles.modalTitleContainer}>
                <Text>
                  新增介紹
                </Text>
                <TouchableOpacity
                  activeOpacity={0.6}
                  style={styles.closeModal}
                  onPress={ () => this.setState({visibleModal:false,photoSource:null,description:""})}
                >
                  <Icon2
                    name="remove-close"
                    color="#b8b8b8"
                    size={20}
                  />
                </TouchableOpacity>
              </View>
            <TouchableOpacity
              onPress={ () => this.selectPhotoTapped()}
              style={styles.addPhoto}>
              {this.state.photoSource?
                <Image resizeMode="cover" style={styles.photo} source={this.state.photoSource} />:
                <View style={{alignItems:"center"}}>
                  <Icon
                    name="add-a-photo"
                    color="#ccc"
                    size={80}
                  />
                  <Text style={{color:"#ccc",fontSize:25}}>
                    按此新增相片
                  </Text>
                </View>

              }


            </TouchableOpacity>

              <View
                style={styles.descriptionEntryContainer}
              >
                <TextInput
                  value={this.state.description}
                  onChangeText={ (description) => this.setState({description})}
                  style={styles.descriptionEntry}
                  multiline={true}
                  placeholder="在這輸入描述"
                  placeholderTextColor="#a1a1a1"
                />
              </View>
              <View style={styles.comfirmButtonContainer}>
                <Icon.Button
                  onPress={() => this.onPressUpload()}
                  style={{width:width*0.7,backgroundColor:"#409ee0",justifyContent:"center",borderRadius:0}}
                  name="add"
                  color="#fff"
                  size={20}

                >
                確定
                </Icon.Button>
              </View>

            </View>
          </View>
        </Modal>
    );
  }
}

class EditModal extends Component{
  constructor(props){
    super(props);
    this.state={
      visibleModal:false,
      description:"",
      photoSource:"",
      detail_id:null,
    }

  }
  onPressEdit = () => {
    var editRequest = new XMLHttpRequest();
    editRequest.onreadystatechange = () => {
      if(editRequest.readyState !== 4)
        return;



      //Alert.alert("status: "+editRequest.status);

      if(editRequest.status === 200){
        var result = editRequest.responseText;
        if(result.includes("success")){
          this.props.p.getDetailData();
          this.setState({
            visibleModal:false,
            description:"",
            photoSource:"",
            detail_id:null,
          });
        }
        else{
          Alert.alert("錯誤","請求發送失敗");
        }
      }else{
        Alert.alert("錯誤","請求發送失敗");
      }
    }
    editRequest.open('POST','http://54.179.154.17/editCoachDetail.php', true);
    editRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    var parem = `description=${this.state.description}&detail_id=${this.state.detail_id}`;
    editRequest.send(parem);
  }

  render(){
    return(
      <Modal
        contentContainerStyle={styles.addModalContainer}
        animationType={"slide"}
        visible={this.state.visibleModal}
        transparent={true}
        onRequestClose={ () => null}
      >
        <View
          style={styles.addModalRootContainer}
        >
          <View
            style={styles.addModalContainer}
          >
            <View style={styles.modalTitleContainer}>
              <Text>
                編輯介紹
              </Text>
              <TouchableOpacity
                activeOpacity={0.6}
                style={styles.closeModal}
                onPress={ () => this.setState({
                  visibleModal:false,
                  photoSource:null,
                  description:"",
                  detail_id:null,
                })}
              >
                <Icon2
                  name="remove-close"
                  color="#b8b8b8"
                  size={20}
                />
              </TouchableOpacity>
            </View>
            <Image
              resizeMode="cover"
              style={styles.photo}
              source={{uri:this.state.photoSource}}
            />
            <View
              style={styles.descriptionEntryContainer}
            >
              <TextInput
                value={this.state.description}
                onChangeText={ (description) => this.setState({description})}
                style={styles.descriptionEntry}
                multiline={true}
                placeholder="在這輸入描述"
                placeholderTextColor="#a1a1a1"
              />
            </View>
            <View style={styles.comfirmButtonContainer}>
              <Icon.Button
                onPress={() => this.onPressEdit()}
                style={{width:width*0.7,backgroundColor:"#409ee0",justifyContent:"center",borderRadius:0}}
                name="edit"
                color="#fff"
                size={20}
              >
              確定
              </Icon.Button>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}


var styles = StyleSheet.create({
  photo:{
    height:height*0.4,
    width:width*0.9,
  },
  closeModal:{
    height:26,
    width:26,
    borderRadius:13,
    justifyContent:'center',
    alignItems:"center",
    borderColor:"#dadada",
    borderWidth:1,
  },
  modalTitleContainer:{
    height:50,
    borderBottomWidth:1,
    borderColor:"#dadada",
    width:width*0.9,
    justifyContent:"space-between",
    alignItems:"center",
    flexDirection:"row",
    paddingLeft:15,
    paddingRight:15,

  },
  comfirmButtonContainer:{
    paddingBottom:30,
    width:width*0.7,
  },
  descriptionEntry:{
    width:width*0.9,
    height:width*0.3,
    textAlignVertical:"top",
    padding:10,
  },
  descriptionEntryContainer:{
    height:width*0.3,
    paddingTop:10,
    marginBottom:5,
    backgroundColor:"#fff",
  },
  addPhoto:{
    height:height*0.4,
    width:width*0.9,
    alignItems:'center',
    justifyContent:'center',
  },
  addModalContainer:{
    backgroundColor:"#fff",
    alignItems:'center',
    elevation:5
  },
  addModalRootContainer:{
    flex:1,
    justifyContent:"center",
    alignItems:'center',
    backgroundColor:"rgba(0, 0, 0, 0.5)"
  },
  deleteButton:{
    position:"absolute",
    top:10,
    left:width*0.1/2-10,
    height:20,
    width:20,
    borderRadius:10,
    backgroundColor:"red",
    justifyContent:'center',
    alignItems:"center",
  },
  detailTextContainer:{
    padding:5,
    paddingTop:0,
    flex:1,
    width:width*0.9
  },
  detailImg:{
    height:width*0.4,
    width:width*0.9,
  },
  detailRootContainer:{
    paddingTop:20,
    width:width,
    justifyContent:"center",
    alignItems:"center"
  },
  detailContainer:{
    backgroundColor:"#fff",
    justifyContent:"center",
    alignItems:"center",
    height:width*0.6,
    width:width*0.9,
    borderWidth:1/2,
    borderColor:"#c1c1c1",


  },
  addButton:{
    marginTop:20,
    marginBottom:20,
    height:width*0.5,
    width:width*0.9,
    alignItems:'center',
    justifyContent:'center',
    borderWidth:4,
    borderColor:"#ccc",
    borderRadius:5,
  },
  rootContainer:{
    flex:1,
    marginTop:55,
    backgroundColor:"#dedddd",
    alignItems:"center"
  }
});
