'use strict';

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Navigator,
  Image,
  TouchableOpacity,
  Dimensions,
  ViewPagerAndroid,
  PixelRatio,
  ListView,
  InteractionManager,
  ScrollView
} from 'react-native';

import type { ViewPagerScrollState } from 'ViewPagerAndroid';
var {height, width} = Dimensions.get('window');
const ratio = PixelRatio.get();
import {ViewPager,IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator} from 'rn-viewpager';
const PAGES = 10;
var IMAGE_URIS = [
  'https://3.bp.blogspot.com/-W__wiaHUjwI/Vt3Grd8df0I/AAAAAAAAA78/7xqUNj8ujtY/s1600/image02.png',
  'http://www.w3schools.com/css/img_forest.jpg',
];

import Icon from 'react-native-vector-icons/Ionicons';
import detailPageData from './detailPageData.js';
var styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor:"#fff",
  },
  detailContainer:{
    flex:1,
    width:width,
  },
  leftarrowButton:{
    height:height*0.085,
    width:height*0.085,
    top:height*0.2,
    left:10,
    position:'absolute',
    backgroundColor:'transparent'
  },
  rightarrowButton:{
    height:height*0.085,
    width:height*0.085,
    top:height*0.2,
    left:width-30,
    position:'absolute',
    backgroundColor:'transparent'

  },
  imageStyle:{
    height:height*0.45,
    width:width,
    resizeMode:"cover",
  },

  imageCounter:{
    top:height*0.45-30,
    left:(width-50)/2,
    width:50,
    height:25,
    position:'absolute',
    justifyContent:"center",
    alignItems:"center",
    backgroundColor:"rgba(0, 0, 0, 0.2)",
    paddingLeft:10,
    paddingRight:10,
  },
  nothingMessage:{

    fontSize:20,
  },
  nothingMessageContainer:{
    flex:1,
    height:height-100,
    justifyContent:"center",
    alignItems:'center',
  },
  viewPagerContainer:{
    flex:1,
  },
  pictureViwerContainer:{
    flex:1,
    width:width,
  }
});


var toQueryString = (obj) => {
    return obj ? Object.keys(obj).sort().map(function (key) {
        var val = obj[key];
        if (Array.isArray(val)) {
            return val.sort().map(function (val2) {
                return key + '=' + val2;
            }).join('&');
        }
        return key + '=' + val;
    }).join('&') : '';
}

export default class DetailPage extends Component{
  constructor(props){
    super(props);
    this.state={
      firstLoading:true,
      param:this.props.p.state.param,
      length:0,
    }
    ////console.log(this.state.param);
  }
  shouldComponentUpdate(nextProps, nextState){
    //////console.log(nextState);
    if(nextState.param.coach_id != this.state.param.coach_id){
      this.refs.pictureViwer.getDetailData(nextState.param);
    }
    return true;
  }
  render(){
    return(
        <View style={styles.container}>
          <View style={styles.viewPagerContainer}>
              <PictureViwer
                ref="pictureViwer"
                p={this}
              />
          </View>
      </View>
    )
  }
}

class PictureViwer extends Component{

      constructor(props){
        super(props);
        this.ds = new ListView.DataSource({
          rowHasChanged:(r1, r2) => r1 !== r2
        });
        this.state = {
          page:0,
          dataSource:this.ds.cloneWithRows([]),
          length:0,
        };
      }

      getDetailData = (coach_id)=>{

        var request = new XMLHttpRequest();
        request.onreadystatechange = (e) =>{
          if(request.readyState !== 4){
            return;
          }
          if(request.status === 200){
            var result = request.responseText;
            if(result.length > 0){
              result = JSON.parse(result);
              ////console.log(result);
              //this.props.p.setState({length:result.length});
              this.setState({page:0,length:result.length,dataSource:this.ds.cloneWithRows(result)});

            }
          }
        }
        request.open('POST', 'http://54.179.154.17/getCoachDetail.php', true);
        request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        if(coach_id)
          var param = toQueryString(coach_id);
        else
          var param = toQueryString(this.props.p.state.param);
        ////console.log(param);
        request.send(param);
      }

    go = () => {
        this.refs.pictureViwer.scrollTo({x:(this.state.page+1)*width,y:0,animated:true});

    };
    back = ()=>{
        this.refs.pictureViwer.scrollTo({x:(this.state.page-1)*width,y:0,animated:true});

    };
    _renderRow = (rowData) => {
      return(
      <View style={styles.pictureViwerContainer}>
        <Image
          style={styles.imageStyle}
          source={{uri:"http://54.179.154.17/upload/coachDetail/"+rowData.filename}}
        />
        <ScrollView
          style={styles.detailContainer}
          contentContainerStyle={{padding:5}}
        >
          <Text style={{fontSize:20}}>詳細描述:</Text>
          <Text>{rowData.description}</Text>
        </ScrollView>
      </View>)
    }

    _onScroll = (e) => {
      var page = parseInt(e.nativeEvent.contentOffset.x/width);
      if(page != this.state.page){
        this.setState({page});
        //this.props.p.refs.leftBnt.setState({enabled: page >0 ? true:false});
        //this.props.p.refs.rightBnt.setState({enabled: page < this.state.length-1 ? true:false});
      }
    }

    componentDidMount(){
      InteractionManager.runAfterInteractions(() => {
        this.getDetailData();
      });
    }
    render(){
      return(
          this.state.length > 0?
            <View style={styles.viewPagerContainer}>
              <ListView
              showsHorizontalScrollIndicator={false}
              enableEmptySections={true}
              onScroll={ (e) => this._onScroll(e)}
              ref="pictureViwer"
              pageSize={2}
              pagingEnabled={true}
              horizontal={true}
              dataSource={this.state.dataSource}
              renderRow={(rowData) => this._renderRow(rowData)}
            />
            <View style={styles.imageCounter}>
              <Text style={{fontSize:height*0.03, color:"#fff"}}>
                  {this.state.page+1}/{this.props.p.refs.pictureViwer.state.length}
              </Text>
            </View>

            <TouchableOpacity
               activeOpacity={0.8}
               disabled={this.state.page == 0 ? true:false}
               onPress={() => this.back()}
               style={styles.leftarrowButton}
              >
              <Icon
                name="ios-arrow-back"
                color={this.state.page == 0 ? "transparent":"rgba(255, 255, 255, 0.8)"}
                size={50}
              />
           </TouchableOpacity>
           <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => this.go()}
              disabled={this.state.page < this.state.length - 1 ? false:true}
              style={styles.rightarrowButton}
             >
             <Icon
               name="ios-arrow-forward"
               color={this.state.page < this.state.length - 1 ? "rgba(255, 255, 255, 0.8)":"transparent"}
               size={50}
             />
          </TouchableOpacity>
          </View>


          :
          <View style={styles.nothingMessageContainer}>
            <Text style={styles.nothingMessage}>哩條友好懶無上傳低任何野</Text>
          </View>


      )
    }

}


class Counter extends Component{
    constructor(props){
      super(props);
      this.state={
        currentPage:0
    };
  }


  render(){
    return(
      <View style={styles.imageCounter}>
        <Text style={{fontSize:height*0.03, color:"#fff"}}>
            {this.state.currentPage+1}/{this.props.p.refs.pictureViwer.state.length}
        </Text>
      </View>
    )
  }
}
