'use strict';

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Navigator,
  Dimensions,
  BackAndroid,
  TouchableOpacity,
  PixelRatio,
  Alert,
  ScrollView,
  Image,
  Modal,
  Platform,
  InteractionManager,
  UIManager,
  LayoutAnimation,
  ListView,
  ToastAndroid,
} from 'react-native';

const {height, width} = Dimensions.get('window');
import Icon from 'react-native-vector-icons/FontAwesome';
export default class MessagePage extends Component{
  constructor(props){
    super(props);
    this.ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2,
      sectionHeaderHasChanged : (s1, s2) => s1 !== s2,
    });

    this.state={
      dataSource:this.ds.cloneWithRowsAndSections(this.formatData([])),
      lenght:0,
    }
  }
  componentDidMount(){
    InteractionManager.runAfterInteractions(()=>{
      this.getMessageData();
    });
  }
  getMessageData = () =>{
    var request = new XMLHttpRequest();
    request.onload = ()=>{
      if(request.status === 200){
        var result = request.responseText;

        //////console.log(result);
        if(result.length > 0){
          result = JSON.parse(result);
        }else{
          result = JSON.parse("[]");
        }
        LayoutAnimation.configureNext(this.CustomLayoutLinear);
        this.setState({length:result.length,dataSource:this.ds.cloneWithRowsAndSections(this.formatData(result))});
      }
    }
    request.onerror = (e)=>{
      ////console.log(e);
    }
    request.open('POST','http://54.179.154.17/getMessage.php',true);
    request.send();

  }

  CustomLayoutLinear = {
    duration: 150,
    create: {
      type: LayoutAnimation.Types.linear,
      property: LayoutAnimation.Properties.opacity,
    },
    update: {
      type: LayoutAnimation.Types.linear,
    },
  };

  updateMessage = (mid,state) =>{
    var request = new XMLHttpRequest();
    request.onload = ()=>{
      if(request.status === 200){
        var result = request.responseText;
        if(result.includes("success")){
          this.getMessageData();
        }
      }
    }
    request.onerror = (e)=>{
      ////console.log(e);
    }
    request.open('POST','http://54.179.154.17/updateMessage.php',true);
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    request.send(`message_id=${mid}&state=${state}`);
  }

  renderRow = (rowData) =>{
    return(
      <View
        style={styles.messageContainer}
      >
        <Image
          source={{uri:"http://54.179.154.17/upload/coachPropic/"+rowData.propic}}
          style={styles.coachImg}
        />
        <View
          style={styles.contentCotanier}
        >
        <Text style={{textAlign:"right"}}>日期:{rowData.apply_time}</Text>
          <View style={{
            flexDirection:"row",
            justifyContent:"space-between",
            alignItems:"center"
          }}>
            <Text style={styles.messageCoachName}>{rowData.username}</Text>

          </View>

          <Text>導師{rowData.username}申請了你的個案(編號:{rowData.case_id})</Text>
          <View
             style={{
               flexDirection:"row",
             }}
          >
            <TouchableOpacity
              activeOpacity={0.8}
              style={styles.button}
              onPress={()=>{
                this.props.p.gotoPage({title:'InfoPage',navBarTitle:"導師資料",param:{coach_id:rowData.coach_id}});
              }}
            >
              <Icon
                style={{marginRight:2}}
                name="eye"
                color="#fff"
                size={15}
              />
              <Text style={styles.buttonText}>
                查看資料
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => this.updateMessage(rowData.message_id,"AT")}
              style={rowData.state == 'AT' ? [styles.button,{
                borderLeftWidth:1,
                borderColor:"#ffffff",
                backgroundColor:"#007edb"
              }]:[styles.button,{
                borderLeftWidth:1,
                borderColor:"#ffffff"
              }]}
            >
              <Icon
                style={{marginRight:2}}
                name="check"
                color="#fff"
                size={15}
              />
              <Text style={styles.buttonText}>
                接受
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => this.updateMessage(rowData.message_id,"RT")}
              style={rowData.state == 'RT' ? [styles.button,{
                borderLeftWidth:1,
                borderColor:"#ffffff",
                backgroundColor:"#007edb"
              }]:[styles.button,{
                borderLeftWidth:1,
                borderColor:"#ffffff"
              }]}
            >
              <Icon
                style={{marginRight:2}}
                name="close"
                color="#fff"
                size={15}
              />
              <Text style={styles.buttonText}>
                拒絕
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
  formatData = (data) =>{
    var dataBlob = {};

    dataBlob['新的訊息'] = [];
    dataBlob['已讀訊息'] = [];
    dataBlob['已接受的申請'] = [];
    dataBlob['已拒絕的申請'] = [];

    for(var i = 0;i< data.length;i++){
      if(data[i].state == "UD"){
        dataBlob['新的訊息'].push(data[i]);
      }
      else if(data[i].state == "RD"){
        dataBlob['已讀訊息'].push(data[i]);
      }
      else if(data[i].state == "AT"){
        dataBlob['已接受的申請'].push(data[i]);
      }
      else{
        dataBlob['已拒絕的申請'].push(data[i]);
      }
    }

    if(dataBlob['新的訊息'].length < 1)
      delete dataBlob['新的訊息'];
    if(dataBlob['已讀訊息'].length < 1)
      delete dataBlob['已讀訊息'];
    if(dataBlob['已接受的申請'].length < 1)
      delete dataBlob['已接受的申請'];
    if(dataBlob['已拒絕的申請'].length < 1)
      delete dataBlob['已拒絕的申請'];
    return dataBlob;

  }
  render(){
    return(
      this.state.length > 0?
      <View
        style={styles.container}
      >
      <ListView
        contentContainerStyle={{alignItems:"center"}}
        enableEmptySections={true}
        showsVerticalScrollIndicator={false}
        dataSource={this.state.dataSource}

        renderRow={(rowData,sec,rowId) => this.renderRow(rowData,rowId)}
        renderSectionHeader={ (sectionData,title) =>{
          return(
            <View
              style={{
                width:width,
                alignItems:"center",
                padding:10,
                borderBottomWidth:1,
                borderColor:'#b9b9b9'
              }}
            >
              <Text
                style={{fontSize:20}}
              >
                {title}
              </Text>
            </View>
          );
        }}
      />

      </View>:
      <View
        style={styles.container}
      >
        <Text style={{fontSize:25}}>
          沒有任何訊息
        </Text>

      </View>

    );
  }
}

const styles = StyleSheet.create({
  buttons:{
    flexDirection:"row",
    justifyContent:"space-between"
  },
  buttonText:{
    color:"#fff",
    fontSize:12
  },
  button:{
    flex:1,
    padding:3,
    flexDirection:"row",
    backgroundColor:"#50a2ff",
    justifyContent:"center",
    alignItems:"center"
  },
  contentCotanier:{
    flex:1,
  },
  messageCoachName:{
    color:"#000",
    fontSize:20,
  },
  messageContainer:{
    height:width*0.3,
    width:width,
    flexDirection:"row",
    alignItems:"center",
    padding:10,
    borderBottomWidth:1,
    borderColor:"#b9b9b9",
  },
  coachImg:{
    height:width*0.225,
    width:width*0.225,
    borderRadius:width*0.125,
    backgroundColor:"#fff",
    marginRight:5,
  },
  container:{
    flex:1,
    marginTop:55,
    backgroundColor:"#fff",
    justifyContent:"center",
    alignItems:"center"
  }
});
