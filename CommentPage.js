'use strict';

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  ViewPagerAndroid,
  Image,
  Dimensions,
  ListView,
  LayoutAnimation,
  InteractionManager,
  PixelRatio,
  Alert,
  Modal,
  TextInput,
  UIManager,
  Animated
} from 'react-native';
import Toast from 'react-native-simple-toast';
const {height, width} = Dimensions.get('window');
import commentPageData from './commentPageData.js';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/Ionicons';
import Icon3 from 'react-native-vector-icons/MaterialIcons';
var  Immutable = require('immutable');
var toQueryString = (obj) => {
    return obj ? Object.keys(obj).sort().map(function (key) {
        var val = obj[key];
        if (Array.isArray(val)) {
            return val.sort().map(function (val2) {
                return key + '=' + val2;
            }).join('&');
        }
        return key + '=' + val;
    }).join('&') : '';
}


export default class CommentPage extends Component {
  constructor(props){
    super(props);
    this.state={
      coach_id:null,
      firstLoading:true,
    }
    //UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
  }

  render(){
    return(
        <View
          style={styles.container}
        >
          <View
            style={styles.upperPart}
          >
            <Rating
              ref="Rating"
              p={this}
              pp={this.props.p}
            />
          </View>
          <View
            style={styles.newBtnContainer}
          >
            <TouchableOpacity
              onPress={ () => this.refs.postComment.checkAllowPostComment()}
              style={styles.newButton}
            >
              <Icon3
                name="insert-comment"
                color="#4ea2d7"
                size={30}
                style={styles.icon}
              />
              <Text>
                新增評語
              </Text>
            </TouchableOpacity>
          </View>

          <View style={{flex:1,alignItems:"center"}}>
            <CommentList
              ref="CommentList"
              p={this}
              pp={this.props.p}
            />
          </View>

          <Icon
            style={styles.commentsIcon}
            name="commenting"
            color="#fff"
            size={120}
          />
          <PostComment
            ref="postComment"
            p={this}
            pp={this.props.p}
          />
        </View>
    );
  }
}

class PostComment extends Component{
  constructor(props){
    super(props)
    this.state={
      visibleModal:false,
      newComment:""
    }
  }
  checkAllowPostComment = () =>{
    if(!this.props.pp.props.p.state.userData)
    {
      Toast.show('登入後才可發表評論', Toast.SHORT);
      return;
    }
    else if(this.props.pp.props.p.state.userData.user_type == 1){
      this.setState({visibleModal:true});
    }
    else{
      Toast.show('你不是學生，不能發表評論', Toast.SHORT);
      return;
    }
  }
  postNewComment = () => {
    var request = new XMLHttpRequest();
    request.onreadystatechange = () => {
      if(request.readyState !== 4)
        return;
      ////console.log(request.status);
      if(request.status === 200){
        var result = request.responseText;
        ////console.log(result);
        //result = JSON.parse(result);
        if(result.includes("success")){
          this.props.p.refs.CommentList.getData();
        }
      }
    }
    request.open('POST','http://54.179.154.17/postNewComment.php',true);
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    var param ={newComment:this.state.newComment,...this.props.pp.state.param,...this.props.pp.props.p.state.userData};
    param = toQueryString(param);
    ////console.log(param);
    request.send(param);
  }

  render(){
    return(
      <Modal
        animationType="slide"
        contentContainerStyle={styles.ratingModalContainer}
        visible={this.state.visibleModal}
        transparent={true}
        onRequestClose={ () => null}
      >
        <View
          style={styles.newCmModalInnerContainer}
        >
          <View
            style={styles.newCmContainer}
          >
            <View
              style={styles.newCmHeader}
            >
              <TouchableOpacity
                onPress={ () => {
                  this.setState({visibleModal:false});
                  this.setState({newComment:""});
              }}
                style={{width:40}}
              >
                <Icon2
                  name="ios-arrow-back"
                  color="#fff"
                  size={40}
                />
              </TouchableOpacity>
              <Text
                style={{flex:1,textAlign:"center",color:"#fff",fontSize:20}}
              >
                評語
              </Text>
            </View>
            <View
              style={styles.newCmTextInputContianer}
            >
              <TextInput
                value={this.state.newComment}
                style={{height:height*0.32,textAlign: 'left', textAlignVertical: 'top'}}
                multiline={true}
                placeholder="在此輸入評語"
                placeholderTextColor="#b5b5b5"
                onChangeText={(newComment) => this.setState({newComment})}
              />
            </View>
            <View
              style={styles.newCmOKContianer}
            >
              <Icon.Button
                onPress={
                  () => {
                    this.postNewComment();
                    this.setState({newComment:"",visibleModal:false});
                  }
                }
                name="send"
                color="#fff"
                size={20}
                style={{justifyContent:"center",backgroundColor:"#63b9ff"}}
              >
                發出
              </Icon.Button>
            </View>
          </View>

        </View>
      </Modal>
    );
  }
}

class CommentRow extends Component{
  constructor(props){
    super(props);
    this.state={
      fadeAnim : new Animated.Value(0),
      rebound: new Animated.Value(0)
    }
  }

  componentDidMount(){
    Animated.timing(
      this.state.fadeAnim,
      {
        toValue: 1,
        duration: 300,
      }
    ).start();
  }
  updateRow = () =>{
    Animated.spring(
      this.state.rebound,
      {
        toValue: 1,
        friction: 3,
      }
    ).start(
      () => {
        Animated.timing(
          this.state.rebound,
          {
            toValue: 0,
            duration: 0,
          }
        ).start();
      }
    );
  }
  componentWillReceiveProps(nextProps,nextState){
    if(this.props.rowData.comment_id != nextProps.rowData.comment_id){
      this.updateRow();
    }
  }

  render(){
    return(
      <Animated.View
        style={[styles.commentContainer,{
          opacity:this.state.fadeAnim,
          transform: [
            {scaleY:this.state.rebound.interpolate({
                inputRange:[0,0.5,1],
                outputRange: [1,1.2,1],
              })
            }
          ]
        }
        ]}
      >
        <View
          style={styles.textContainer}
        >
          <View style={styles.iconTextContainer}>
            <Icon
              name="user"
              color="#33b6f7"
              size={13}
              style={styles.icon}
            />
            <Text>
              {this.props.rowData.username}
            </Text>
          </View>
          <View style={styles.iconTextContainer}>
            <Icon
              name="calendar"
              color="#33b6f7"
              size={13}
              style={styles.icon}
            />
            <Text>
              {this.props.rowData.time.split(" ")[0]}
            </Text>
          </View>
          <View style={styles.iconTextContainer}>
            <Icon
              name="clock-o"
              color="#33b6f7"
              size={13}
              style={styles.icon}
            />
            <Text>
              {this.props.rowData.time.split(" ")[1]}
            </Text>
          </View>
        </View>
        <View>
          <Text>
            {this.props.rowData.review}
          </Text>
        </View>
        <View
          style={styles.likeContainer}
        >
          <Text>
            共{this.props.rowData.total_like}人讚好
          </Text>
          <LikeButton
            p={this}
            pp={this.props.p}
            ppp={this.props.pp}
            comment_id={this.props.rowData.comment_id}
            is_liked={this.props.rowData.is_liked}
          />
        </View>
      </Animated.View>
    );
  }

}

class CommentList extends Component{
  constructor(props){
    super(props);
    var ds = new ListView.DataSource({
      rowHasChanged:(r1, r2) => {
        return !Immutable.is(Immutable.Map(r1),Immutable.Map(r2));
      }
    });
    this.state={
      dataSource:ds.cloneWithRows([]),
      length:null,
    }
  }
  componentDidMount() {
    InteractionManager.runAfterInteractions(() => {
      this.getData();
    });
  }

  getData = () => {
    var request = new XMLHttpRequest();
    request.onreadystatechange = () => {
      if(request.readyState !== 4)
        return;
        //console.log(request.status);
      if(request.status === 200){
        var result = request.responseText;
        //console.log(result);
        result = JSON.parse(result);
        this.setState({length:result.length,dataSource:this.state.dataSource.cloneWithRows(result)});
      }
    }
    request.open('POST','http://54.179.154.17/getCoachComment.php',true);
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    if(this.props.pp.props.p.state.userData){
      var param = toQueryString({...this.props.pp.state.param,student_id:this.props.pp.props.p.state.userData.student_id});
    }
    else{
      var param = toQueryString({...this.props.pp.state.param,student_id:null});
    }

    ////console.log(param);
    request.send(param);
  }

  render(){
    return(
      this.state.length == 0 ?
      <View style={styles.emptyCommentMsgContainer}>
        <Text style={{fontSize:20}}>
          暫時沒有評語。
        </Text>
      </View>
      :
      <ListView
        ref="ListView"
        showsVerticalScrollIndicator={false}
        enableEmptySections={true}
        contentContainerStyle={{padding:10}}
        dataSource={this.state.dataSource}
        renderRow={(rowData) => <CommentRow p={this} pp={this.props.p} rowData={rowData}/>}
        renderSeparator={(sid,rid,ad) => {
          return(<View style={styles.line} key={rid}></View>);
        }}
      />
    )
  }
}


class LikeButton extends Component{
  constructor(props){
    super(props);
    this.state={
      is_like:this.props.is_liked,
      rebound:new Animated.Value(0),
      color:this.props.is_liked == 1 ? "#fff" : "rgba(255, 255, 255, 0.6)",
    }
  }
  likeAndRenew(){
    if(!this.props.ppp.props.p.props.p.state.userData)
    {
      Toast.show('登入後才可讚', Toast.SHORT);
      return;
    }
    else if(this.props.ppp.props.p.props.p.state.userData.user_type == 1){
      Animated.spring(
        this.state.rebound,
        {
          toValue: 1,
          friction : 5,
        }
      ).start(
        () =>{
          Animated.timing(
            this.state.rebound,
            {
              toValue: 0,
              duration : 0,
            }
          ).start();

        }
      );
      var request = new XMLHttpRequest();
      request.onload= () =>{

        if(request.status === 200){
          var result = request.responseText;
          //console.log(result);
          if(result.includes("success")){
            this.props.pp.getData();
          }
        }
        else{
          //console.log("Something went wrong.");
        }
      }
      request.onerror= () =>{
        //console.log("ERROR");
      }
      request.open('POST','http://54.179.154.17/comment_like.php',true);
      request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      var param = {comment_id:this.props.comment_id,student_id:this.props.ppp.props.p.props.p.state.userData.student_id}
      param = toQueryString(param);
      //console.log(param);
      request.send(param);
    }
    else{
      Toast.show('只有學生才可以讚', Toast.SHORT);
      return;
    }
  }

  render(){
    this.state.color=this.props.is_liked == 1 ? "#fff" : "rgba(255, 255, 255, 0.6)";
    return(
      <TouchableOpacity
        onPress={ () =>{
          this.likeAndRenew();

        }}
        activeOpacity={1}
        style={styles.likeButton}
      >
        <Animated.View
          style={{height:20,transform:[{
            scale:this.state.rebound.interpolate({
              inputRange: [0,0.5,1],
              outputRange: [1,2,1],
            })
          }]}}
        >
          <Icon
            ref="Icon"
            name="thumbs-up"
            color={this.state.color}
            size={20}
            style={styles.icon}
          />
        </Animated.View>

        <Text style={{color:this.state.color}}>
          Like
        </Text>
      </TouchableOpacity>
    );
  }
}


class Rating extends Component{
  constructor(props){
    super(props);
    this.state={
      visibleModal:false,
      param:{
        myRating:0,
        avgRating:0,
      }
    }
  }
  getRating = (myRating) =>{
    var request = new XMLHttpRequest();
    request.onload = ()=>{
      var result = request.responseText;
      //console.log(result);
      if(request.status===200){
        result = JSON.parse(result);
        if(result){

          this.setState({param:result});
        }
        else{

          this.setState({param:{
            myRating:0,
            avgRating:0,
          }});
        }


      }

    }
    request.onerror= () =>{
      //console.log(request.responseText);
    }
    request.open('POST','http://54.179.154.17/rating.php',true);
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    if(myRating){
      var param = {...this.props.pp.state.param,myRating:myRating}
    }
    else {
      var param = {...this.props.pp.state.param}
    }
    param = toQueryString(param);
    //console.log(param);
    request.send(param);
  }
  componentDidMount(){
    InteractionManager.runAfterInteractions(()=>{
      this.getRating();
    });
  }
  checkAllowRating = () =>{
    if(!this.props.pp.props.p.state.userData)
    {
      Toast.show('登入後才可以評分', Toast.SHORT);
      return;
    }
    else if(this.props.pp.props.p.state.userData.user_type == 1){
      this.setState({visibleModal:true});
    }
    else{
      Toast.show('學生才可以評分', Toast.SHORT);
      return;
    }
  }
  render(){
    return(
      <TouchableOpacity
      activeOpacity={0.8}
        onPress={() => this.checkAllowRating()}
        style={styles.starContainer}
      >
        <Text style={{color:"#fff",marginRight:5,}}>
          點此評分
        </Text>
        <Icon
          style={styles.star}
          name={
            this.state.param.avgRating-1 >= 0 ?
            "star"
            :
            (this.state.param.avgRating-1 >= -0.5? "star-half-full":"star-o")
          }
          size={25}
          color="yellow"
        />
        <Icon
          style={styles.star}
          name={
            this.state.param.avgRating-2 >= 0 ?
            "star"
            :
            (this.state.param.avgRating-2 >= -0.5? "star-half-full":"star-o")
          }
          size={25}
          color="yellow"
        />
        <Icon
          style={styles.star}
          name={
            this.state.param.avgRating-3 >= 0 ?
            "star"
            :
            (this.state.param.avgRating-3 >= -0.5? "star-half-full":"star-o")
          }
          size={25}
          color="yellow"
        />
        <Icon
          style={styles.star}
          name={
            this.state.param.avgRating-4 >= 0 ?
            "star"
            :
            (this.state.param.avgRating-4 >= -0.5? "star-half-full":"star-o")
          }
          size={25}
          color="yellow"
        />
        <Icon
          style={styles.star}
          name={
            this.state.param.avgRating-5 >= 0 ?
            "star"
            :
            (this.state.param.avgRating-5 >= -0.5? "star-half-full":"star-o")
          }
          size={25}
          color="yellow"
        />
        <Modal
          animationType="slide"
          contentContainerStyle={styles.ratingModalContainer}
          visible={this.state.visibleModal}
          transparent={true}
          onRequestClose={ () => this.setState({visibleModal:false})}
        >
        <View style={styles.ratingModalInnerContainer}>
          <View style={styles.selectRatingContainer}>
          <Text
            onPress={() => this.setState({visibleModal:false})}
            style={{color:'#fff',marginRight:5,}}
          >取消
          </Text>
          <TouchableOpacity
            activeOpacity={0.9}
            onPress={ () => {

                this.setState({param:{avgRating:this.state.param.avgRating,myRating:1}});
                setTimeout( () =>{
                  this.setState({visibleModal:false});
                }, 300)
                //console.log("before send");
                //console.log(this.state.param.myRating);
                this.getRating(1);
                ////console.log(this.state.param);
              }
            }
          >
          <Icon
            style={styles.star}
            name={
              this.state.param.myRating-1 >= 0 ? "star":"star-o"
            }
            size={40}
            color="yellow"
          />
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.9}
            onPress={ () => {
                this.setState({param:{avgRating:this.state.param.avgRating,myRating:2}});
                setTimeout( () =>{
                  this.setState({visibleModal:false});
                }, 300)
                this.getRating(2);
                ////console.log(this.state.param);
              }
            }
          >
          <Icon
            style={styles.star}
            name={
              this.state.param.myRating-1 >= 1 ? "star":"star-o"
            }
            size={40}
            color="yellow"
          />
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.9}
            onPress={ () => {

                this.setState({param:{avgRating:this.state.param.avgRating,myRating:3}});
                setTimeout( () =>{
                  this.setState({visibleModal:false});
                }, 300)
                this.getRating(3);
                ////console.log(this.state.param);
              }
            }
          >
          <Icon
            style={styles.star}
            name={
              this.state.param.myRating-1 >= 2 ? "star":"star-o"
            }
            size={40}
            color="yellow"
          />
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.9}
            onPress={ () => {

                this.setState({param:{avgRating:this.state.param.avgRating,myRating:4}});
                setTimeout( () =>{
                  this.setState({visibleModal:false});
                }, 300)
                this.getRating(4);
                ////console.log(this.state.param);
              }
            }
          >
          <Icon
            style={styles.star}
            name={
              this.state.param.myRating-1 >= 3 ? "star":"star-o"
            }
            size={40}
            color="yellow"
          />
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.9}
            onPress={ () => {

                this.setState({param:{avgRating:this.state.param.avgRating,myRating:5}});
                setTimeout( () =>{
                  this.setState({visibleModal:false});
                }, 300)
                this.getRating(5);
                ////console.log(this.state.param);
              }
            }
          >
          <Icon
            style={styles.star}
            name={
              this.state.param.myRating-1 >= 4 ? "star":"star-o"
            }
            size={40}
            color="yellow"
          />
          </TouchableOpacity>

          </View>
        </View>
        </Modal>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  emptyCommentMsgContainer:{
    flex:1,
    justifyContent:"center",
    alignItems:"center",
  },
  newCmOKContianer:{
    flex:1,
    justifyContent:"center",
    alignItems:"flex-end",
    padding:10,

  },
  newCmTextInputContianer:{
    height:height*0.32,
    padding:10,
    backgroundColor:"#fff",
    paddingTop:10
  },
  newCmHeader:{
    flex:1,
    flexDirection:"row",
    backgroundColor:'#63b9ff',
    alignItems:"center",
    padding:5,
    paddingLeft:10
  },
  newCmContainer:{
    height:height*0.5,
    width:width*0.9,
    backgroundColor:"#fff"
  },
  newCmModalInnerContainer:{
    height:height,
    width:width,
    backgroundColor:"rgba(0, 0, 0, 0.5)",
    justifyContent:"center",
    alignItems:"center"
  },
  selectRatingContainer:{
    marginTop:55,
    flexDirection:"row",
    alignItems:'center',
    justifyContent:"center",

  },
  ratingModalInnerContainer:{
    height:height,
    width:width,
    alignItems:"flex-end",
    padding:10,
    backgroundColor:"rgba(0, 0, 0, 0.5)"
  },
  ratingModalContainer:{
    justifyContent:"center",
    alignItems:"center",
  },
  starContainer:{
    flexDirection:"row",
    alignItems:'center',
  },
  star:{
    marginRight:2
  },
  newBtnContainer:{
    height:width/3/2,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'flex-end',
    padding:10,
  },
  newButton:{
    height:width/3/2*0.7,
    padding:10,
    backgroundColor:"#fff",
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    borderRadius:10
  },
  likeContainer:{
    marginTop:10,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-between'
  },
  likeButton:{
    height:30,
    padding:10,
    flexDirection:'row',
    alignItems:"center",
    justifyContent:"center",
    borderRadius:5,
    backgroundColor:"#4ea2d7"
  },
  icon:{
    flex:1,
    marginRight:3
  },
  iconTextContainer:{
    flexDirection:'row',
    alignItems:"center",
  },
  textContainer:{
    flexDirection:'row',
    justifyContent:"space-between",
    marginBottom:5
  },
  commentContainer:{
    width:width*0.9,
    backgroundColor:"#fff",
    borderRadius:5,
    padding:10,
    elevation:3,
  },
  line:{
    height:20,
  },
  upperPart:{
    height:height*0.15,
    backgroundColor:"#29ace4",
    justifyContent:"flex-end",
    alignItems:"flex-end",
    padding:10,
  },
  commentsIcon:{
    position:"absolute",
    top:height*0.03,
    left:width*0.05,
    backgroundColor:"transparent"
  },
  userImg:{
    height:width/3,
    width:width/3,
    borderRadius:width/3/2,
    position:"absolute",
    top:height*0.15-width/3/2,
    left:width*0.1,
    borderWidth:2,
    borderColor:"#fff"
  },
  container:{
    flex:1,
    backgroundColor:"#dfdfdf",
  }

});
