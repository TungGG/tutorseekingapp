'use strict'

import  React, {Component} from 'react';
import{
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Navigator,
  Image,
  TouchableOpacity,
  Dimensions,
  TextInput,
  Alert
} from 'react-native';


var {height, width} = Dimensions.get('window');
const height = height-55;

var styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems:"center",
  },
  logoStyle:{
    top:50,
    height:width*0.5,
    width:width*0.5,
  },
  iconStyle:{
    height:height*0.08,
    width:width*0.08,
  },
  emailContainer : {
    height:height*0.08,
    width:width*0.7,
    flexDirection:'row',
    marginTop:30,
    alignItems:"center",
  },
  passwordContainer : {
    height:height*0.08,
    width:width*0.7,
    flexDirection:'row',
    marginTop:20,
    alignItems:"center",
  },
  buttonContainer:{
    height:height*0.08,
    width:width*0.4,
    alignItems:"center",
    justifyContent:'center',
    marginTop:20,

  },
  forgetPasswordContainer:{
    height:height*0.06,
    width:width*0.4,
    marginTop:10,
    alignItems:"center",
    justifyContent:'center',
  },
  orContainer : {
    height:height*0.06,
    width:width*0.8,
    flexDirection:'row',
    marginTop:10,
    alignItems:'center',
    justifyContent:'center',

  },
  line:{
    flex:1,
    height:1,
    backgroundColor:"#000",
  },
  otherMethodContainer : {
    height:height*0.08,
    width:width*0.7,
    flexDirection:'row',
    marginTop:height*0.03,
    alignItems:'center',
    justifyContent:'center',
  },
  facebookButtonContainer:{
    height:height*0.08,
    width:width*0.3,
    flexDirection:'row',

  },
  googleButtonContainer:{
    height:height*0.08,
    width:width*0.3,
    flexDirection:'row',
  },
  ConditionButtonContainer:{
    height:height*0.04,
    width:width*0.2,
    marginTop:10,
    alignItems:'center',
  },
  submitButton:{
      height:height*0.08,
      width:width*0.4,
      alignItems:"center",
      justifyContent:'center',
      backgroundColor:"#409ee0",
  }
})

import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/Entypo';

export default class LoginPage extends Component{
    constructor(props){
      super(props);
      this.state={
        email:"",
        password:""
      }
    }

    jumpBack = () => {
      const currentRouteStack = this.props.p.refs.navigator.getCurrentRoutes();
      const stack = currentRouteStack[0].stack;
      stack.pop();
      this.props.p.gotoPage(stack[stack.length-1],true);
      //console.log("stack:",stack);
      //console.log("currentRouteStack:",currentRouteStack);
    }

    submit = () => {
      var email = this.state.email;
      var password = this.state.password;
      var re=/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if(this.state.email&&this.state.password){
        if(re.test(email)){
          var request = new XMLHttpRequest();
          request.onreadystatechange = (e) =>{
            if (request.readyState !== 4) {
              return;
            }
            if(request.status === 200){
              var result = request.responseText;
              //console.log(request.responseText);
              if(result.includes('fail')){
                Alert.alert("錯誤","登入失敗，請輸入正確的電郵及密碼。");
              }
              else{
                result = JSON.parse(result);
                //console.log(result);
                if(result['user_type'] == 1){
                  this.props.p.slideMenuContent.refs.loginPart.setState({userID:result['student_id'],userData:result});
                  this.props.p.setState({userData:result});
                  //this.props.p.refs.navigator.pop();
                  //this.props.p.gotoPage({title:"DetailManagementPage",navBarTitle:"管理我的介紹"});
                  this.jumpBack();
                }
                else{
                  this.props.p.slideMenuContent.refs.loginPart.setState({userID:result['coach_id'],userData:result});
                  this.props.p.setState({userData:result});
                  //this.props.p.refs.navigator.pop();
                  //this.props.p.gotoPage({title:"DetailManagementPage",navBarTitle:"管理我的介紹"});
                  this.jumpBack();
                }
                Alert.alert("成功","登入成功");

              }
            }
            else { console.warn('error'); }
          }
          request.open('POST', 'http://54.179.154.17/login.php', true);
          request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
          var param = `email=${email}&password=${password}`;
          request.send(param);
        }else{
          Alert.alert("錯誤","請輸入正確的電郵及密碼!");
        }
      }
      else{
        Alert.alert("錯誤","請輸入電郵及密碼!");
      }


    }

    render(){
        return(
          <View style={styles.container}>
            <Image
              style={{flex:1, alignItems:'center',width:width}}
              resizeMode="contain"
              source={require('./img/LoginPage/background.png')}
              >
                <Image
                  style={styles.logoStyle}
                  source={require('./img/plivo_icon.png')}
                  resizeMode="contain"
                />

                <View  style={styles.emailContainer}>
                    <Icon2
                      name="mail"
                      size={20}
                      color="#000"

                    />
                    <TextInput
                      style={{flex:1,left:width*0.03,}}
                      keyboardType="email-address"
                      placeholder="電郵"
                      returnKeyType="next"
                      onSubmitEditing={
                        () => this.refs.passwordInput.focus()
                      }
                      onChangeText={(email) => this.setState({email})}
                      value={this.state.email}
                      ref="emailInput"
                    />
                </View>

                <View  style={styles.passwordContainer}>
                  <Icon2
                    name="lock"
                    size={20}
                    color="#000"
                  />
                  <TextInput
                    style={{flex:1,left:width*0.03,}}
                    placeholder="密碼"
                    secureTextEntry={true}
                    ref="passwordInput"
                    returnKeyType="go"
                    onChangeText={(password) => this.setState({password})}
                    value={this.state.password}
                    onSubmitEditing={
                      () => this.submit()
                    }
                  />
                </View>
                <SubmitButton
                  ref="submitBnt"
                  p={this}
                />
                <ForgetPassword />
                <View style={styles.orContainer}>
                  <View style={styles.line}></View>
                  <Text style={{fontSize:20,margin:10}}>OR</Text>
                  <View style={styles.line}></View>
                </View>
                <View style={styles.otherMethodContainer}>
                  <Text>
                    Coming soon
                  </Text>
                </View>
                <ConditionButton />
            </Image>
          </View>

        )
    }
}




class SubmitButton extends Component{
      constructor(props){
        super(props);
      }
     /*handlePress =() =>{
       if(this.state.enabled){
         this.props.p.refs.pictureViwer.back();
       }
     };*/

     render(){
       return(
          <View
           style={styles.buttonContainer}
          >
            <Icon2.Button
              onPress={() => this.props.p.submit()}
              name="login"
              color="#fff"
              size={20}
              activeOpacity={0.8}
              style={styles.submitButton}
            >
              登入
            </Icon2.Button>
          </View>

       )
     }
}

class ForgetPassword extends Component{
      constructor(props){
        super(props);
      }

     /*handlePress =() =>{
       if(this.state.enabled){
         this.props.p.refs.pictureViwer.back();
       }
     };*/

     render(){
       return(
           <TouchableOpacity
           style={styles.forgetPasswordContainer}>
               <Text style={{fontSize:15,color:"#000"}}>忘記密碼</Text>
          </TouchableOpacity>

       )
     }
}

class FacebookButton extends Component{
      constructor(props){
        super(props);
      }

     /*handlePress =() =>{
       if(this.state.enabled){
         this.props.p.refs.pictureViwer.back();
       }
     };*/

     render(){
       return(
         <Icon.Button
          name="facebook"
          size={20}
          color="#fff"
         >
         Facebook
         </Icon.Button>

       )
     }
}

class GoogleButton extends Component{
      constructor(props){
        super(props);
      }

     /*handlePress =() =>{
       if(this.state.enabled){
         this.props.p.refs.pictureViwer.back();
       }
     };*/

     render(){
       return(
         <Icon.Button
          name="google"
          size={20}
          color="#fff"
          style={{backgroundColor:"red"}}
         >
         Google
         </Icon.Button>

       )
     }
}

class ConditionButton extends Component{
      constructor(props){
        super(props);
      }

     /*handlePress =() =>{
       if(this.state.enabled){
         this.props.p.refs.pictureViwer.back();
       }
     };*/

     render(){
       return(
           <TouchableOpacity
           style={styles.ConditionButtonContainer}>
               <Text style={{fontSize:15,color:"#fff"}}>合約條款</Text>
          </TouchableOpacity>

       )
     }
}
