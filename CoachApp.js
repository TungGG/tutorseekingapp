'use strict';

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Navigator,
  Dimensions,
  BackAndroid,
  TouchableOpacity,
  PixelRatio,
  Alert,
  Image,
  UIManager,
  LayoutAnimation
} from 'react-native';
import Drawer from 'react-native-drawer';


import MainPage from './MainPage.js';
import SlideMenu from './SlideMenu.js';
import InfoPage from './InfoPage.js';
import LoginPage from './LoginPage.js';
import AboutUsPage from './AboutUsPage.js';
import BasicRegistrationPage from './BasicRegistrationPage.js';
import CoachRegistrationPage from './CoachRegistrationPage.js';
import StudentRegistrationPage from './StudentRegistrationPage.js';
import CasesListPage from './CasesListPage.js';
import CoachListPage from './CoachListPage.js';
import ProfilePage from './ProfilePage.js';
import StudentProfilePage from './StudentProfilePage.js';
import CommentPage from './CommentPage.js';
import DetailManagementPage from './DetailManagementPage.js';
import CaseManagementPage from './CaseManagementPage.js';
import MessagePage from './MessagePage.js';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon2 from 'react-native-vector-icons/MaterialIcons';


const {height, width} = Dimensions.get('window');
const ratio = PixelRatio.get();

export default class untitledProject extends Component {
  constructor(props){
    super(props);
    this.routeStack = [];
    this.state={
      userID:null,
      userData:null
    }
    var _this = this;
    this.NavigationBarRouteMapper = {
      LeftButton:(route, navigator, index, navState) => {
        // menu button in navigationBar
        return (
          <TouchableOpacity
            activeOpacity={0.6}
            style={styles.menuBtnContainerBack}
            onPress={this.back}
          >
            <Icon
              name="ios-arrow-back-outline"
              color="#fff"
              size={40}
            />
          </TouchableOpacity>
        )

          /*
          <Image
            resizeMode="contain"
            style={styles.menuBtn}
            source={require("./img/menu_btn.png")}/>
          */


      },
      RightButton:(route, navigator, index, navState) => {
        // some component or null
        return(
          <TouchableOpacity
            onPress={() => this.refs.slideMenu.open()}
            style={styles.menuBtnContainer}
          >
            <Icon
              name="md-menu"
              color="#fff"
              size={40}
            />
          </TouchableOpacity>
        );
      },

      Title(route, navigator, index, navState) {
        // Title in navigationBar
        return (
          <View
            style={styles.titleContainer}
          >
            <Text
              style={[styles.title,{color:'#fff'}]}
            >
              {route.navBarTitle}
            </Text>
          </View>
        )
      }
    }
    UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
  }



  _renderScene = (route, navigator) => {
    var _this = this;
    if(route.title == 'MainPage'){
      this.routeStack.push(route.title);
      return(

        //<CoachRegistrationPage/>

        /*<ProfilePage
        p={this}
        />*/

        /*<DetailManagementPage
          p={this}
        />*/

        //<ProfilePage/>

        //<StudentRegistrationPage/>
        <MainPage
          ref={(c) => this["MainPage"] = c}
          p={this}
        />
      );
    }
    else if(route.title == 'InfoPage'){
      return(
        <InfoPage
          ref={(c) => this["InfoPage"] = c}
          p={this}
          param={route.param}
        />
      );
    }
    else if(route.title == 'LoginPage'){
      return(
        <LoginPage
          p={this}
        />
      );
    }
    else if(route.title == 'BasicRegistrationPage'){
      return(
        <BasicRegistrationPage
          p={this}
        />
      );
    }
    else if(route.title == 'CoachListPage'){
      return(
        <CoachListPage
          ref={(c) => this["CoachListPage"] = c}
          p={this}
          param={route.param}
        />
      );
    }
    else if(route.title == 'CasesListPage'){
      return(
        <CasesListPage
        ref={(c) => this["CasesListPage"] = c}
        p={this}
        param={route.param}

        />
      );
    }
    else if(route.title == 'CoachRegistrationPage'){
      return(
        <CoachRegistrationPage
          p={this}
        />
      );
    }
    else if(route.title == 'ProfilePage'){
      return(
        <ProfilePage
          ref={(c) => this['ProfilePage'] = c}
          p={this}
        />
      );
    }
    else if(route.title == 'DetailManagementPage'){
      return(
        <DetailManagementPage
          ref={(c) => this['DetailManagementPage'] = c}
          p={this}
        />
      );
    }
    else if(route.title == 'StudentRegistrationPage'){
      return(
        <StudentRegistrationPage
          p={this}
        />
      );
    }

    else if(route.title == 'StudentProfilePage'){
      return(
        <StudentProfilePage
          ref={(e) => this['StudentProfilePage'] = e}
          p={this}
        />
      )
    }
    else if(route.title == 'CaseManagementPage'){
      return(
        <CaseManagementPage
          ref={(e) => this['CaseManagementPage'] = e}
          p={this}
        />
      );
    }
    else if(route.title == 'MessagePage'){
      return(
        <MessagePage
          ref={(e) => this['MessagePage'] = e}
          p={this}
        />
      );
    }
    else if(route.title == 'AboutUsPage'){
      return(
        <AboutUsPage/>
      );
    }
  }
  gotoPage = (page,pop) => {
    const currentRouteStack = this.refs.navigator.getCurrentRoutes();
    let historyPosition = 0;
    let histroyRoute;
    let stack = currentRouteStack[0].stack;
    currentRouteStack.some((item, index) => {
      if (page.title === item.title) {
        historyPosition = index;
        histroyRoute = currentRouteStack[index];
        return true;
      }
    });

    if (page.title !== stack[stack.length - 1].title){
      stack.push(page);
    }

    if (histroyRoute) {
      let nextRoute = currentRouteStack[historyPosition];
      if(page.param){
        this[`${page.title}`].setState({param:page.param});
      }
      this.refs.navigator.jumpTo(nextRoute,pop);
    } else {
      this.refs.navigator.push(page);
    }
  };

  _onPress = (page) => {
    this.gotoPage(page);
  }

  componentDidMount() {
    this.checkLogin();
    var navigator = this.refs.navigator;
    const currentRouteStack = navigator.getCurrentRoutes();
    if(currentRouteStack[0].stack){
      currentRouteStack[0].stack.push({title:"MainPage",navBarTitle:"主頁"});
    }
    else{
      currentRouteStack[0].stack = [];
      currentRouteStack[0].stack.push({title:"MainPage",navBarTitle:"主頁"});
    }

    const stack = currentRouteStack[0].stack;
    BackAndroid.addEventListener('hardwareBackPress', () => {
      return this.back();
    });
  }

  back = () => {
    var navigator = this.refs.navigator;
    const currentRouteStack = navigator.getCurrentRoutes();
    const stack = currentRouteStack[0].stack;
    if (navigator && stack.length > 1) {
        stack.pop();
        const prevRouteTitle = stack[stack.length - 1].title;
        var prevRoutePosition;
        currentRouteStack.some((item, index) => {
          if (item.title === prevRouteTitle) {
            prevRoutePosition = index;
            return true;
          }
        });

        try {
          navigator.jumpTo(currentRouteStack[prevRoutePosition]);
        } catch (err) {
          navigator.pop();
        }
        return true;
    }
    return false;
  }

  checkLogin = () => {
    var request = new XMLHttpRequest();
    request.onreadystatechange = (e) =>{
      if (request.readyState !== 4) {
        return;
      }
      if(request.status === 200){
        var result = request.responseText;
        //console.log(request.responseText);
        if( !result || result.includes('fail')){
          return
        }
        else{
          result = JSON.parse(result);
          //console.log(result);
          if(result['user_type'] == 1){
            this.slideMenuContent.refs.loginPart.setState({userID:result['student_id'],userData:result});
            this.setState({userData:result});
          }
          else{
            this.slideMenuContent.refs.loginPart.setState({userID:result['coach_id'],userData:result});
            this.setState({userData:result});
          }
        }
      }
      else {
        //some error message
        null;
      }

    }
    request.open('POST', 'http://54.179.154.17/login.php', true);
    request.send();
  }


  render() {

    return (
      <Drawer
        type="overlay"
        side="right"
        panOpenMask={0.05}
        tapToClose={true}
        ref='slideMenu'
        openDrawerOffset={0.3}
        content={
            <SlideMenu ref={(c) =>this.slideMenuContent=c} p={this}/>
        }
      >
        <Navigator
          ref='navigator'
          initialRouteStack={
            [
              {title:'InfoPage',navBarTitle:"導師資料",param:{coach_id:46}},
              {title: 'MainPage',navBarTitle:"主頁"},
            ]
          }
          renderScene={ (route, navigator) => this._renderScene(route, navigator)}
          navigationBar={
            <Navigator.NavigationBar
              style={styles.navBar}
              routeMapper={this.NavigationBarRouteMapper}
              p={this}
            />
          }
          configureScene={() => ({
            ...Navigator.SceneConfigs.PushFromRight,
            gestures: {},
          })}
        />
      </Drawer>
    );
  }
}



const styles = StyleSheet.create({
  title:{
    fontSize:18,
  },
  titleContainer:{
    flex:1,
    justifyContent:'center',
    alignItems:'center',
  },
  menuBtn:{
    height:27,
    width:27,
  },
  menuBtnContainerBack:{
    width:55,
    flex:1,
    justifyContent:'center',
    alignItems:"center",

    left:-3,
  },
  menuBtnContainer:{
    width:55,
    flex:1,
    justifyContent:'center',

    borderColor:"#0273bb",
    alignItems:"center",
    marginRight:-3,
  },
  navBar:{
    backgroundColor:"#2aade5",
    height:55,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 6,
  },
});
