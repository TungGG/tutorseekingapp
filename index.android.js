'use strict';

import React, { Component } from 'react';
import {
  AppRegistry,
} from 'react-native';
import CoachApp from './CoachApp.js';


AppRegistry.registerComponent('untitledProject', () => CoachApp);
