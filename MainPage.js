'use strict';

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  ViewPagerAndroid,
  Image,
  Dimensions,
  ListView,
  LayoutAnimation,
  InteractionManager,
  PixelRatio,
  Alert,
  RefreshControl,
  UIManager,
} from 'react-native';
import Toast from 'react-native-simple-toast';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/Octicons';
const {height, width} = Dimensions.get('window');
const ratio = PixelRatio.get();





import viewPagerData from './viewPagerData.js';
import topRatingCoachData from './topRatingCoachData.js';
import topRatingTeacherData from './topRatingTeacherData.js';
import mostPopularTutData from './mostPopularTutData.js';
import newestCourseData from './newestCourseData.js';
import newestCaseData from './newestCaseData.js';
import newestEmploymentData from './newestEmploymentData.js';

import {IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator} from 'rn-viewpager';


export default class MainPage extends Component{
  constructor(props){
    super(props);
    this.state={
      isRefreshing: false,
    }
    //UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
  }
  _onRefresh = () =>{
    this.setState({isRefreshing: true});
    this.refs.TopRatingCoach.getTopRatingData();
    this.refs.TopRatingTeacher.getTopRatingData();
    this.refs.NewestCase.getNewestCases();
    //LayoutAnimation.configureNext(LayoutAnimation.Presets.linear);
    this.setState({isRefreshing: false});
  }
  render(){
    return(
      <ScrollView
        refreshControl={
          <RefreshControl
            progressViewOffset={-20}
            refreshing={this.state.isRefreshing}
            onRefresh={this._onRefresh}
            colors={['#0084ff']}
            progressBackgroundColor="#fff"
          />
        }
        showsVerticalScrollIndicator={false}
        style={styles.pageContainer}
      >
        <AdvViewPager/>
        <TopRatingCoach
          ref="TopRatingCoach"
          p={this}
          pp={this.props.p}
        />
        <TopRatingTeacher
          ref="TopRatingTeacher"
          p={this}
          pp={this.props.p}
        />
        <NewestCase
          ref="NewestCase"
          p={this}
          pp={this.props.p}
        />
      </ScrollView>
    );
  }

}

/*class AdvViewPager extends Component{
  constructor(props){
    super(props);
    var ds = new ViewPager.DataSource({
      pageHasChanged: (p1, p2) => p1 !== p2,
    });
    this.state={
      dataSource: ds.cloneWithPages(viewPagerData),
    }
  }

  _renderPage = (data, id) => {
    return(
    <View
      style={styles.flexAndRow}
    >
      <TouchableOpacity>
        <Image
          style={styles.pagerImg}
          resizeMode="cover"
          source={{uri:data.img}}
        >
        <View
          style={styles.pagerTextContainer}
        >
          <Text
            style={styles.pagerText}
          >
            {data.title}
          </Text>
        </View>
        </Image>
      </TouchableOpacity>
    </View>
    )
  }

  render(){
    return(
      <ViewPager
        ref="advViewPager"
        initialPage={0}
        dataSource={this.state.dataSource}
        renderPage={this._renderPage}
        isLoop={true}
        autoPlay={true}
      />
    );
  }

}*/

class AdvViewPager extends Component {
  constructor(props){
    super(props);
    this.state={
      position:0,

    };

    this.data=this.generatePage(viewPagerData);
  }

  generatePage = (data) => {
    var pages = [];
    for(let i = 0; i < data.length;i++){
      pages.push(
        <View
          key={i}
          style={styles.flexAndRow}
        >
          <TouchableOpacity>
            <Image
              style={[styles.pagerImg,{width}]}
              resizeMode="cover"
              source={{uri:data[i].img}}
            >
            <View
              style={styles.pagerTextContainer}
            >
              <Text
                style={styles.pagerText}
              >
                {data[i].title}
              </Text>
            </View>

            </Image>
          </TouchableOpacity>
        </View>
      );

    }
    return pages;
  }

  onPageSelected = (e) =>{
    this.setState({position:e.position})
  }

  onPageScrollStateChanged = (state) =>{
    if(state == 'idle' && !this.timer){
      this.timer = setInterval( () => {
        if(this.state.position < viewPagerData.length-1){
          this.refs.viewPager.setPage(this.state.position+1);
          this.setState({position:this.state.position+1});
        }
        else{
          this.refs.viewPager.setPage(0);
          this.setState({position:0});
        }
      }, 3000);
    }else{
      clearInterval(this.timer);
      this.timer = null;
    }
  }

  componentDidMount() {
    this.timer = setInterval(() => {
      if(this.state.position < viewPagerData.length-1){
        this.refs.viewPager.setPage(this.state.position+1);
        this.setState({position:this.state.position+1});
      }
      else{
        this.refs.viewPager.setPage(0);
        this.setState({position:0});
      }
    }, 3000);
  }

  componentWillUnmount(){
    clearInterval(this.timer);
    this.timer = null;
  }




  render(){

    return(
      <View>
        <IndicatorViewPager
          onPageSelected={ (e) => this.onPageSelected(e)}
          onPageScrollStateChanged={(state) => this.onPageScrollStateChanged(state)}
          ref="viewPager"
          style={styles.pagerContainer}
          indicator={
            <PagerDotIndicator
              pageCount={this.data.length}
              style={{bottom: 15}}
              dotStyle={{backgroundColor: '#FFFFFF88'}}
            />
          }
        >
          {this.data}
        </IndicatorViewPager>

      </View>
      /*
      <ViewPagerAndroid
        initialPage={0}
        style={styles.pagerContainer}
        onPageSelected={(e) => this.onPageSelected(e)}
        ref='viewPager'
        onPageScrollStateChanged={(e) => this.onPageScrollStateChanged(e)}
      >

      </ViewPagerAndroid>
      <DotTab
        ref="dotTab"
      />
      */
    )
  }
}

class DotTab extends Component{
  constructor(props){
    super(props);
    this.state={
      position:0
    };
    this.dotTabs = [];
  }


  generateTab = (num) => {
    var tabs = [];
    for(let i = 0; i < num;i++){
      i == 0?
      tabs.push(
        <View
          key = {i}
          style={{
            width: 7,
            height: 7,
            borderRadius: 7/2,
            backgroundColor: 'white',
            margin:3
          }}
          ref={(c) => {this.dotTabs.push(c)}}
        >
        </View>
      ):
      tabs.push(
        <View
          key = {i}
          style={styles.circle}
          ref={(c) => {this.dotTabs.push(c)}}
        >
        </View>
      );
    }
    return tabs;
  }

  changeDot = (num) => {
    this.dotTabs[this.state.position].setNativeProps({
      style:{
        width: 6,
        height: 6,
        borderRadius: 6/2,
        backgroundColor: 'rgba(180, 180, 180, 0.5)',
      }
    });
    this.setState({position:num});
    this.dotTabs[num].setNativeProps({
      style:{
        width: 7,
        height: 7,
        borderRadius: 7/2,
        backgroundColor: 'white'
      }
    });

  }
  render(){
    return(
      <View
        style={styles.circleContainer}
      >
        {this.generateTab(viewPagerData.length)}
      </View>
    );
  }
}

class TopRatingCoach extends Component{
  constructor(props){
    super(props);
    this.ds = new ListView.DataSource({
      rowHasChanged:(r1, r2) => r1 !== r2
    });
    this.state={
      isLoading:true,
      dataSource:this.ds.cloneWithRows([])
    }
    this.listviews = [];
    this.previousIndex = 0;
    this.currentIndex = 0;
    this.getTopRatingData();

  }

  getTopRatingData= () => {
    var request = new XMLHttpRequest();
    request.onreadystatechange = () => {
      if(request.readyState !== 4)
        return;
      if(request.status === 200){
        var result = request.responseText;
        ////////console.log(result);
        result = JSON.parse(result);
          //LayoutAnimation.configureNext(LayoutAnimation.Presets.linear);
        this.setState({dataSource:this.ds.cloneWithRows(result)});
      }
    }
    request.open('POST','http://54.179.154.17/getTopRatingCoach.php',true);
    request.send();
  }

  _onPress = (coach_id) => {

    this.props.pp.gotoPage({title:'InfoPage',navBarTitle:"導師資料",param:{coach_id:coach_id}});
  };

  renderRow = (rowData,rowId) => {

    return(
      <TouchableOpacity
        onPress={() => this._onPress(rowData.coach_id)}
      >
        <Image
          ref={(c) => c && (this.listviews[rowId] = c)}
          resizeMode="cover"
          style={rowId == 0 ? [styles.coachImg,{
            height:height/5+30,
            width:width/2-15+40,
          }]:styles.coachImg}
          source={{uri:"http://54.179.154.17/upload/coachPropic/"+rowData.propic}}
        >
          <View
            style={styles.coachImgMask}
          >
          <View
            style={{
              flexDirection:"row",
              justifyContent:"space-between",
            }}
          >
            <Text style={{color:"#fff"}}>{rowData.username}</Text>

          </View>
          <View
          >
            <Rating
              numOfStars={rowData.avgRating}
            />
        </View>
          </View>
        </Image>
      </TouchableOpacity>
    )
  };

  onPressCoach = () => {
    var param={
      type:2,
      category1:1,
      category2:-1,
      category3:-1,
      district1:-1,
      district2:-1,
      sortByRating:true
    }
    ////////console.log(param);
    this.props.pp.gotoPage({title:"CoachListPage",navBarTitle:"導師列表",param:param});
  }

  CustomLayoutLinear = {
    duration: 150,
    create: {
      type: LayoutAnimation.Types.linear,
      property: LayoutAnimation.Properties.opacity,
    },
    update: {
      type: LayoutAnimation.Types.linear,
    },
  };

  onScroll = (event) =>{
    var itemWidth = width/2-15;
    this.currentIndex = parseInt((event.nativeEvent.contentOffset.x+(width/2-5))/(width/2));
    if(this.currentIndex != this.previousIndex){
      LayoutAnimation.configureNext(this.CustomLayoutLinear);
      this.listviews[this.currentIndex].setNativeProps({
        style:{
          height:height/5+30,
          width:width/2-15+40,
        }
      });
      this.listviews[this.previousIndex].setNativeProps({
        style:{
          height:height/5,
          width:width/2-15,
        }
      });
    }
    this.previousIndex = parseInt((event.nativeEvent.contentOffset.x+(width/2-5))/(width/2));
    ////////console.log((event.nativeEvent.contentOffset.x+165)/180);
  }


  render(){
    return(
      <View>
        <View style={styles.titleTextContainer}>
          <Text style={styles.titleText}>好評教練</Text>
          <Text
            style={{fontSize:15}}
            onPress={() => this.onPressCoach()}
          >更多</Text>
        </View>
        <ListView
          initialListSize={3}
          ref="ListView"
          contentContainerStyle={{alignItems:"center"}}
          onScroll={(even) => this.onScroll(even)}
          enableEmptySections={true}
          style={{marginRight:10}}
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          dataSource={this.state.dataSource}
          renderRow={(rowData,sec,rowId) => this.renderRow(rowData,rowId)}
        />
      </View>
    );
  }
}

class Rating extends Component{
  constructor(props){
    super(props);
  }
  render(){

    return(
      <View
        style={styles.starImg}
      >
        <Icon
          style={{marginLeft:1}}
          name={
            this.props.numOfStars-1 >= 0 ?
            "star"
            :
            (this.props.numOfStars-1 >= -0.5? "star-half-full":"star-o")
          }
          size={15}
          color="yellow"
        />
        <Icon
          style={{marginLeft:1}}
          name={
            this.props.numOfStars-2 >= 0 ?
            "star"
            :
            (this.props.numOfStars-2 >= -0.5? "star-half-full":"star-o")
          }
          size={15}
          color="yellow"
        />
        <Icon
          style={{marginLeft:1}}
          name={
            this.props.numOfStars-3 >= 0 ?
            "star"
            :
            (this.props.numOfStars-3 >= -0.5? "star-half-full":"star-o")
          }
          size={15}
          color="yellow"
        />
        <Icon
          style={{marginLeft:1}}
          name={
            this.props.numOfStars-4 >= 0 ?
            "star"
            :
            (this.props.numOfStars-4 >= -0.5? "star-half-full":"star-o")
          }
          size={15}
          color="yellow"
        />
        <Icon
          style={{marginLeft:1}}
          name={
            this.props.numOfStars-5 >= 0 ?
            "star"
            :
            (this.props.numOfStars-5 >= -0.5? "star-half-full":"star-o")
          }
          size={15}
          color="yellow"
        />
      </View>

    );
  }
}

class Bookmark extends Component{
  constructor(props){
    super(props);
    this.state={
      bookmarked:false
    }
  }

  onPress = () => {
    this.setState({bookmarked:!this.state.bookmarked});
  }
  render(){
    return(
      <TouchableOpacity
        activeOpacity={1}
        onPress={() => this.onPress()}
      >
        <Icon
          name={this.state.bookmarked? "bookmark":"bookmark-o"}
          color={this.state.bookmarked? "#009cff":"#fff"}
          size={30}
        />
      </TouchableOpacity>

    );
  }
}

class TopRatingTeacher extends Component{
  constructor(props){
    super(props);
    this.ds = new ListView.DataSource({
      rowHasChanged:(r1, r2) => r1 !== r2
    });
    this.state={
      dataSource:this.ds.cloneWithRows([])
    }
    this.listview = [];
    this.previousIndex = 0;
    this.currentIndex = 0;
    this.getTopRatingData();
  }

  getTopRatingData= () => {
    var request = new XMLHttpRequest();
    request.onreadystatechange = () => {
      if(request.readyState !== 4)
        return;
      if(request.status === 200){
        var result = request.responseText;
        ////////console.log(result);
        result = JSON.parse(result);
        //LayoutAnimation.configureNext(LayoutAnimation.Presets.linear);
        this.setState({dataSource:this.ds.cloneWithRows(result)});
      }
    }
    request.open('POST','http://54.179.154.17/getTopRatingTeacher.php',true);
    request.send();
  }
  _onPress = (coach_id) => {
    this.props.pp.gotoPage({title:'InfoPage',navBarTitle:"導師資料",param:{coach_id:coach_id}});
  };
  renderRow = (rowData,rowId) => {
    return(
      <TouchableOpacity
        onPress={() => this._onPress(rowData.coach_id)}
      >
        <Image
          ref={(c) => c && (this.listview[rowId] = c)}
          resizeMode="cover"
          style={rowId == 0 ? [styles.coachImg,{
            height:height/5+30,
            width:width/2-15+40,
          }]:styles.coachImg}
          source={{uri:"http://54.179.154.17/upload/coachPropic/"+rowData.propic}}
        >
          <View
            style={styles.coachImgMask}
          >
            <View
              style={{
                flexDirection:"row",
                justifyContent:"space-between"
              }}
            >
              <Text style={{color:"#fff"}}>{rowData.username}</Text>

            </View>
            <View
              >
              <Rating
                numOfStars={rowData.avgRating}
                />
            </View>
          </View>

        </Image>

      </TouchableOpacity>
    )
  };

  onPressCoach = () => {
    var param={
      type:2,
      category1:2,
      category2:-1,
      category3:-1,
      district1:-1,
      district2:-1,
      sortByRating:true
    }
    ////////console.log("MainPage");
    ////////console.log(param);
    this.props.pp.gotoPage({title:"CoachListPage",navBarTitle:"導師列表",param:param});
  }
  CustomLayoutLinear = {
    duration: 150,
    create: {
      type: LayoutAnimation.Types.linear,
      property: LayoutAnimation.Properties.opacity,
    },
    update: {
      type: LayoutAnimation.Types.linear,
    },
  };
  onScroll = (event) =>{
    var itemWidth = width/2-15;
    this.currentIndex = parseInt((event.nativeEvent.contentOffset.x+(width/2-5))/(width/2));
    //////console.log(this.currentIndex);
    //////console.log(this.previousIndex);
    //////console.log(this.listview.length);
    if(this.currentIndex != this.previousIndex){
      LayoutAnimation.configureNext(this.CustomLayoutLinear);
      this.listview[this.currentIndex].setNativeProps({
        style:{
          height:height/5+30,
          width:width/2-15+40,
        }
      });
      this.listview[this.previousIndex].setNativeProps({
        style:{
          height:height/5,
          width:width/2-15,
        }
      });
    }
    this.previousIndex = parseInt((event.nativeEvent.contentOffset.x+(width/2-5))/(width/2));
    ////////console.log((event.nativeEvent.contentOffset.x+165)/180);
  }

  render(){
    return(
      <View>
        <View style={styles.titleTextContainer}>
          <Text style={styles.titleText}>好評導師</Text>
          <Text
          style={{fontSize:15}}
          onPress={() => this.onPressCoach()}
          >更多</Text>
        </View>
        <ListView
          contentContainerStyle={{alignItems:"center"}}
          onScroll={ (event) => this.onScroll(event)}
          enableEmptySections={true}
          style={{marginRight:10}}
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          dataSource={this.state.dataSource}
          renderRow={(rowData,sec,rowId) => this.renderRow(rowData,rowId)}
        />
      </View>
    );
  }
}

class MostPopularTut extends Component{
  constructor(props){
    super(props);

    var ds = new ListView.DataSource({
      rowHasChanged:(r1, r2) => r1 !== r2
    });
    this.state={
      dataSource:ds.cloneWithRows(mostPopularTutData)
    }
  }

  renderRow = (rowData) => {
    return(
      <TouchableOpacity
        style={styles.tutContainer}
      >
        <Image
          style={styles.tutImg}
          source={{uri:rowData.img}}
          resizeMode="cover"
        />
        <View
          style={styles.tutDesContainer}
        >
          <Image
            style={styles.uploderImg}
            resizeMode="contain"
            source={{uri:rowData.userImg}}
          />
          <View style={styles.tutTextContainer}>
            <View style={{flexDirection:'row',justifyContent:'space-between'}}>
            <Text style={styles.tutTitle}>
              {rowData.title}
              </Text>
              <Bookmark
                ref='bookmark'
                color="#afafaf"
              />
            </View>

            <Text>{rowData.uploader}</Text>
            <View style={{
              flexDirection:'row',
              justifyContent:'space-between'
            }}>
              <Text >{rowData.date}</Text>
              <Text>瀏覽次數: {rowData.views}</Text>
            </View>
          </View>
        </View>


      </TouchableOpacity>
    )
  };

  shouldComponentUpdate(){
    return false;
  }

  render(){
    return(
      <View>
        <View style={styles.titleTextContainer}>
          <Text style={styles.titleText}>最受歡迎教學</Text>
          <Text style={{fontSize:15}}>更多</Text>
        </View>
        <ListView
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          dataSource={this.state.dataSource}
          renderRow={(rowData) => this.renderRow(rowData)}
        />
      </View>
    );
  }

}

class NewestCourse extends Component{
  constructor(props){
    super(props);

    var ds = new ListView.DataSource({
      rowHasChanged:(r1, r2) => r1 !== r2
    });
    this.state={
      dataSource:ds.cloneWithRows(newestCourseData)
    }
  }

  renderRow = (rowData) => {
    return(
      <View style={styles.courseContainer}>
        <Image
          style={styles.courseImg}
          source={{uri:rowData.img}}
        />
        <View
          style={styles.courseTextContainer}
        >
          <Text style={styles.courseTextTitle}>{rowData.title}</Text>
          <View style={{flexDirection:'row'}}>
            <View>
              <Text>導師:{rowData.teacher}</Text>
              <Text>地區:{rowData.district}</Text>
              <Text>時間:{rowData.time}</Text>
              <Text>每週堂數:{rowData.perWeek}</Text>
            </View>
            <View>
              <Text>時限:{rowData.deadline}</Text>
              <Text>費用:{rowData.fee}</Text>
              <Text>用品:{rowData.material}</Text>
              <Text style={styles.courseDetailBnt}>詳情>></Text>
            </View>
          </View>


        </View>

      </View>
    )
  };

  shouldComponentUpdate(){
    return false;
  }

  render(){
    return(
      <View>
        <View style={styles.titleTextContainer}>
          <Text style={styles.titleText}>最新課程</Text>
          <Text style={{fontSize:15}}>更多</Text>
        </View>
        <ListView
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          dataSource={this.state.dataSource}
          renderRow={(rowData) => this.renderRow(rowData)}
        />
      </View>
    );
  }
}

class NewestCase extends Component{
  constructor(props){
    super(props);

    this.ds = new ListView.DataSource({
      rowHasChanged:(r1, r2) => r1 !== r2
    });
    this.state={
      dataSource:this.ds.cloneWithRows([]),
    }
    this.getNewestCases();
  }

  applyCase = (case_id) =>{
    if(!this.props.pp.state.userData)
    {
      Toast.show('登入後才可申請。', Toast.SHORT);
      return;
    }
    else if(this.props.pp.state.userData.user_type != 2){
      Toast.show('只有導師才可以申請。', Toast.SHORT);
      return;
    }
    var request = new XMLHttpRequest();
    request.onload= () => {

      if(request.status === 200){
        var result = request.responseText;

        if(result.includes('success')){
          Toast.show('已提交申請。', Toast.SHORT);
        }else{
          Toast.show(result, Toast.SHORT);
        }
        //////console.log(result);
          //LayoutAnimation.configureNext(LayoutAnimation.Presets.linear);
        //this.setState({dataSource:this.ds.cloneWithRows(result)});
      }
    }
    request.onerror = (e) => {
      //////console.log(e);
    }
    request.open("POST","http://54.179.154.17/sendApplyRequest.php",true);
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    request.send(`case_id=${case_id}`);
  }

  getNewestCases = () => {
    var request = new XMLHttpRequest();
    request.onload= () => {

      if(request.status === 200){
        var result = request.responseText;
        result = JSON.parse(result);
        //////console.log(result);
          //LayoutAnimation.configureNext(LayoutAnimation.Presets.linear);
        this.setState({dataSource:this.ds.cloneWithRows(result)});
      }
    }
    request.onerror = (e) => {
      //////console.log(e);
    }
    request.open("POST","http://54.179.154.17/getNewestCases.php",true);
    request.send();
  }

  renderRow = (rowData) => {
    return(
      <View style={styles.caseCotainer}>
        <View style={styles.newestCaseHeader}>
          <Text>日期:{rowData.post_time}
          </Text>
          <Icon
            name="thumb-tack"
            color="#e72b2b"
            size={25}
          />
        </View>
        <View style={styles.textContainer}>
          <View>
            <Text>項目:{rowData.event_name}</Text>
            <Text>形式:{rowData.size=='P' ? "個人":"團體"}</Text>
            <Text>程度:{rowData.level}</Text>
          </View>
          <View>
            <Text>時薪:{rowData.salary}</Text>
            <Text>性別:{rowData.gender == "M" ? "男":(rowData.gender == "F" ? "女":"有男有女")}</Text>
            <Text>年齡:{rowData.age}</Text>
          </View>
          <View>
            <Text>地區:{rowData.location_name}</Text>
            <Text>每週:{rowData.period}</Text>
            <Text>時間:{rowData.hour}</Text>
          </View>
        </View>
        <View style={styles.otherContainer}>
          <View style={{flex:1}}>
            <Text>其他要求:</Text>
            <Text>{rowData.requirement}</Text>
          </View>
            <Icon2.Button
              onPress={ () => this.applyCase(rowData.case_id)}
              name="repo-pull"
              color="#fff"
              size={15}
            >
              申請
            </Icon2.Button>
        </View>
      </View>
    )
  };

  onPressCase = () => {
    var param={
      type:1,
      category1:-1,
      category2:-1,
      category3:-1,
      district1:-1,
      district2:-1,
      sortByRating:false,

    }


    this.props.pp.gotoPage({title:"CasesListPage",navBarTitle:"個案列表",param:param});
  }


  render(){
    return(
      <View>
        <View style={styles.titleTextContainer}>
          <Text style={styles.titleText}>最新個案</Text>
          <Text
            style={{fontSize:15}}
            onPress={ ()=> this.onPressCase()}
          >更多</Text>
        </View>
        <ListView
          enableEmptySections={true}
          pagingEnabled={true}
          style={{marginRight:10}}
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          dataSource={this.state.dataSource}
          renderRow={(rowData) => this.renderRow(rowData)}
        />
      </View>
    );
  }
}

class NewestEmployment extends Component{
  constructor(props){
    super(props);

    var ds = new ListView.DataSource({
      rowHasChanged:(r1, r2) => r1 !== r2
    });
    this.state={
      dataSource:ds.cloneWithRows(newestEmploymentData)
    }
  }

  renderRow = (rowData) => {
    return(
      <View style={styles.caseCotainer}>
        <View style={{flex:1}}>
          <Text>項目:{rowData.type}</Text>
          <Text>地區:{rowData.district}</Text>
          <Text>時間:{rowData.time}</Text>

        </View>
        <View style={{flex:1,marginLeft:5}}>
          <Text>程度:{rowData.level}</Text>
          <Text>每週:{rowData.perWeek}</Text>
          <Text>薪金:{rowData.fee}</Text>
        </View>
        <View style={{flex:1}}>
          <Text>發佈:{rowData.releaseTime}</Text>

          <TouchableOpacity style={{alignItems:"flex-end"}}>
            <Icon
              name="arrow-circle-right"
              size={30}
              color="#33ceff"
            />
          </TouchableOpacity>

        </View>
      </View>
    )
  };

  shouldComponentUpdate(){
    return false;
  }

  render(){
    return(
      <View>
        <View style={styles.titleTextContainer}>
          <Text style={styles.titleText}>最新聘請</Text>
          <Text style={{fontSize:15}}>更多</Text>
        </View>
        <ListView
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          dataSource={this.state.dataSource}
          renderRow={(rowData) => this.renderRow(rowData)}
        />
      </View>
    );
  }
}



const styles = StyleSheet.create({
  otherContainer:{
    flex:1,
    flexDirection:"row",
    paddingTop:10,
    justifyContent:"space-between",
    alignItems:"flex-end",
  },
  newestCaseHeader:{
    flexDirection:"row",
    justifyContent:"space-between",
    alignItems:"center",
    marginBottom:5
  },
  textContainer:{
    flexDirection:"row",
    justifyContent:"space-between",
    borderBottomWidth:1/2,
    borderColor:"#ddd9d9",
    paddingBottom:10,
  },
  caseCotainer:{
    width:width-20,
    padding:10,
    marginLeft:10,
    marginBottom:10,
    backgroundColor:"#ffffff",
    elevation:2,
    borderWidth:1/2,
    borderColor:"#c3c3c3",
    borderRadius:5,
  },
  courseDetailBnt:{
    fontSize:15,
    color:"#000",
    textAlign:"right"
  },
  courseTextTitle:{
    fontSize:18,
    color:"#000",
  },
  courseTextContainer:{
    flexWrap:'wrap',
    padding:5,
  },
  courseContainer:{
    height:150,
    width:340,
    alignItems:'center',
    flexDirection:'row',
    borderWidth:1,
    borderColor:"rgb(209, 209, 209)",
    borderRadius:10,
    marginLeft:10,
    paddingLeft:5,
    marginBottom:10,
    backgroundColor:"#fff",
  },
  courseImg:{
    height:100,
    width:100,
  },
  tutTitle:{
    fontSize:18,
    color:"#000"
  },
  tutTextContainer:{
    flex:1,
    paddingLeft:5,

  },
  uploderImg:{
    height:60,
    width:60,

  },
  tutDesContainer:{
    flex:1,
    flexDirection:'row',
    height:70,
    width:340,
    alignItems:'center',
    padding:5,
  },
  tutImg:{
    height:height/2.5*0.7,
    width:width-20,
  },
  tutContainer:{
    height:height/2.5,
    width:width-20,
    marginLeft:10,
    marginBottom:10,
    backgroundColor:"#fff",
    elevation:3,
  },
  coachImgMask:{
    flex:1,
    padding:5,
    justifyContent:"space-between",
    backgroundColor:"rgba(0, 0, 0, 0.2)"

  },
  bookmarkImg:{
    height:24,
    width:24,
  },
  starImg:{
    height:18,
    width:90,
    flexDirection:"row",
    alignItems:'center'
  },
  coachImg:{
    height:height/5,
    width:width/2-15,
    marginLeft:10,

  },
  titleText:{
    fontSize:15,
  },
  titleTextContainer:{
    padding:10,
    flexDirection:'row',
    justifyContent:'space-between'
  },
  flexAndRow:{
    flex:1,
    flexDirection:'row',
  },
  pagerTextContainer:{
    height:60,
    backgroundColor:"rgba(0, 0, 0, 0.50)",
    padding:5
  },
  pagerText:{
    color:"#fff",
    fontSize:25,
  },
  pagerImg:{
    height:250,
    width:width,
    justifyContent:'flex-end'
  },
  pagerContainer:{
    height:250,
  },
  pageContainer:{
    flex:1,
    backgroundColor:"#fff",
    marginTop:55,
  },
  circleContainer:{
    flexDirection:'row',
    position:'absolute',
    left:(width-13*viewPagerData.length)/2,
    top:230,
    justifyContent:'center',
    alignItems:'center',
  },
  circle: {
    width: 6,
    height: 6,
    borderRadius: 6/2,
    backgroundColor: 'rgba(208, 208, 208, 0.5)',
    margin:3
  }
})
