export default topRatingCoachData = [
  {
    "name":"name1",
    "rating":5,
    "description":"description1",
    "img":"http://www.kids-play-soccer.com/images/coachingyouthsoccer.jpg"
  },
  {
    "name":"name2",
    "rating":4,
    "description":"description2",
    "img":"http://www.kids-play-soccer.com/images/coachingyouthsoccer.jpg"
  },
  {
    "name":"name3",
    "rating":3,
    "description":"description3",
    "img":"http://www.chickita.si/wp-content/uploads/2016/02/0805-mgmnt-coach-630x420.jpg"
  },
  {
    "name":"name4",
    "rating":2,
    "description":"description4",
    "img":"http://www.chickita.si/wp-content/uploads/2016/02/0805-mgmnt-coach-630x420.jpg"
  },
  {
    "name":"name5",
    "rating":1,
    "description":"description5",
    "img":"http://www.chickita.si/wp-content/uploads/2016/02/0805-mgmnt-coach-630x420.jpg"
  },


]
