'use strict'

import  React, {Component} from 'react';
import{
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Navigator,
  Image,
  TouchableOpacity,
  Dimensions,
  TextInput,
  Picker,
  ScrollView,
  Keyboard,
  Alert
} from 'react-native';


var {height, width} = Dimensions.get('window');
const height = height-55;

var styles = StyleSheet.create({
  container: {
    marginTop:55,
    flex: 1,
  },
  logo:{
    height:height*0.25,
    width:height*0.25,

  },
  TextInputContainer:{
    width:width*0.8,
    height:height*0.09,
    flexDirection:"row",
    alignItems:"center",
  },
  nextPageButton:{
    flexDirection:"row",
    alignItems:"center",
    justifyContent:"flex-end",
    padding:2,
  },
})

import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/Entypo';
export default class BasicRegistrationPage extends Component{
    constructor(props){
      super(props);
      this.state={
        userType:1,
        email:"",
        password:"",
        RTpassword:"",
        phone:"",
        username:"",
      };
    }

    componentWillMount () {
      this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', ()=>{
        this.refs.container.scrollTo({y:0});
      });
    }
    componentWillUnmount () {
      this.keyboardDidHideListener.remove()
    }
    sendRegisterRequest = () => {
      var request = new XMLHttpRequest();
      request.onreadystatechange = (e) =>{
        if (request.readyState !== 4) {
          return;
        }
        if(request.status === 200){
          var result = request.responseText;
          //console.log(request.responseText);
          if(result.includes("Success")){
            Alert.alert("正確","成功註冊帳戶，進入下一頁填寫其他資料");
            if(this.state.userType == 1){
              const currentRouteStack = this.props.p.refs.navigator.getCurrentRoutes();
              const stack = currentRouteStack[0].stack;
              stack.pop();
              this.props.p.gotoPage({title:"StudentRegistrationPage",navBarTitle:"學生註冊"},true);
            }
            else if(this.state.userType == 2){
              const currentRouteStack = this.props.p.refs.navigator.getCurrentRoutes();
              const stack = currentRouteStack[0].stack;
              stack.pop();
              this.props.p.gotoPage({title:"CoachRegistrationPage",navBarTitle:"導師註冊"},true);
            }
          }
          else{
            Alert.alert("錯誤",result);
          }

        }
        else {
          //console.warn('error'); 
        }
      }
      request.open('POST', 'http://54.179.154.17/register.php', true);
      request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      var parem = `email=${this.state.email}&password=${this.state.password}&phone=${this.state.phone}&userName=${this.state.username}&userType=${this.state.userType}`;
      request.send(parem);
    }

    checkInput = () => {
      var pattern = /^\d{8}$/;
      if(!this.state.email){
        Alert.alert("錯誤","請輸入電郵");
        return;
      }
      else if(!this.state.password){
        Alert.alert("錯誤","請輸入密碼");
        return;
      }
      else if(!this.state.RTpassword){
        Alert.alert("錯誤","請輸入確認密碼");
        return;
      }
      else if(!this.state.phone){
        Alert.alert("錯誤","請輸入電話");
        return;
      }
      else if(!this.state.username){
        Alert.alert("錯誤","請輸入用戶名稱");
        return;
      }
      var re=/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if(!re.test(this.state.email)){
        Alert.alert("錯誤","請輸入正確的電郵");
        return;
      }
      else if(this.state.password.length<8){
        Alert.alert("錯誤","密碼長度不能少於8");
        return;
      }
      else if(this.state.password != this.state.RTpassword){
        Alert.alert("錯誤","確認密碼與密碼不符");
        return;
      }
      else if(this.state.phone.length<8){
        Alert.alert("錯誤","電話長度不能少於8");
        return;
      }
      else if(!pattern.test(this.state.phone)){
        Alert.alert("錯誤","請輸入正確的電話號碼");
        return;
      }


      this.sendRegisterRequest();
    }
    render(){
        return(
          <ScrollView
            ref="container"
            style={styles.container}
            keyboardShouldPersistTaps={true}
            scrollEnabled={false}
            showsVerticalScrollIndicator={false}
          >

            <Image
              style={{
                flex:1,
                width:width,
                alignItems:"center"
              }}
              resizeMode="contain"
              source={require('./img/BasicRegistrationPage/background.png')}
            >
                <Image
                  style={styles.logo}
                  source={require("./img/plivo_icon.png")}
                  resizeMode="contain"
                  ref="logo"
                />
                <View
                  style={styles.TextInputContainer}
                >
                  <Text >
                    用戶類別:
                  </Text>
                  <Picker
                    style={{width:80}}
                    mode="dropdown"
                    onValueChange={(value) => this.setState({userType:value})}
                    selectedValue={this.state.userType}
                  >
                    <Picker.Item
                      label="學生" value={1}
                    />
                    <Picker.Item
                      label="導師" value={2}
                    />
                  </Picker>

                </View>

              <View
                style={styles.TextInputContainer}
              >
                <Icon2
                  name="mail"
                  color="gray"
                  size={20}
                />
                <TextInput
                  onChangeText={(email) => {this.setState({email})}}
                  value={this.state.email}
                  ref="email"
                  style={{marginLeft:5,flex:1}}
                  placeholder="電郵"
                  keyboardType="email-address"
                  returnKeyType="next"
                  onSubmitEditing={() => this.refs.password.focus()}

                />
              </View>
              <View
                style={styles.TextInputContainer}
              >
                <Icon2
                  name="lock-open"
                  color="gray"
                  size={20}
                />
                <TextInput
                  value={this.state.password}
                  onChangeText={(password) => this.setState({password})}
                  ref="password"
                  style={{marginLeft:5,flex:1}}
                  placeholder="密碼"
                  secureTextEntry={true}
                  returnKeyType="next"
                  onSubmitEditing={() => this.refs.RTpassword.focus()}
                />
              </View>

              <View
                style={styles.TextInputContainer}
              >
                <Icon2
                  name="lock"
                  color="gray"
                  size={20}
                />
                <TextInput
                  value={this.state.RTpassword}
                  onChangeText={(RTpassword) => this.setState({RTpassword})}
                  ref="RTpassword"
                  secureTextEntry={true}
                  style={{marginLeft:5,flex:1}}
                  placeholder="確認密碼"
                  returnKeyType="next"
                  onSubmitEditing={() => this.refs.phone.focus()}
                />
              </View>

              <View
                style={styles.TextInputContainer}
              >
                <Icon2
                  name="phone"
                  color="gray"
                  size={20}
                />
                <TextInput
                  value={this.state.phone}
                  onChangeText={(phone) => this.setState({phone})}
                  ref="phone"
                  style={{marginLeft:5,flex:1}}
                  placeholder="電話"
                  returnKeyType="next"
                  maxLength={8}
                  keyboardType="phone-pad"
                  onSubmitEditing={() => this.refs.username.focus()}
                />
              </View>
              <View
                style={styles.TextInputContainer}
              >
                <Icon2
                  name="user"
                  color="gray"
                  size={20}
                />
                <TextInput
                  value={this.state.username}
                  onChangeText={(username) => this.setState({username})}
                  ref="username"
                  style={{marginLeft:5,flex:1}}
                  placeholder="用戶名稱"
                  returnKeyType="go"
                  onSubmitEditing={() => this.checkInput()}
                />
              </View>


              <View
                style={[styles.TextInputContainer,{justifyContent:"flex-end"}]}
              >
                <Button
                  p={this}
                />
              </View>
            </Image>

          </ScrollView>

        )
    }
}


class Button extends Component{
      constructor(props){
        super(props);
      }

     /*handlePress =() =>{
       if(this.state.enabled){
         this.props.p.refs.pictureViwer.back();
       }
     };*/

     render(){
       return(
           <View
           style={styles.nextPageButton}
           >
            <Icon.Button
              onPress={() => this.props.p.checkInput()}
              name="forward"
              color="#fff"
              size={20}
              activeOpacity={0.8}
            >
                下一頁
            </Icon.Button>
          </View>

       )
     }
}
