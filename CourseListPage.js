/*'use strict'
import React , {Component} from 'react';
import{
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Navigator,
  Image,
  TouchableOpacity,
  Dimensions,
  ListView,
  Alert,

} from 'react-native';

var {height, width} = Dimensions.get('window');
const height = height-55;


var DATA=[
  {
   title :'How to choose a girl dfgfdgfd gdf?',
   available:0,
   view:0,
   bookmarked:'false',
   picture: 'http://www.nba-trade-rumors.com/images/nba-rumors-banner2.jpg'
 },
 {
  title :'Hsdfjdfshgkjdshgfdjsghdjsfkghdfskljghgd?',
  available:2,
  view:5,
  bookmarked:'false',
  picture: 'http://www.nba-trade-rumors.com/images/nba-rumors-banner2.jpg'
},{
 title :'I LOVE Ugdewgjhewighw?',
 available:2,
 view:20,
 bookmarked:'false',
 picture: 'http://www.nba-trade-rumors.com/images/nba-rumors-banner2.jpg'
},

];



var styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  listviewContainer:{
    flex:8.1,
  },
  tagButtonContainer:{
    flex:0.9,
    borderWidth:1,
    flexDirection:'row',
  },
  oneViewContainer:{
    height:(height/9*8.1)/2,
    width:width,
    borderWidth:1,
  },
  courseTitleBox:{
    height:((height/9*8.1)/2)/4,
    width:width,
    borderWidth:1,
    flexDirection:'row',
  },
  coursePictureBox:{
    height:((height/9*8.1)/2)/2,
    width:width,
    borderWidth:1,
  },
  courseViewCounterBox:{
    height:((height/9*8.1)/2)/4,
    width:width,
    borderWidth:1,
    flexDirection:'row',
  },
  bookmarkImage:{
    height:((height/9*8.1)/2)/4,
    width:((height/9*8.1)/2)/4,
  },
  userIconImage:{
    height:((height/9*8.1)/2)/4*0.7,
    width:((height/9*8.1)/2)/4*0.7,
    borderWidth:1,
    borderRadius:((height/9*8.1)/2)/4,
    top:((height/9*8.1)/2)/4*0.1,
    left:width*0.03,
  },
  usertext:{
    marginTop:((height/9*8.1)/2)/4*0.35,
    marginLeft:width*0.05,
  },
  viewImage:{
    height:((height/9*8.1)/2)/4*0.5,
    width:((height/9*8.1)/2)/4*0.5,
    borderWidth:1,
    top:((height/9*8.1)/2)/4*0.2,
    left:width*0.3,
  },
  viewtext:{
    marginTop:((height/9*8.1)/2)/4*0.35,
    marginLeft:width*0.3,
  },
  availableImage:{
    height:((height/9*8.1)/2)/4*0.5,
    width:((height/9*8.1)/2)/4*0.5,
    borderWidth:1,
    top:((height/9*8.1)/2)/4*0.2,
    left:width*0.03,
  },
  availabletext:{
    marginTop:((height/9*8.1)/2)/4*0.35,
    marginLeft:width*0.03,
  }


})


export default class CourseListPage extends Component{
  constructor(porps){
    super(porps);
    var ds = new ListView.DataSource({
      rowHasChanged: (row1, row2) => row1 !== row2,
    });
    this.state={
      dataSource: ds.cloneWithRows(DATA),
    };
}

renderView (course){
  return(
    <View style={styles.oneViewContainer}>
      <View style={styles.courseTitleBox}>
          <Text style={{backgroundColor:'red',width:width-((height/9*8.1)/2)/4,}}> {course.title}</Text>
          <Bookmark/>
      </View>
      <Image
            style={styles.coursePictureBox}
            resizeMode="contain"
            source={{uri:course.picture}}
      />
      <View style={styles.courseViewCounterBox}>
      <Image
            style={styles.userIconImage}
            resizeMode="contain"
            source={require('./image/courseList/user.jpeg')}
      />
      <Text style={styles.usertext}> 李老</Text>

      <Image
            style={styles.viewImage}
            resizeMode="contain"
            source={require('./image/courseList/view.png')}
      />
      <Text style={styles.viewtext}>{course.view}</Text>

      <Image
            style={styles.availableImage}
            resizeMode="contain"
            source={require('./image/courseList/available.png')}
      />
      <Text style={styles.availabletext}> avilable </Text>

      </View>
    </View>
  )
}


  render(){
    return(
      <View style={styles.container}>
        <ListView
        dataSource={this.state.dataSource}
        renderRow={this.renderView}
        style={styles.listviewContainer}
        />
      <View style={styles.tagButtonContainer}></View>
      </View>


    )
  }
}


class Bookmark extends Component{
      constructor(props){
        super(props);
        this.state={
          bookmarked:false,
        };
      }
      handlePress = () =>{
          this.setState({bookmarked:!this.state.bookmarked});
          //Alert.alert("hello");
          this.refs.bookmarkimg.setNativeProps({
            source:require('./image/courseList/bookmarkTrue.png')
          })
      }
      render(){
        return(
          <View>
            <TouchableOpacity
              onPress={this.handlePress}
              style={styles.bookmarkImage}
            >
            <Image
              ref='bookmarkimg'
              style={{height:((height/9*8.1)/2)/4,width:((height/9*8.1)/2)/4,}}
              source={this.state.bookmarked ?require('./image/courseList/bookmarkTrue.png') : require('./image/courseList/bookmarkFalse.png')}
              resizeMode="contain"
            />
          </TouchableOpacity>
          </View>

        )
      }
}

class ViewCounterBar extends Component{
  render(){
    return(
            <View style={styles.courseViewCounterBox}>
            <Image
                  style={styles.userIconImage}
                  resizeMode="contain"
                  source={require('./image/courseList/user.jpeg')}
            />
            <Text style={styles.usertext}> Sai Hoi</Text>

            <Image
                  style={styles.viewImage}
                  resizeMode="contain"
                  source={require('./image/courseList/view.png')}
            />
            <Text style={styles.viewtext}> 123 </Text>

            <Image
                  style={styles.availableImage}
                  resizeMode="contain"
                  source={require('./image/courseList/available.png')}
            />
            <Text style={styles.availabletext}> avilable </Text>

    </View>
    )
  }
}
*/
