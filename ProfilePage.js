'use strict'

import  React, {Component} from 'react';
import{
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Navigator,
  Image,
  TouchableOpacity,
  Dimensions,
  TextInput,
  Picker,
  ScrollView,
  Keyboard,
  Modal,
  ListView,
  Alert,
  InteractionManager,
  Platform,

} from 'react-native';

import ImagePicker from 'react-native-image-picker';
var {height, width} = Dimensions.get('window');
const height = height-55;
import Icon from 'react-native-vector-icons/Ionicons';
import Icon2 from 'react-native-vector-icons/FontAwesome';
import Icon3 from 'react-native-vector-icons/Entypo';
import Icon4 from 'react-native-vector-icons/Foundation';
import Checkbox from 'react-native-custom-checkbox';
import districtData from './getDistrict.js';
var styles = StyleSheet.create({
  container: {
    flex:1,
    marginTop:55,
  },
  fieldContainer:{
    flex:1,
    width:width,
    alignItems:'center',
  },
  questionContainer:{
    height:height*0.07,
    width:width*0.8,
    flexDirection:'row',
    alignItems:'center',
    margin:1,
  },
  nextPageContainer:{
    width:width,
    padding:10,
    justifyContent:"flex-end",
    alignItems:"flex-end"
  },
  nextPageButton:{
    height:height*0.07,
    width:width*0.25,
    flexDirection:'row',
    justifyContent:'center',
    alignItems:'center',
    backgroundColor:"#0197cf",

  },
  nextPageImage:{
    height:height*0.1,
    width:width*0.1,
  },
  textInputStyle:{
    flex:1,
  },
  textStyle:{
    fontSize:15,
  },
  selectionContainer:{
    flexDirection:"row",
    paddingBottom:5,
  },
  selectionBar:{
    flex:1,
    flexDirection:'row',
    alignItems:'center',
    paddingTop:5,
  },
  picker:{
    flex:1,
  },
  pickerContainer:{
    flex:1,
    borderWidth:1/2,
    paddingTop:-10,
    borderRadius:5,
    marginLeft:5,
    borderColor:"#000",
  },
  avaDistrictContainer:{
    flex:1,
    backgroundColor:"#fff",
    margin:20,
    borderColor:"#b6b6b6",
    borderWidth:1/2,
  },
  checkRowContainer:{
    flexDirection:"row",
    alignItems:"center",
    justifyContent:"space-between",
    borderBottomWidth:1,
    borderColor:"#9d9d9d",
    padding:10,
  },
  districtName:{
    fontSize:20,
  },
  districtTitle:{
    fontSize:22,
    color:"#000",
    textAlign:"center",
    margin:5
  },
  avatar: {
    borderRadius: 50,
    width: 100,
    height: 100
},
avatarContainer: {
    borderColor: '#000',
    borderWidth: 1 / 2,
    justifyContent: 'center',
    alignItems: 'center',
},
editIconStyle:{
  height:height*0.08,
  width:height*0.08,
  position:"absolute",
  left:width*0.88,
},
})




export default class ProfilePage extends Component{
    constructor(props){
      super(props);
      this.state ={
        isLoaded:false,
        ...this.props.p.state.userData,
        teaching_language:this.props.p.state.userData.teaching_language.slice(),
        available_district:this.props.p.state.userData.available_district.slice(),
        firstLoading:true,


        //Enabled:false,
        //touchableEnabled:true,

      };
      this.isCategoryLoaded = false;
      console.log(this.state.experience);
    }

    componentWillMount () {
      this.getCategory = (() => {
        var request = new XMLHttpRequest();
        var result = [];
        request.onreadystatechange = (e) =>{
          if (request.readyState !== 4) {
            return;
          }
          if(request.status === 200){
            result.push.apply(result,JSON.parse(request.responseText));
            this.isCategoryLoaded = true;
            this.setState({isLoaded:this.isCategoryLoaded});
          }
        }
        request.open('POST', 'http://54.179.154.17/getCategory.php', true);
        request.send();
        return result;

      })();

      this.getDistrict = (() => {
        return districtData;
      })();
    }
    componentWillUnmount () {
      //this.keyboardDidHideListener.remove()
    }
    componentDidMount(){
      InteractionManager.runAfterInteractions(() => {
        this.setState({firstLoading:false})
      });
    }

    checkInput = () => {
      console.log(this.state.available_district);
      if(!this.state.propic){
        Alert.alert("錯誤","請選擇相片。");
        return;
      }
      else if(!this.state.chinese_full_name){
        Alert.alert("錯誤","請輸入中文全名。");
        return;
      }
      else if(!this.state.eng_full_name){
        Alert.alert("錯誤","請輸入英文全名。");
        return;
      }
      else if(!this.state.age){
        Alert.alert("錯誤","請輸入年齡。");
        return;
      }
      else if(this.state.available_district.length < 1){
        Alert.alert("錯誤","請選擇可教授地區。");
        return;
      }
      else if(this.state.expert<0){
        Alert.alert("錯誤","請選擇專業項目。");
        return;
      }
      else if(!this.state.experience){
        Alert.alert("錯誤","請輸入教學年資。");
        return;
      }
      else if(!this.state.expected_fee){
        Alert.alert("錯誤","請輸入培訓費用。");
        return;
      }
      else if(!this.state.phone){
        Alert.alert("錯誤","電話號碼。");
        return;
      }


      var pattern = /^\d{1,3}$/;
      var pattern2 = /^\d{1,5}$/;
      var pattern3 = /^\d{8}$/;
      if(!pattern.test(this.state.age)){
        Alert.alert("錯誤","年齡只能輸入數字。");
        return;
      }
      if(!pattern3.test(this.state.phone)){
        Alert.alert("錯誤","請輸入八個數字的電話號碼。")
        return;
      }
      if(!pattern.test(this.state.experience)){
        Alert.alert("錯誤","年資只能輸入數字。");
        return;
      }
      if(!pattern2.test(this.state.expected_fee)){
        Alert.alert("錯誤","培訓費用只能輸入數字。");
        return;
      }

      this.sendRegisterRequest();
    }


    sendRegisterRequest = () =>{
      this.refs.button.setState({disabled:true});
      var request = new XMLHttpRequest();
      request.onreadystatechange = (e) =>{
        if (request.readyState !== 4) {
          return;
        }
        if(request.status === 200){
          var result = request.responseText;
          console.log(request.responseText);
          if(result.includes("Success")){
            if(this.state.propic.uri ){
              var data = new FormData();
              data.append("SelectedFile",this.state.propic);
              var request1 = new XMLHttpRequest();
              request1.onreadystatechange = () => {
                if(request1.readyState !== 4){
                  return;
                }
                if(request1.status === 200){
                  try {
                    var resp = JSON.parse(request1.response);
                  } catch (e){
                    var resp = {
                      status: 'error',
                      data: 'Unknown error occurred: [' + request1.responseText + ']'
                    };
                  }
                  if(resp.data.includes("success")){
                    this.props.p.checkLogin();
                    Alert.alert("正確","成功更改相片又及個人資料。");
                    this.props.p.gotoPage({title:"MainPage",navBarTitle:"主頁"});
                    /*const currentRouteStack = this.props.p.refs.navigator.getCurrentRoutes();
                    const stack = currentRouteStack[0].stack;
                    stack.pop();
                    this.props.p.gotoPage({title:"MainPage",navBarTitle:"主頁"});*/
                  }
                  else{
                    Alert.alert(resp.status + ': ' + resp.data);
                  }
                  //this.refs.button.setState({disabled:false});
                };
              }
              request1.open('POST', 'http://54.179.154.17/uploadCoachPhoto.php',true);
              request1.send(data);
            }else {
              this.props.p.checkLogin();
              Alert.alert("正確","成功更改資料");
              this.props.p.gotoPage({title:"MainPage",navBarTitle:"主頁"});
            }
            }
/*            else{
              this.refs.button.setState({disabled:false});
              Alert.alert("錯誤",result);
            }*/

          }
        else {
          console.warn('error');
          this.refs.button.setState({disabled:false});}
      }
      request.open('POST', 'http://54.179.154.17/profileUpdate.php', true);
      request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      var avaDistrict = this.state.available_district.toString();
      var language = this.state.teaching_language.toString();
      var parem = `chinese_full_name=${this.state.chinese_full_name}&eng_full_name=${this.state.eng_full_name}&gender=${this.state.gender}&age=${this.state.age}&phone=${this.state.phone}&expert=${this.state.expert}&experience=${this.state.experience}&teaching=${this.state.teaching_form}&employment=${this.state.type_of_employment}&fee=${this.state.expected_fee}&avaDistrict=${this.state.available_district}&language=${this.state.teaching_language}`;
      request.send(parem);
      //route page
      const currentRouteStack = this.props.p.refs.navigator.getCurrentRoutes();
      const stack = currentRouteStack[0].stack;
      stack.pop();
      this.props.p.gotoPage({title:"MainPage",navBarTitle:"主頁"});
    }
    selectPhotoTapped() {
      const options = {
        title:"選擇相片",
        cancelButtonTitle:"取消",
        takePhotoButtonTitle:"拍照",
        chooseFromLibraryButtonTitle:"從圖庫中選取",
        quality: 1.0,
        maxWidth: 300,
        maxHeight: 300,
        storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        var source;

        // You can display the image using either:
        //source = {uri: 'data:image/jpeg;base64,' + response.data, isStatic: true};

        //Or:
        if (Platform.OS === 'android') {
          source = {uri: response.uri,type:response.type,name:response.fileName, isStatic: true};
        } else {
          source = {uri: response.uri.replace('file://', ''), isStatic: true};
        }
        console.log(source);
        this.setState({
          propic: source
        });
      }
    });
}
    render(){
      if(!this.state.isLoaded || this.state.firstLoading){
        return(
          <ScrollView
            ref="container"
            style={styles.container}
            keyboardShouldPersistTaps={true}
            scrollEnabled={false}
            showsVerticalScrollIndicator={false}
          >
              <Image
                style={styles.fieldContainer}
                resizeMode="contain"
                source={require('./img/CoachRegistrationPage/background.png')}
                >

                <View style={[styles.questionContainer,{height:100,justifyContent:"center",margin:5}]}>
                  <TouchableOpacity
                  onPress={this.selectPhotoTapped.bind(this)}

                  >
                    <View style={[styles.avatar, styles.avatarContainer, {marginBottom: 20}]}>

                        <Image resizeMode="cover" style={styles.avatar}  />

                    </View>
                  </TouchableOpacity>
                </View>
                  <View style={styles.questionContainer}>
                    <Text
                      style={styles.textStyle}
                    >中文全名:</Text>
                    <TextInput
                      returnKeyType="next"
                      value={this.state.chinese_full_name}
                      onChangeText={(chinese_full_name)=>this.setState({chinese_full_name})}

                      style={styles.textInputStyle}
                      />
                  </View>

                  <View style={styles.questionContainer}>
                    <Text
                      style={styles.textStyle}
                    >英文全名:</Text>
                    <TextInput
                      returnKeyType="next"
                      value={this.state.eng_full_name}
                      onChangeText={(eng_full_name)=>this.setState({eng_full_name})}
                      style={styles.textInputStyle}

                      />
                  </View>
                  <View style={styles.questionContainer}>
                    <Text
                      style={styles.textStyle}
                    >電話號碼:</Text>
                    <TextInput
                      returnKeyType="next"
                      maxLength={8}
                      value={this.state.phone}
                      onChangeText={(phone)=>this.setState({phone})}
                      style={styles.textInputStyle}
                      />
                  </View>
                  <View style={styles.questionContainer}>
                    <Text
                      style={styles.textStyle}>姓別 :</Text>
                    <Picker
                      ref="gender"
                        style={{width:width*0.18}}
                        mode="dropdown"
                        onValueChange={(value)=> this.setState({gender:value})}
                        selectedValue={this.state.gender}
                      >
                      <Picker.Item label="男" value="M" />
                      <Picker.Item label='女' value="F" />
                      </Picker>
                  </View>


                  <View style={styles.questionContainer}>
                    <Text
                      style={styles.textStyle}
                    >年齡:</Text>
                    <TextInput
                      returnKeyType="next"
                      value={this.state.age}
                      onChangeText={(age)=>this.setState({age})}
                      style={styles.textInputStyle}
                      maxLength={3}
                      />
                  </View>
                </Image>
          </ScrollView>


        )
      }
        return(
          <ScrollView
            ref="container"
            style={styles.container}
            keyboardShouldPersistTaps={true}
            scrollEnabled={true}
            showsVerticalScrollIndicator={false}
          >
              <Image
                style={styles.fieldContainer}
                resizeMode="contain"
                source={require('./img/CoachRegistrationPage/background.png')}
                >

                <View style={[styles.questionContainer,{height:100,justifyContent:"center",margin:5}]}>
                  <TouchableOpacity
                  onPress={this.selectPhotoTapped.bind(this)}
                  disabled={this.state.touchableEnabled}
                  >
                    <View style={[styles.avatar, styles.avatarContainer, {marginBottom: 20}]}>
                        <Image style={styles.avatar} source={
                          this.state.propic && this.state.propic.uri ?
                          this.state.propic
                          :
                          {uri:"http://54.179.154.17/upload/coachPropic/"+this.state.propic}
                          } />
                    </View>
                  </TouchableOpacity>
                </View>

                  <View style={styles.questionContainer}>
                    <Text
                      style={styles.textStyle}
                    >中文全名:</Text>
                    <TextInput
                      editable={this.state.Enabled}
                      ref="chinese_full_name_input"
                      returnKeyType="next"
                      value={this.state.chinese_full_name}
                      onChangeText={(chinese_full_name)=>this.setState({chinese_full_name})}
                      style={styles.textInputStyle}
                      onSubmitEditing={
                        () => this.refs.eng_full_name_input.focus()
                      }
                      />
                  </View>

                  <View style={styles.questionContainer}>
                    <Text
                      style={styles.textStyle}
                    >英文全名:</Text>
                    <TextInput
                      editable={this.state.Enabled}
                      ref="eng_full_name_input"
                      returnKeyType="next"
                      value={this.state.eng_full_name}
                      onChangeText={(eng_full_name)=>this.setState({eng_full_name})}
                      style={styles.textInputStyle}
                      onSubmitEditing={
                        () => this.refs.ageInput.focus()
                      }
                      />
                  </View>
                  <View style={styles.questionContainer}>
                    <Text
                      style={styles.textStyle}
                    >電話號碼:</Text>
                    <TextInput
                      editable={this.state.Enabled}
                      ref="phoneTextInput"
                      returnKeyType="next"
                      maxLength={8}
                      value={this.state.phone}
                      onChangeText={(phone)=>this.setState({phone})}

                      style={styles.textInputStyle}
                      />
                  </View>
                  <View style={styles.questionContainer}>
                    <Text
                      style={styles.textStyle}>姓別 :</Text>
                    <Picker
                        enabled={this.state.Enabled}
                        ref="gender"
                        style={{width:width*0.18}}
                        mode="dropdown"
                        onValueChange={(value)=> this.setState({gender:value})}
                        selectedValue={this.state.gender}
                      >
                      <Picker.Item label="男" value="M" />
                      <Picker.Item label='女' value="F" />
                      </Picker>
                  </View>


                  <View style={styles.questionContainer}>
                    <Text
                      style={styles.textStyle}
                    >年齡:</Text>
                    <TextInput
                      editable={this.state.Enabled}
                      keyboardType="numeric"
                      ref="ageInput"
                      returnKeyType="next"
                      value={this.state.age}
                      onChangeText={(age)=>this.setState({age})}
                      style={styles.textInputStyle}
                      maxLength={3}
                      onSubmitEditing={
                        () => this.refs.experienceInput.focus()
                      }
                    />
                  </View>

                  <View style={styles.questionContainer}>
                    <Text

                      style={styles.textStyle}>教學語言: </Text>
                      <Text>廣東話</Text>
                      <Checkbox
                        checked= {this.state.teaching_language.indexOf("C") > -1}
                        onChange={(name,checked) => {

                          if(checked){
                            this.state.teaching_language.push("C");
                          }else{
                            this.state.teaching_language.splice(this.state.teaching_language.indexOf("C"),1);
                          }
                        }}
                        size={20}
                      />
                      <Text> 英文</Text>
                      <Checkbox
                      checked = {this.state.teaching_language.indexOf("E") > -1}
                      onChange={(name,checked) => {
                        console.log(checked);
                        if(checked){
                          this.state.teaching_language.push("E");
                        }else{
                          this.state.teaching_language.splice(this.state.teaching_language.indexOf("E"),1);
                        }
                      }}
                        size={20}
                      />
                      <Text> 普通話</Text>
                      <Checkbox
                      checked = {this.state.teaching_language.indexOf("M") > -1}

                      onChange={(name,checked) => {
                        console.log(checked);
                        if(checked){
                          this.state.teaching_language.push("M");
                        }else{
                          this.state.teaching_language.splice(this.state.teaching_language.indexOf("M"),1);
                        }
                      }}
                        size={20}
                      />
                  </View>

                  <View style={styles.questionContainer}>
                    <Text
                      style={styles.textStyle}
                    >可教授地區：</Text>
                    <TouchableOpacity

                      onPress={ () => this.refs.district.setState({modalVisible:true})}
                      activeOpacity={0.5}
                      style={{flexDirection:"row",alignItems:"center"}}
                    >
                      <Icon3
                        name="select-arrows"
                        color="#000"
                        size={20}
                      />
                      <Text>選擇</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={styles.questionContainer}>
                    <Text
                      style={styles.textStyle}
                    >專業項目：</Text>
                  </View>
                  <View style={styles.questionContainer}>
                    <Expert
                      p={this}
                    />
                  </View>

                  <View style={styles.questionContainer}>
                    <Text
                      style={styles.textStyle}
                    >教學年資:</Text>
                    <TextInput
                      editable={this.state.Enabled}
                      keyboardType="numeric"
                      ref="experienceInput"
                      returnKeyType="next"
                      value={this.state.experience}
                      onChangeText={(experience)=>this.setState({experience})}
                      style={styles.textInputStyle}
                      maxLength={3}
                      onSubmitEditing={
                        () => this.refs.expected_feeInput.focus()
                      }
                      />
                  </View>
                  <View style={styles.questionContainer}>
                    <Text
                      style={styles.textStyle}>工作性質:</Text>
                    <Picker
                        enabled={this.state.Enabled}
                        style={{width:width*0.2}}
                        mode="dropdown"
                        onValueChange={(value)=> this.setState({type_of_employment:value})}
                        selectedValue={this.state.type_of_employment}
                      >
                      <Picker.Item label="全職" value="F" />
                      <Picker.Item label='兼職' value="P" />
                      </Picker>
                  </View>

                  <View style={styles.questionContainer}>
                    <Text
                      style={styles.textStyle}>教學形式 :</Text>
                    <Picker
                        enabled={this.state.Enabled}
                        style={{width:width*0.3}}
                        mode="dropdown"
                        onValueChange={(value)=> this.setState({teaching_form:value})}
                        selectedValue={this.state.teaching_form}
                      >
                      <Picker.Item label="一對一" value="I" />
                      <Picker.Item label="小班教學" value="G" />
                      <Picker.Item label='皆可' value="B" />
                    </Picker>
                  </View>
                  <View style={styles.questionContainer}>
                    <Text
                      style={styles.textStyle}
                    >培訓費用(每小時):</Text>
                    <TextInput
                      editable={this.state.Enabled}
                      keyboardType="numeric"
                      ref="expected_feeInput"
                      onSubmitEditing={
                        () => this.checkInput()
                      }
                      returnKeyType="go"
                      value={this.state.expected_fee}
                      onChangeText={(expected_fee)=>this.setState({expected_fee})}
                      style={styles.textInputStyle}
                      maxLength={5}
                      />
                      <Button
                        ref="button"
                        p={this}
                        pp={this.props.p}
                      />
                  </View>
                  <AvalibleDistrict
                    ref="district"
                    p={this}
                  />
                  </Image>

          </ScrollView>

        )
    }
}


class Button extends Component{
      constructor(props){
        super(props);
        this.state={
          disabled:false
        }
      }

     onPress = () => {

       Alert.alert("係咪要改資料呀？","改黎改去咁煩嫁！",[
         {
           text:"係呀！吹咩",
           onPress: () => {
              this.props.p.checkInput();


           }
         },
         {
           text:"禁錯咋！屌你!",
           onPress:null
         },
       ]);
     }



     render(){
       return(

              <Icon.Button
                onPress={ () => this.onPress()}
                style={styles.nextPageButton}
                name="ios-arrow-forward-outline"
                size={30}
                color="#fff"
              >
              完成
              </Icon.Button>
       )
     }
}

class Question extends Component{
    constructor(props){
      super(props);
      this.state={

      };
    }
  render(){
    return(
        <View style={styles.questionContainer}>
          <Text
            style={styles.textStyle}
          >{this.props.name}</Text>
          <TextInput
            keyboardType={this.props.type}
            style={styles.textInputStyle}
            placeholder={this.props.placeHolder}
          />

        </View>

    )
  }
}

class Expert extends Component{
  constructor(props){
    super(props);
    this.state={
      expert1:-1,
      expert2:-1,
      expert3:-1,
    }
    if(this.props.p.state.expert && this.props.p.state.expert > -1){
      this.expert = this.props.p.getCategory.filter((item)=>{
        return (item.category_id == this.props.p.state.expert);
      });


      this.state.expert3= this.props.p.state.expert;

      this.state.expert2= this.props.p.getCategory.filter((item)=>{
        return (item.category_id == this.expert[0].parent_id);
      })[0].category_id;

      this.expert_temp = this.props.p.getCategory.filter((item)=>{  //temp row expert2
        return (item.category_id == this.expert[0].parent_id);
      });

      this.state.expert1= this.props.p.getCategory.filter((item)=>{
        return (item.category_id == this.expert_temp[0].parent_id);
      })[0].category_id;
    }


    //console.log(this.state.expert3);
    //console.log(this.state.expert2);
    //console.log(this.state.expert1);
    this.expert1=[];
    this.expert2=[];
    this.expert3=[];
    this.expert1.push(<Picker.Item key={0} label="大分類" value={-1} />);
    this.expert2.push(<Picker.Item key={0} label="小分類" value={-1} />);
    this.expert3.push(<Picker.Item key={0} label="項目" value={-1} />);

    this.expert1.push.apply(this.expert1,this.getCategoryElement(0,this.props.p.getCategory.filter((item) => {
      return item.parent_id == 0;
    })));
    this.expert2.push.apply(this.expert2,this.getCategoryElement(this.state.expert1,this.props.p.getCategory.filter((item) => {
      return item.parent_id == this.state.expert1;
    })));
    this.expert3.push.apply(this.expert3,this.getCategoryElement(this.state.expert2,this.props.p.getCategory.filter((item) => {
      return item.parent_id == this.state.expert2;
    })));

  }

  getCategoryElement = (pid,items) =>{
    var itemsView = [];
    if(pid == -1)
      return itemsView;
    for(var i = 0;i < items.length;i++){
      itemsView.push(<Picker.Item label={items[i]['name']} value={items[i]['category_id']} key={`${pid}`+`${i}`}/>);
    }
    return itemsView;
  }

  componentWillUpdate =(nextProps,nextState) => {
    console.log(nextState.expert3);
    if(this.props.p.state.expert && this.props.p.state.expert > -1 && nextState.expert3 != this.props.p.state.expert){
      this.expert = this.props.p.getCategory.filter((item)=>{
        return (item.category_id == this.props.p.state.expert);
      });
      this.state.expert3= this.props.p.state.expert;
      this.state.expert2= this.props.p.getCategory.filter((item)=>{
        return (item.category_id == this.expert[0].parent_id);
      })[0].category_id;
      this.expert_temp = this.props.p.getCategory.filter((item)=>{  //temp row expert2
        return (item.category_id == this.expert[0].parent_id);
      });
      this.state.expert1= this.props.p.getCategory.filter((item)=>{
        return (item.category_id == this.expert_temp[0].parent_id);
      })[0].category_id;
      this.expert1=[];
      this.expert2=[];
      this.expert3=[];
      this.expert1.push(<Picker.Item key={0} label="大分類" value={-1} />);
      this.expert2.push(<Picker.Item key={0} label="小分類" value={-1} />);
      this.expert3.push(<Picker.Item key={0} label="項目" value={-1} />);

      this.expert1.push.apply(this.expert1,this.getCategoryElement(0,this.props.p.getCategory.filter((item) => {
        return item.parent_id == 0;
      })));
      this.expert2.push.apply(this.expert2,this.getCategoryElement(this.state.expert1,this.props.p.getCategory.filter((item) => {
        return item.parent_id == this.state.expert1;
      })));
      this.expert3.push.apply(this.expert3,this.getCategoryElement(this.state.expert2,this.props.p.getCategory.filter((item) => {
        return item.parent_id == this.state.expert2;
      })));
    }
  }
  render(){
    return(
      <View style={styles.selectionBar}>
      <View style={styles.pickerContainer}>
        <Picker
          enabled={this.props.p.state.Enabled}
          style={styles.picker}
          selectedValue={this.state.expert1}
          mode="dropdown"
          onValueChange={(expert1) => {
            this.expert2 = [];
            this.expert2.push(<Picker.Item key={0} label="小分類" value={-1} />);
            this.expert2.push.apply(this.expert2,this.getCategoryElement(expert1,this.props.p.getCategory.filter((item) => {
              return item.parent_id == expert1;
            })));
            this.setState({expert1});
          }}
        >
          {this.expert1}
        </Picker>
      </View>

      <View style={styles.pickerContainer}>
        <Picker
          enabled={this.props.p.state.Enabled}
          style={styles.picker}
          selectedValue={this.state.expert2}
          mode="dropdown"
          onValueChange={(expert2) => {
            this.expert3 = [];
            this.expert3.push(<Picker.Item key={0} label="項目" value={-1} />);
            this.expert3.push.apply(this.expert3,this.getCategoryElement(expert2,this.props.p.getCategory.filter((item) => {
              return item.parent_id == expert2;
            })));
            this.setState({expert2});
          }}
        >
          {this.expert2}
        </Picker>
      </View>
      <View style={styles.pickerContainer}>
        <Picker
          enabled={this.props.p.state.Enabled}
          style={styles.picker}
          selectedValue={this.state.expert3}
          mode="dropdown"
          onValueChange={(expert3) => {
            this.setState({expert3});
            this.props.p.setState({expert:expert3});
          }}
        >
        {this.expert3}
        </Picker>
      </View>
    </View>
    );
  }
}

class AvalibleDistrict extends Component{
  constructor(props){
    super(props);
    this.state={
      avaDistrict:this.props.p.state.available_district,
      modalVisible:false,
      firstLoading:true
    }

    this.ds = new ListView.DataSource({
      rowHasChanged:(r1, r2) => r1 !== r2
    });

    this.district = [];

    this.hk = this.props.p.getDistrict.filter((item) => {
      return item.parent_id == 1;
    });

    this.kl = this.props.p.getDistrict.filter((item) => {
      return item.parent_id == 2;
    });

    this.nt = this.props.p.getDistrict.filter((item) => {
      return item.parent_id == 3;
    });



  }
  onPressCheck = (num,checked) => {
    if(checked){
      this.props.p.state.available_district.push(num);
    }else{

      let index = this.props.p.state.available_district.indexOf(num);
      this.props.p.state.available_district.splice(index,1);

    }
    console.log(this.props.p.state.available_district);
  }
  generateOption = () => {
    var options = [];
    options.push(<Text key={1} style={styles.districtTitle}>香港</Text>);
    this.hk.map((item) => {
      options.push(
        <View
          style={styles.checkRowContainer}
          key={item.district_id}>
          <Text style={styles.districtName}>
          {item.name}
          </Text>
          <Checkbox
            checked = {this.props.p.state.available_district.indexOf(item.district_id)> -1 ? true: false}
            onChange={(name,checked) => {
              this.onPressCheck(item.district_id, checked);
            }}
            size={25}
          />
        </View>
      );
    });
    options.push(<Text key={2} style={styles.districtTitle}>九龍</Text>);
    this.kl.map((item) => {
      options.push(
        <View
          style={styles.checkRowContainer}
          key={item.district_id}>
          <Text style={styles.districtName}>
          {item.name}
          </Text>
          <Checkbox
            checked = {this.props.p.state.available_district.indexOf(item.district_id)> -1 ? true: false}
            onChange={(name,checked) => {
              this.onPressCheck(item.district_id, checked);
            }}
            size={25}
          />
        </View>
      );
    });
    options.push(<Text key={3} style={styles.districtTitle}>新界</Text>);
    this.nt.map((item) => {
      options.push(
        <View
          style={styles.checkRowContainer}
          key={item.district_id}>
          <Text style={styles.districtName}>
          {item.name}
          </Text>
          <Checkbox
            checked = {this.props.p.state.available_district.indexOf(item.district_id)> -1 ? true: false}
            onChange={(name,checked) => {
              this.onPressCheck(item.district_id, checked);
            }}
            size={25}
          />
        </View>
      );
    });

    return options;

  }
  componentDidMount(){
    setTimeout(()=> this.setState({firstLoading:false}),1000);

  }
  render(){
    return(
      <View>
        <Modal
          animationType={"slide"}
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => null}
        >
        <View style={styles.avaDistrictContainer}>
        <ListView
          dataSource={this.ds.cloneWithRows(this.generateOption())}
          renderRow={(rowData) => rowData}
          initialListSize={8}
        />

        <Icon2.Button
          style={{borderRadius:0,justifyContent:"center"}}
          onPress={() =>
            {
              this.setState({modalVisible:false});
            }
          }
          name="close"
          color="#fff"
          size={30}
        >確定</Icon2.Button>


        </View>

      </Modal>
      </View>





    );
  }

}

/*class EditButton extends Component{
    constructor(props){
      super(props);
      this.state={

      };
    }

    handlePress =() =>{
      this.props.p.setState({Enabled:true});
      this.props.p.setState({touchableEnabled:false});

    };


  render(){
    return(
      <TouchableOpacity style={styles.editIconStyle} onPress={this.handlePress}>
        <Icon2
          name="edit"
          color="#000"
          size={40}
        />
      </TouchableOpacity>

    )
  }
}
*/
