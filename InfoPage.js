/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
'use strict'
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  ViewPagerAndroid,
  PixelRatio,
  InteractionManager,
  Modal,
  ScrollView,
  UIManager,
  LayoutAnimation,
  Animated,
} from 'react-native';
import Toast from 'react-native-simple-toast';

import {IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator} from 'rn-viewpager';
var {height, width} = Dimensions.get('window');
const height = height-55;
import DetailPage from './DetailPage.js';
import CourseListPage from './CourseListPage.js';
import CommentPage from './CommentPage.js';
const ratio = PixelRatio.get();

var IMAGE_URIS = [
  'https://3.bp.blogspot.com/-W__wiaHUjwI/Vt3Grd8df0I/AAAAAAAAA78/7xqUNj8ujtY/s1600/image02.png',
  'http://www.w3schools.com/css/img_forest.jpg',
];

import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/Entypo';
import Icon3 from 'react-native-vector-icons/MaterialIcons';
import Icon4 from 'react-native-vector-icons/Ionicons';

//style
var styles = StyleSheet.create({
   container:{
     flex:1,
     width: null,
     height: null,
     backgroundColor:"#fff",
   },
   blur:{
     flex:1
   },
   blurUser:{
     flex:1,
     width: null,
     height: null,
   },

  first_container: {
    backgroundColor:"#2aade5",
    alignItems:"center",
    elevation:1,
  },
  second_2container:{
     flexDirection:'row',

   },
   second_3container:{
     flex: 1,
     paddingLeft:15,
     elevation:1,
   },

   third_container:{
     height:height*0.13,
     flexDirection:'row',
     alignItems:"flex-end",
     backgroundColor:"#16a3d7",
   },
   fouth_container:{
     flex:0.12,
     flexDirection: 'row',
     alignItems:"flex-end",
     backgroundColor:"#29b2f6"

   },
   likeButton :{
     flex:1,
     height:height*0.1,
     justifyContent:'center',
     alignItems:'center',
     borderRightWidth:1,
     borderColor :'#1a9ac9',
     backgroundColor:"#16a3d7"
   },
   messageButton :{
     flex:1,
     height:height*0.1,
     justifyContent:'center',
     alignItems:'center',
     backgroundColor:"#16a3d7"

   },
   fontStyle:{
     fontSize:15,
     color:"#fff"
   },
   tagButton:{
     flex:1,
     alignItems:'center',
     justifyContent:'center'
   },
   line:{
     height:1/2,
     width:width/3,
     backgroundColor:"#fff",
     marginTop:height*0.025,
     marginBottom:height*0.025
   },
  iconButton:{
    justifyContent:'center',
    alignItems:'center',
    padding:5,
    paddingRight:25,
    paddingLeft:25,
  },
  levelButton:{
    height: height*0.08,
    width:width*0.2,
    marginTop:3,
  },
  levelText:{
    marginTop:5,
    color:"#fff"
  },
  circleIcon:{
    height: height*0.25-2,
    width: height*0.25-2,
    borderRadius:height*0.125-1,
    borderWidth:1,
    borderColor:"#fff",
  },
  userPhotoContainer:{
    justifyContent:'center',
    alignItems:'center',
    height: height*0.3,
    width: height*0.3,
    borderRadius:height*0.15,
    borderWidth:2,
    borderColor:"#fff",
    position:"absolute",
    bottom:-25,
    left:(width-height*0.3)/2,
  },
  backgroundConfig:{
    height:height*0.67,
    width: width,
  },

  addressContainer:{
    flexDirection:'row',
    justifyContent:'center',
    alignItems:'center',
  },
  addressImgStyle:{
    height:height*0.04,
    width:width*0.04,
  },
  inforText:{

  },
  tagText:{
    color:"#fff",
    fontSize:height*0.025
  },
  tagBar:{
    flex:0.01,
    backgroundColor:"#29ace4"
  },
  userImgBaackground:{
    height:height*0.25,
    width:width,
  },
  districtModalContainer:{
    justifyContent:'center',
    alignItems:"center",
    flex:1,
    backgroundColor:"rgba(0, 0, 0, 0.28)"
  },
  districtsContainer:{
    height:height*0.5,
    width:width*0.5,
    backgroundColor:"#fff",
    alignItems:"center",
    paddingBottom:10,

  },
  districtHeader:{
    height:height*0.08,
    width:width*0.5,
    alignItems:"center",
    backgroundColor:"#249cf2",
    padding:10,
    flexDirection:"row",
  },
  iconContainer:{
    height:50,
    width:50,
    borderRadius:25,
    alignItems:"center",
    justifyContent:"center",
    backgroundColor:"#1e9ace",
  },
  infoTitle:{
    padding:5,
    paddingLeft:15,
  },
  inforTextTitle:{
    color:"#2aade5",
    fontSize:12,
  },
  infoRow:{
    paddingBottom:10,
    paddingTop:10,
    borderBottomWidth:1,
    borderColor:"rgba(0, 0, 0, 0.1)"
  }
});




var toQueryString = (obj) => {
    return obj ? Object.keys(obj).sort().map(function (key) {
        var val = obj[key];
        if (Array.isArray(val)) {
            return val.sort().map(function (val2) {
                return key + '=' + val2;
            }).join('&');
        }
        return key + '=' + val;
    }).join('&') : '';
}

export default class InfoPage extends Component {
  constructor(props){
    super(props);
    this.state={
      param:this.props.param
    }
    //UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
    ////console.log(this.state.param);
  }

  shouldComponentUpdate(nextProps, nextState){
    ////console.log(nextState);
    if(nextState.param !== this.state.param){
      this.refs.viewpager.setPageWithoutAnimation(0);
      this.refs.BasicInfo.state.param = nextState.param;
      this.refs.BasicInfo.getCoachInfo(nextState.param);
      InteractionManager.runAfterInteractions(() => {
        this.refs.CommentPage.refs.CommentList.getData(true);
        this.refs.CommentPage.refs.Rating.getRating();
        this.refs.DetailPage.setState({param:nextState.param});
        if(this.refs.DetailPage.refs.pictureViwer.refs.pictureViwer){
          this.refs.DetailPage.refs.pictureViwer.refs.pictureViwer.scrollTo({x:0});
        }
        if(this.refs.CommentPage.refs.CommentList.refs.ListView){
          this.refs.CommentPage.refs.CommentList.refs.ListView.scrollTo({y:0});
        }
      });
    }
    return false;
  }

  render() {

    return (
      <View
        style={{flex:1}}
      >
        <View
          style={{height:55}}
        />
        <IndicatorViewPager
          ref="viewpager"
          scrollEnabled={false}
          style={{flex:1}}
          indicator={
            <PagerTitleIndicator
              style={styles.tagBar}
              titles={['基本資料', '詳細資料', '評價']}
            />
          }
        >
            <View style={{flex:1}}>
              <BasicInfo
                p={this}
                ref="BasicInfo"
              />
            </View>
            <View style={{flex:1}}>
              <DetailPage
                p={this}
                ref="DetailPage"
              />
            </View>
            <View style={{flex:1}}>
              <CommentPage
                ref="CommentPage"
                p={this}
              />
            </View>
        </IndicatorViewPager>
      </View>

    );
  }
}


class BasicInfo extends Component{
  constructor(props){
    super(props);
    this.state={
      firstLoading:true,
      param:this.props.p.state.param,
      coachData:new Object,
    }
  }
  componentDidMount(){
    InteractionManager.runAfterInteractions(() => {
      this.getCoachInfo();
    });
  }
  getCoachInfo = (coach_id) => {
    //////console.log(this.state.param);
    var request = new XMLHttpRequest();
    request.onreadystatechange = () => {
      if(request.readyState !== 4)
        return;
      if(request.status === 200){
        var result = request.responseText;
        ////console.log(result);
        if(result.length == 0)
          result=null;
        result = JSON.parse(result);
        //LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
        this.setState({coachData:result});
      }
    }
    request.open('POST','http://54.179.154.17/getCoachInfo.php',true);
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    if(this.props.p.props.p.state.userData){
      var param = {...this.state.param,student_id:this.props.p.props.p.state.userData.student_id};
    }
    else{
      var param = {...this.state.param,student_id:null};
    }
    param = toQueryString(param);
    request.send(param);
  }
  convertLang = (langString) => {
    var lang ="";
    if(langString){
      if(langString.includes("C"))
        lang = lang + "廣東話";
      if(langString.includes("M"))
        lang = lang + ",普通話";
      if(langString.includes("E"))
        lang = lang + ",英文";
    }
    return lang;
  }
  render(){
    return(
        <View
          style={styles.container}
        >
            <View style={styles.first_container}>
              <Text style={{
                fontSize:height*0.04,
                color:"#fff"
              }}> {this.state.coachData.username} </Text>
              <View style ={styles.addressContainer}>
                <Icon2
                  name="location-pin"
                  color="#fff"
                  size={20}
                />

                <Text
                  onPress={ () => this.refs.districtViewer.setState({visibleModal:true})}
                style={{
                  fontSize:height*0.02,
                  color:"#fff"
                }}>點擊查看地區</Text>
              </View>
              <Image
                style={styles.circleIcon}
                source={{uri:"http://54.179.154.17/upload/coachPropic/"+this.state.coachData.propic}}
                resizeMode="cover"
              />
              <IconButton
                p={this}
                pp={this}
              />
            </View>
            <View
              style={styles.infoTitle}
            >
              <Text
                style={{fontSize:15}}
              >
                基本資料
              </Text>
            </View>
              <ScrollView
                style={styles.second_3container}

              >
                <View style={styles.infoRow}>
                  <Text style={styles.inforTextTitle}>中文名字</Text>
                  <Text style={styles.inforText}>{this.state.coachData.chinese_full_name}</Text>
                </View>
                <View style={styles.infoRow}>
                <Text style={styles.inforTextTitle}>英文名字</Text>
                  <Text style={styles.inforText}>{this.state.coachData.eng_full_name}</Text>

                </View>
                <View style={styles.infoRow}>
                <Text style={styles.inforTextTitle}>性別</Text>
                  <Text style={styles.inforText}>{this.state.coachData.gender == "M"? '男':'女'}</Text>

                </View>
                <View style={styles.infoRow}>
                <Text style={styles.inforTextTitle}>教學經驗</Text>
                  <Text style={styles.inforText}>{this.state.coachData.experience}</Text>

                </View>
                <View style={styles.infoRow}>
                  <Text style={styles.inforTextTitle}>教學方式</Text>
                  <Text style={styles.inforText}>{this.state.coachData.teaching_form=="B" ? '一對一教學/小班教學':this.state.coachData.teaching_form=="G" ? "小班教學":"一對一教學"}</Text>

                </View>
                <View style={styles.infoRow}>
                  <Text style={styles.inforTextTitle}>全職/兼職</Text>
                  <Text style={styles.inforText}>{this.state.coachData.type_of_employment == "F" ? "全職":"兼職"}</Text>

                </View>
                <View style={styles.infoRow}>
                  <Text style={styles.inforTextTitle}>培訓費用(每小時)</Text>
                    <Text style={styles.inforText}>${this.state.coachData.expected_fee}</Text>

                </View>
                <View style={styles.infoRow}>
                  <Text style={styles.inforTextTitle}>教學語言</Text>
                  <Text style={styles.inforText}>{this.convertLang(this.state.coachData.teaching_language)}</Text>

                </View>

              </ScrollView>
              <DistrictViewer
                p={this}
                ref="districtViewer"
              />
        </View>

    );
  }
}


class IconButton extends Component{
  constructor(props){
    super(props);
    this.state={
      like:0,
      is_liked:0,
      rebound:new Animated.Value(0),
    }
  }
  likeAndRenew = () => {
    if(!this.props.pp.props.p.props.p.state.userData)
    {
      Toast.show('登入後才可讚', Toast.SHORT);
      return;
    }
    else if(this.props.pp.props.p.props.p.state.userData.user_type == 1){
      var request = new XMLHttpRequest();
      request.onreadystatechange = () => {
        if(request.readyState !== 4)
          return;
        //////console.log(request.status);
        if(request.status === 200){
          var result = request.responseText;
          ////console.log(result);
          result = JSON.parse(result);
          this.props.p.state.coachData.total_like = result.total_like;
          this.props.p.state.coachData.is_liked = result.is_liked;
          this.setState({is_liked:result.is_liked,like:result.total_like});

        }
      }
      request.open('POST','http://54.179.154.17/like.php',true);
      request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      var param = {...this.props.p.state.param,student_id:this.props.pp.props.p.props.p.state.userData.student_id}
      param = toQueryString(param);
      request.send(param);
    }
    else{
      Toast.show('你不是學生，不能讚', Toast.SHORT);
    }

  }
  render(){
    return(
      <View style={styles.second_2container}>

        <TouchableOpacity
          activeOpacity={0.7}
          style={styles.iconButton}
          onPress={ () => {
            Animated.spring(
            this.state.rebound,
            {
              toValue:1,
              friction:5,
            }).start(
              Animated.timing(
                this.state.rebound,
                {
                  toValue:0,
                  duration:0
                }
              ).start()
            );
            this.likeAndRenew();
          }}
        >
          <View
            style={styles.iconContainer}
          >
            <Animated.View
              style={[{
                alignItems:"center",
                justifyContent:"center",
                transform:[{
                  scale:this.state.rebound.interpolate({
                    inputRange: [0,0.5,1],
                    outputRange: [1,1.5,1],
                  })
                }]
              }]}
            >

              <Icon
                name="thumbs-up"
                color={this.props.p.state.coachData.is_liked == 1 ? "white":"rgba(255, 255, 255, 0.5)"}
                size={30}
              />
            </Animated.View>
          </View>
        <Text style={styles.levelText}>{this.props.p.state.coachData.total_like ? this.props.p.state.coachData.total_like:0} </Text>
        </TouchableOpacity>

        <View style={styles.iconButton}>
          <View
            style={styles.iconContainer}
          >
            <Icon
              name="wrench"
              color="white"
              size={30}
            />

        </View>
        <Text style={styles.levelText}>{this.props.p.state.coachData.expert_name}</Text>
      </View>

        <View style={styles.iconButton}>
          <View style={styles.iconContainer}>
            <Icon2
              name="medal"
              color="white"
              size={30}
            />

          </View>
          <Text style={styles.levelText}>{this.props.p.state.coachData.avgRating ? parseFloat(this.props.p.state.coachData.avgRating).toFixed(1):0}</Text>
        </View>

      </View>
    )
  }
}


class DistrictViewer extends Component{
  constructor(props){
    super(props)
    this.state={
      visibleModal:false,
    }
  }

  generateView = (districtString) => {
    if(!districtString)
      return (<Text>沒有選取地區</Text>);
    var districts = [];
    var districtArray = districtString.split(",");
    for(var i = 0;i < districtArray.length;i++){
      districts.push(
        <Text key={i}>
          {districtArray[i]}
        </Text>
      );
    }
    return districts;
  }

  render(){
    return(
      <Modal
        animationType="slide"
        visible={this.state.visibleModal}
        transparent={true}
        onRequestClose={ () => this.setState({visibleModal:false})}
      >
        <View
          style={styles.districtModalContainer}
        >

          <View style={styles.districtsContainer}>
            <View style={styles.districtHeader}>
              <TouchableOpacity
                style={{flex:1}}
                onPress={ () => this.setState({visibleModal:false})}
              >
                <Icon4
                  name="ios-arrow-back"
                  color="#fff"
                  size={25}
                />
              </TouchableOpacity>

              <Text style={{flex:4,color:"#fff",textAlign:"center"}}>
                可教授地區
              </Text>
              <View style={{flex:1}}>
              </View>


            </View>
            <ScrollView
            contentContainerStyle={{padding:5}}
            style={{flex:1}}>
              {this.generateView(this.props.p.state.coachData.available_district_name)}
            </ScrollView>
          </View>

        </View>
      </Modal>
    );
  }
}
