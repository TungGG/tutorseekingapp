'use strict';

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  ViewPagerAndroid,
  Image,
  Dimensions,
  ListView,
  LayoutAnimation,
  Alert,
  UIManager
} from 'react-native';


const {height, width} = Dimensions.get('window');
import categoryData from './categoryData.js'
import Icon from 'react-native-vector-icons/Ionicons';
import Icon2 from 'react-native-vector-icons/FontAwesome';
import Icon3 from 'react-native-vector-icons/MaterialIcons';
export default class SlideMenu extends Component{
  constructor(props){
    super(props);
    UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
  }
  onPress = ()=>{
    this.props.p.gotoPage({title:"MainPage",navBarTitle:"主頁"});
    this.props.p.refs.slideMenu.close();
  }
  render(){
    return(
      <ScrollView
        style={styles.menuContainer}
      >
        <LoginPart
          ref="loginPart"
          p={this}
          pp={this.props.p}
        />
        <TouchableOpacity
          onPress={()=> this.onPress()}
          activeOpacity={0.8}
          style={styles.homeBnt}
        >
            <Text style={styles.categoryTilte}>主頁</Text>
            <Icon2
              name="home"
              size={20}
              color="gray"
            />
        </TouchableOpacity>

        <Category
        ref="CategoryPart"
        p={this}
        pp={this.props.p}/>

        <Other
        ref="OtherPart"
        p={this}
        pp={this.props.p}
        />


      </ScrollView>

    );
  }
}



class LoginPart extends Component{
  constructor(props){
    super(props);
    this.state={
      userID:null,
      userData:null,
    }

  }

  onPressLogin = (page) => {
    this.props.pp.gotoPage(page);
    this.props.pp.refs.slideMenu.close();
  }

  onPressRegister = (page) => {
    this.props.pp.gotoPage(page);
    this.props.pp.refs.slideMenu.close();
  }

  logout = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({userID:null,userData:null});
    this.props.pp.setState({userID:null,userData:null});
    Alert.alert("登出成功");
    var logoutRequest = new XMLHttpRequest();
    logoutRequest.open('POST', 'http://54.179.154.17/logout.php',true);
    logoutRequest.send();
  }

  render(){
    return(
      this.state.userID ?
      (this.state.userData.user_type == 2 ?
      <View
        style={styles.loginContainer2}
      >
          <Image
            source={{uri:"http://54.179.154.17/upload/coachPropic/"+this.state.userData['propic']}}
            style={styles.userPropic}
            resizeMode="cover"
          />

        <View style={styles.userDataContainer}>
            <View
              style={styles.userTextContainer}
            >
              <Text style={styles.userDataText}>{this.state.userData['username']}</Text>
              <View style={styles.userButtonContainer}>
                <TouchableOpacity
                onPress={() => {
                  if(this.props.pp.ProfilePage)
                    this.props.pp.ProfilePage.setState({
                      ...this.props.pp.state.userData,
                      teaching_language:this.props.pp.state.userData.teaching_language.slice(),
                      available_district:this.props.pp.state.userData.available_district.slice(),
                    });
                  this.props.pp.gotoPage({title:"ProfilePage",navBarTitle:"個人資料"});
                  this.props.pp.refs.slideMenu.close();
                }

                }
                  activeOpacity={0.5}
                  style={[styles.userButton,{
                    backgroundColor:'#046fa6',
                    padding:2,
                    borderRadius:10,
                    marginBottom:5,

                  }]}
                >
                  <Icon2
                    name="user"
                    color="#fff"
                    size={15}
                    style={{paddingRight:5}}
                  />
                  <Text
                  style={{
                    color:"#fff"
                  }}>
                  個人資料
                  </Text>
                </TouchableOpacity>

                <View style={styles.userButtonContainer2}>
                <TouchableOpacity
                  onPress={() => {
                    if(this.props.pp.DetailManagementPage)
                      this.props.pp.DetailManagementPage.componentDidMount();
                    this.props.pp.gotoPage({title:"DetailManagementPage",navBarTitle:"我的介紹"});
                    this.props.pp.refs.slideMenu.close();
                  }
                }
                  style={styles.userButton}
                  activeOpacity={0.5}
                >
                  <Icon3
                    name="edit"
                    color="#fff"
                    size={15}
                  />
                  <Text style={{color:"#fff"}}>管理</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={[styles.userButton,{
                    borderLeftWidth:1,
                    borderColor:"#fff"
                  }]}
                  activeOpacity={0.5}
                  onPress={()=> this.logout()}
                >
                <Icon3
                  name="exit-to-app"
                  color="#fff"
                  size={15}
                />
                  <Text style={{color:"#fff"}}>登出</Text>
                </TouchableOpacity>
                </View>
              </View>
            </View>
        </View>
      </View>
      :
      <View
        style={styles.loginContainer2}
      >
          <Image
            source={require("./img/SlideMenu/student.png")}
            style={styles.userPropic}
            resizeMode="cover"
          />

        <View style={styles.userDataContainer}>
            <View
              style={styles.userTextContainer}
            >
              <Text style={styles.userDataText}>{this.state.userData['username']}</Text>
              <View style={styles.userButtonContainer}>
                <TouchableOpacity
                  onPress={() => {
                    if(this.props.pp.StudentProfilePage)
                      this.props.pp.StudentProfilePage.setState({...this.props.pp.state.userData});
                    this.props.pp.gotoPage({title:"StudentProfilePage",navBarTitle:"個人資料"});
                    this.props.pp.refs.slideMenu.close();
                }}
                  activeOpacity={0.5}
                  style={[styles.userButton,{backgroundColor:'#046fa6',padding:2,borderRadius:10,marginBottom:5}]}
                >
                  <Icon2
                    name="user"
                    color="#fff"
                    size={15}
                    style={{paddingRight:5}}
                  />
                  <Text
                  style={{
                    color:"#fff",
                    fontSize:12,
                  }}>個人資料</Text>
                </TouchableOpacity>

                <View style={styles.userButtonContainer2}>
                <TouchableOpacity
                  onPress={() => {
                    this.props.pp.gotoPage({title:"CaseManagementPage",navBarTitle:"我的個案"});
                    if(this.props.pp.CaseManagementPage)
                      this.props.pp.CaseManagementPage.componentDidMount();
                    this.props.pp.refs.slideMenu.close();
                  }
                }
                  style={styles.userButton}
                  activeOpacity={0.5}
                >
                  <Icon3
                    name="edit"
                    color="#fff"
                    size={15}
                  />
                  <Text style={{color:"#fff"}}>管理</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={[styles.userButton,{
                    borderLeftWidth:1,
                    borderColor:"#fff"
                  }]}
                  activeOpacity={0.5}
                  onPress={()=>{
                    this.props.pp.gotoPage({title:"MessagePage",navBarTitle:"訊息"});
                    if(this.props.pp.MessagePage)
                      this.props.pp.MessagePage.componentDidMount();
                    this.props.pp.refs.slideMenu.close();
                  }}
                >
                  <Icon3
                    name="mail"
                    color="#fff"
                    size={15}
                  />
                  <Text style={{color:"#fff"}}>訊息</Text>
                </TouchableOpacity>
                </View>
              </View>

            </View>
        </View>
        <TouchableOpacity
          style={[styles.userButton,{
            position:"absolute",
            bottom:5,
            right:5,
          }]}
          activeOpacity={0.5}
          onPress={()=> this.logout()}
        >
          <Icon3
            name="exit-to-app"
            color="#fff"
            size={15}
          />
          <Text style={{color:"#fff"}}>登出</Text>
        </TouchableOpacity>
      </View>)
      :
      <View
        style={styles.loginContainer}
        >
        <Image
          source={{uri:"https://yourstory.com/wp-content/uploads/2013/04/plivo_icon.png"}}
          resizeMode="contain"
          style={styles.logoImg}
        />
        <View style={{flexDirection:'row'}}>
          <TouchableOpacity
            activeOpacity={0.8}
            style={styles.bntContainer}
            onPress={() => this.onPressLogin({title:"LoginPage",navBarTitle:"登入"})}
          >
            <View style={styles.loginBnt}>
              <Text style={styles.bntText}>登入</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.8}
            style={styles.bntContainer}
            onPress={ () => this.onPressRegister({title:"BasicRegistrationPage",navBarTitle:"註冊"})}
          >
            <View style={styles.registerBnt}>
              <Text style={styles.bntText}>註冊</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

class Category extends Component{
  constructor(props){
    super(props);
  }

  shouldComponentUpdate(){
    return false;
  }

  onPress = (i) => {
    var param=categoryData[i].param;
    console.log(param);
    if( param.type == 2){
      this.props.pp.gotoPage({title:"CoachListPage",navBarTitle:"導師列表",param:param});
      this.props.pp.refs.slideMenu.close();
    }
    else if (param.type == 1) {
        this.props.pp.gotoPage({title:"CasesListPage",navBarTitle:"個案列表",param:param});
        this.props.pp.refs.slideMenu.close();
    }
  }


  render(){
    var categories = [];
    for(let i = 0; i < categoryData.length;i++){
      categories.push(
        <TouchableOpacity
          activeOpacity={0.8}
          key={i}
          style={styles.categorieContainer}
          onPress={() => this.onPress(i)}
        >
          <Image
            style={styles.categoryImg}
            resizeMode="contain"
            source={{uri:categoryData[i].img}}
          />
          <Text style={styles.categoryText}>
            {categoryData[i].category}
          </Text>
        </TouchableOpacity>
      );
    }
    return(
      <View>
        <View style={styles.categoryTilteContainer}>
           <Text style={styles.categoryTilte}>分類</Text>
        </View>
        {categories}
      </View>
    );
  }
}

class Other extends Component{
  constructor(props){
    super(props);
  }

  shouldComponentUpdate(){
    return false;
  }

  goToAboutUs = () => {
      this.props.pp.gotoPage({title:"AboutUsPage",navBarTitle:"關於我們"});
      this.props.pp.refs.slideMenu.close();
  }
  goToContactUs = () => {
      this.props.pp.gotoPage({title:"ContactUsPage",navBarTitle:"聯絡我們"});
      this.props.pp.refs.slideMenu.close();
  }

  render(){
    return(
      <View>
        <View style={styles.otherTitleContainer}>
          <Text style={styles.otherTitle}>其他</Text>
        </View>

        <TouchableOpacity
          activeOpacity={0.8}
          onPress={this.goToAboutUs}
          style={styles.otherContainer}>
          <Image
            style={styles.othersImg}
            source={{uri:"https://cdn2.iconfinder.com/data/icons/flatte-internet-and-websites/80/15_-_Bookmark-512.png"}}
          />
          <Text style={styles.otherText}>
            關於我們
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          activeOpacity={0.8}
          onPress={this.goToContactUs}
          style={styles.otherContainer}>
          <Image
            style={styles.othersImg}
            source={{uri:"https://cdn2.iconfinder.com/data/icons/flatte-internet-and-websites/80/15_-_Bookmark-512.png"}}
          />
          <Text style={styles.otherText}>
            聯絡我們
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  homeBnt:{
    padding:5,
    flexDirection:"row",
    justifyContent:"space-between",
    borderBottomWidth:1/2,
    borderColor:"#e7e7e7"
  },
  userButtonContainer2:{
    flexDirection:'row',
    borderColor:"#015f8f",
    backgroundColor:"#046fa6",
    padding:2,
    borderRadius:10,
  },
  userButton:{
    alignItems:"center",
    justifyContent:"center",
    flexDirection:"row"
  },
  userButtonContainer:{
    width:100,
    padding:2,
  },
  userDataText:{
    color:"#fff",
    fontSize:18,
  },
  userTextContainer:{
    padding:10,
    flex:1,
    justifyContent:"space-between",
    alignItems:"center",
  },
  userDataContainer:{
    flex:1,
    justifyContent:"space-between"
  },
  otherText:{
    marginLeft:10
  },
  otherContainer:{
    flexDirection:'row',
    alignItems:'center',
    padding:8,
    borderTopWidth:1/2,
    borderColor:"#e7e7e7"
  },
  othersImg:{
    height:30,
    width:30,
  },
  otherTitle:{
    fontSize:15,
    color:"#000"
  },
  otherTitleContainer:{
    padding:5,
    justifyContent:'center',
    borderTopWidth:1/2,
    borderColor:"#e7e7e7"
  },
  background:{
    backgroundColor:"#2f84d9"
  },
  categoryText:{
    marginLeft:10,
  },
  categorieContainer:{
    flexDirection:'row',
    padding:8,
    alignItems:'center',
    borderTopWidth:1/2,
    borderColor:"#e7e7e7"
  },
  categoryTilteContainer:{
    padding:5,
    justifyContent:'center',
  },
  categoryTilte:{
    fontSize:15,
    color:"#000"
  },
  categoryImg:{
    height:30,
    width:30,
  },
  logoImg:{
    height:90,
    width:90,
  },
  loginContainer:{
    height:height*0.22,
    backgroundColor:"#2b9fcf",
    justifyContent:'flex-end',
    alignItems:"center",
  },
  loginContainer2:{
    height:height*0.22,
    backgroundColor:"#2b9fcf",
    padding:15,
    flexDirection:'row',
    justifyContent:"center",
    alignItems:'center'
  },
  menuContainer:{
    flex:1,
    backgroundColor:"#fff"
  },
  bntText:{
    color:"#fff",
    fontSize:15
  },
  registerBnt:{
    flex:1,
    height:30,
    justifyContent:'center',
    alignItems:'center',

  },
  loginBnt:{
    flex:1,
    height:30,
    justifyContent:'center',
    alignItems:'center',
    borderRightWidth:1/2,
    borderRightColor:"#1d94ba",
  },
  bntContainer:{
    flex:1,
    height:40,
    backgroundColor:"#2b9fcf",
    flexDirection:'row',
    alignItems:"flex-end",

  },
  userPropicContainer:{
  },
  userPropic:{
    height:width*0.25,
    width:width*0.25,
    borderRadius:width*0.125,
    borderWidth:1,
    borderColor:"#fff"
  }
})
