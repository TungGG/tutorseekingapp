export default topRatingTeacherData = [
  {
    "name":"name1",
    "rating":5,
    "description":"description1",
    "img":"http://skilldesigns.com/images/skilldesigns/SocialMeial/teacher.jpg"
  },
  {
    "name":"name2",
    "rating":4,
    "description":"description2",
    "img":"http://skilldesigns.com/images/skilldesigns/SocialMeial/teacher.jpg"
  },
  {
    "name":"name3",
    "rating":3,
    "description":"description3",
    "img":"http://skilldesigns.com/images/skilldesigns/SocialMeial/teacher.jpg"
  },
  {
    "name":"name4",
    "rating":2,
    "description":"description4",
    "img":"http://skilldesigns.com/images/skilldesigns/SocialMeial/teacher.jpg"
  },
  {
    "name":"name5",
    "rating":1,
    "description":"description5",
    "img":"http://skilldesigns.com/images/skilldesigns/SocialMeial/teacher.jpg"
  },


]
