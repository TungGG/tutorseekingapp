'use strict';

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Navigator,
  Dimensions,
  BackAndroid,
  TouchableOpacity,
  PixelRatio,
  Alert,
  ListView,
  ScrollView,
  Image,
  Modal,
  TextInput,
  Platform,
  InteractionManager,
  Picker,
  UIManager,
  LayoutAnimation
} from 'react-native';

const {height, width} = Dimensions.get('window');
import Icon from 'react-native-vector-icons/MaterialIcons';
import Icon2 from 'react-native-vector-icons/Octicons';
import districtData from './getDistrict.js';


var getCategory = [];
var getDistrict = districtData;
var toQueryString = (obj)=> {
    return obj ? Object.keys(obj).sort().map(function (key) {
        var val = obj[key];
        if (Array.isArray(val)) {
            return val.sort().map(function (val2) {
                return key + '=' + val2;
            }).join('&');
        }
        return key + '=' + val;
    }).join('&') : '';
}

export default class CaseManagementPage extends Component{
  constructor(props){
    super(props);
    this.ds = new ListView.DataSource({
      rowHasChanged: (p1, p2) => p1 !== p2,
      sectionHeaderHasChanged : (s1, s2) => s1 !== s2,
    });
    this.state={
      dataSource:this.ds.cloneWithRowsAndSections(this.formatData([])),
      numOfitem: 0,
    }

    //UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
  }

  CustomLayoutLinear = {
    duration: 150,
    create: {
      type: LayoutAnimation.Types.linear,
      property: LayoutAnimation.Properties.opacity,
    },
    update: {
      type: LayoutAnimation.Types.linear,
    },
  };
  getCaseData = () =>{
    var request = new XMLHttpRequest();
    request.onreadystatechange = (e) =>{
      if(request.readyState !== 4){
        return;
      }

      if(request.status === 200){
        var result = request.responseText;
        if(result.length > 0){
          //console.log(result);
          result = JSON.parse(result);
          LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
          this.setState({dataSource:this.ds.cloneWithRowsAndSections(this.formatData(result)),numOfitem:result.length});
        }
      }
    }
    request.open('POST', 'http://54.179.154.17/getStudentCase.php', true);
    request.send();
  }

  formatData = (data) =>{
    var dataBlob = {};

    dataBlob['近期發佈的個案'] = [];
    dataBlob['已過期或取消的個案'] = [];

    for(var i = 0;i< data.length;i++){
      if(data[i].state == "A"){
        dataBlob['近期發佈的個案'].push(data[i]);
      }
      else if(data[i].state == "C"){
        dataBlob['已過期或取消的個案'].push(data[i]);
      }

    }

    if(dataBlob['近期發佈的個案'].length < 1)
      delete dataBlob['近期發佈的個案'];
    if(dataBlob['已過期或取消的個案'].length < 1)
      delete dataBlob['已過期或取消的個案'];
    return dataBlob;

  }


  renderRow = (rowData) => {
    return(
      <View
        style={styles.caseRootContainer}
      >
        <View style={styles.caseHeader}>
          <Text>日期:{rowData.post_time}
          </Text>
          {rowData.state != 'C' ?
            <TouchableOpacity
              onPress={ () => this.deleteCase(rowData.case_id)}
              activeOpacity={0.6}
              style={styles.deleteButton}
            >
              <Icon2
                name="remove-close"
                color="#fff"
                size={15}
              />
            </TouchableOpacity>
            :
            null
          }
        </View>
          <View style={styles.caseContent}>
            <View style={styles.textContainer}>
              <View>
                <Text>編號:{rowData.case_id}</Text>
                <Text>項目:{rowData.event_name}</Text>
                <Text>形式:{rowData.size=='P' ? "個人":"團體"}</Text>
                <Text>程度:{rowData.level}</Text>
              </View>
              <View>
                <Text> </Text>
                <Text>時薪:{rowData.salary}</Text>
                <Text>性別:{rowData.gender == "M" ? "男":(rowData.gender == "F" ? "女":"有男有女")}</Text>
                <Text>年齡:{rowData.age}</Text>
              </View>
              <View>
                <Text> </Text>
                <Text>每週:{rowData.period}</Text>
                <Text>地區:{rowData.location_name}</Text>
                <Text>時間:{rowData.hour}</Text>
              </View>
            </View>
            <View style={styles.otherContainer}>
              <Text>其他要求:</Text>
              <Text>{rowData.requirement}</Text>
            </View>
          </View>


      </View>

    );
  }
  deleteCase = (id) => {
    Alert.alert("咪住先","求下你唔好剷我！！！",[
      {
        text:"唔理佢照剷",
        onPress: () => {
          var deleteRequest = new XMLHttpRequest();
          deleteRequest.onreadystatechange = () => {
            if(deleteRequest.readyState !== 4)
              return;
            //console.log(deleteRequest.status);
            if(deleteRequest.status === 200){
              var result = deleteRequest.responseText;

              console.log(result);
              if(result.includes("success")){
                this.getCaseData();
              }
            }
          }
          deleteRequest.open('POST','http://54.179.154.17/deleteCase.php', true);
          deleteRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
          var parem = `case_id=${id}`;
          deleteRequest.send(parem);
        }
      },
      {
        text:"都係唔剷算",
        onPress:null
      },
    ]);
  }
  onPressAdd = () => {
    this.refs.AddCase.setState({visibleModal:true});
  }
  componentDidMount(){
    InteractionManager.runAfterInteractions(() => {
      this.getCaseData();
      var request = new XMLHttpRequest();
      var result = [];
      request.onload = () =>{
        if(request.status === 200){
          result.push.apply(result,JSON.parse(request.responseText));
          getCategory = result;
          ////console.log(result);
          ////console.log( result);
        }
      }
      request.open('POST', 'http://54.179.154.17/getCategory.php', true);
      request.send();
    });

  }
  render(){
    return(
      <View style={styles.rootContainer}>
        {this.state.numOfitem > 0 ?
        <ListView
          showsVerticalScrollIndicator={false}
          enableEmptySections={true}
          contentContainerStyle={{justifyContent:'center',alignItems:'center',paddingBottom:20}}
          dataSource={this.state.dataSource}
          renderRow={(dataRow) => this.renderRow(dataRow)}
          renderSectionHeader={ (sectionData,title) =>{
            return(
              <View
                style={{
                  width:width,
                  alignItems:"center",
                  paddingTop:10,

                }}
              >
                <Text
                  style={{fontSize:20}}
                >
                  {title}
                </Text>
              </View>
            );
          }}
        />
        :
        <Text style={{fontSize:25}}>
          找不到任何個案
        </Text>
      }
        <AddCase
          p={this}
          ref="AddCase"
        />
        <TouchableOpacity
          onPress={ () => this.onPressAdd()}
          style={styles.addButton}>
          <Icon
            name="add-box"
            color="#3b9ff1"
            size={50}
          />
        </TouchableOpacity>
      </View>

    );
  }
}

class AddCase extends Component{
  constructor(props){
    super(props);
    this.state={
      visibleModal:false,
      hourS:-1,
      hourE:-1,
      period:"",
      event:-1,
      requirement:"",
      gender:"M",
      age:"",
      location:-1,
      level:"",
      fee:"",
      size:"P",
    }
    this.Stime = [];
    this.Stime.push(
      <Picker.Item
      key={-1}
      label="開始時間"
      value={-1}
    />);
    this.Etime = [];
    this.Etime.push(
      <Picker.Item
        key={-1}
        label="結束時間"
        value={-1}
      />
    );
    for(var i=0;i<24;i++){
      var hourString = i >= 10 ? `${i}:00`:`0${i}:00`;
      this.Stime.push(
        <Picker.Item
          key={i}
          label={hourString}
          value={i}
        />
      );
      this.Etime.push(
        <Picker.Item
          key={i}
          label={hourString}
          value={i}
        />
      );

    }
  }



  onPressPostCase = () => {
  if(!this.state.age){
    Alert.alert("錯誤","請輸入年齡。");
    return;
  }
  else if(this.state.event < 0){
    Alert.alert("錯誤","請選擇項目。");
    return;
  }
  else if(!this.state.level){
    Alert.alert("錯誤","請輸入程度。");
    return;
  }
  else if(this.state.district<0){
    Alert.alert("錯誤","請選擇地區。");
    return;
  }else if(this.state.hourS < 0 || this.state.hourE < 0 || this.state.hourS > this.state.hourE ){
    Alert.alert("錯誤","請選擇正確的時間。");
    return;
  }
  else if(!this.state.period){
    Alert.alert("錯誤","請輸入每週上課日數。");
    return;
  }
  else if(!this.state.fee){
    Alert.alert("錯誤","請輸入費用。");
    return;
  }
  else if(!this.state.requirement){
    Alert.alert("錯誤","請輸入其他要求，如沒有請填寫沒有。");
    return;
  }

  /*var pattern = /^\d{1,3}$/;
  if(!pattern.test(this.state.age)){
    Alert.alert("錯誤","請輸入正確的年齡。");
    return;
  }*/
  var pattern2 = /^\d{1,5}$/;
  if(!pattern2.test(this.state.fee)){
    Alert.alert("錯誤","請輸入正確的費用。");
    return;
  }
  var request = new XMLHttpRequest();
  request.onload= () =>{
    if(request.status === 200){
      var result = request.responseText;
      //console.log(result);
      if(result.includes("success")){
        this.props.p.getCaseData();
        this.setState({
          visibleModal:false,
          hourS:-1,
          hourE:-1,
          period:"",
          event:-1,
          requirement:"",
          gender:"M",
          age:"",
          location:-1,
          level:"",
          fee:"",
          size:"P",
        });
      }
    }
    else{
      //console.log("Something went wrong.");
    }
  }
  request.onerror= (e) =>{
    //console.log(e);
  }

  request.open('POST', 'http://54.179.154.17/postNewCase.php', true);
  request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  var param = toQueryString(this.state);
  //console.log(param);
  request.send(param);


}
  render(){

    return(
        <Modal
          style={styles.addModalRootContainer}
          animationType={"slide"}
          visible={this.state.visibleModal}
          transparent={true}
          onRequestClose={ () => null}
        >
          <View
            style={styles.addModalRootContainer}
          >
            <View
              style={styles.addModalContainer}

            >
              <ScrollView
                contentContainerStyle={{alignItems:'center',}}
              >
                <View style={styles.modalTitleContainer}>
                  <Text>
                    新增個案
                  </Text>
                  <TouchableOpacity
                    activeOpacity={0.6}
                    style={styles.closeModal}
                    onPress={ () => this.setState({
                      visibleModal:false,
                      hourS:-1,
                      hourE:-1,
                      period:"",
                      event:-1,
                      requirement:"",
                      gender:"M",
                      age:"",
                      location:-1,
                      level:"",
                      fee:"",
                    })}
                  >
                    <Icon2
                      name="remove-close"
                      color="#b8b8b8"
                      size={20}
                    />
                  </TouchableOpacity>
                </View>
                <View style={styles.questionCotainer}>
                  <Text>
                    個人/團體:
                  </Text>
                  <Picker
                    mode="dropdown"
                    style={{width:80}}
                    selectedValue={this.state.size}
                    onValueChange={(size) => this.setState({size})}
                  >
                    <Picker.Item
                      label="個人"
                      value="P"
                    />
                    <Picker.Item
                      label="團體"
                      value="G"
                    />
                  </Picker>
                </View>
                <View style={styles.questionCotainer}>
                  <Text>
                    性別:
                  </Text>
                  {
                    this.state.form == 'P' ?
                    <Picker
                      style={{width:60}}
                      selectedValue={this.state.gender}
                      onValueChange={(gender) => this.setState({gender})}
                      mode="dropdown"
                    >
                      <Picker.Item
                        label="男"
                        value="M"
                      />
                      <Picker.Item
                        label="女"
                        value="F"
                      />
                    </Picker>
                    :
                    <Picker
                      style={{width:110}}
                      selectedValue={this.state.gender}
                      onValueChange={(gender) => this.setState({gender})}
                      mode="dropdown"
                    >
                      <Picker.Item
                        label="男"
                        value="M"
                      />
                      <Picker.Item
                        label="女"
                        value="F"
                      />
                      <Picker.Item
                        label="有男有女"
                        value="B"
                      />
                    </Picker>
                  }

                </View>
                <View style={styles.questionCotainer}>
                  <Text>
                    年齡:
                  </Text>
                  <TextInput
                    style={{flex:1}}
                    placeholder="請輸入年齡或年齡範圍(例如:12-16)"
                    value={this.state.age}
                    onChangeText={(age) => this.setState({age})}
                    keyboardType="numeric"
                    maxLength={5}
                    returnKeyType="next"
                    onSubmitEditing={
                      () => this.refs.level.focus()
                    }
                  />
                </View>
                <View style={[styles.questionCotainer,{height:height*0.03}]}>
                  <Text>
                    項目:
                  </Text>
                </View>
                <View style={styles.questionCotainer}>

                  <Category
                    p={this}
                  />
                </View>
                <View style={styles.questionCotainer}>

                  <Text>
                  程度:
                  </Text>
                  <TextInput
                    ref="level"
                    style={{flex:1}}
                    value={this.state.level}
                    onChangeText={(level) => this.setState({level})}
                    placeholder="例如初級/中級/高級"
                    placeholderTextColor="#b8b8b8"
                    returnKeyType="next"
                    onSubmitEditing={
                      () => this.refs.period.focus()
                    }
                  />
                </View>

                <View style={styles.questionCotainer}>
                  <Text>
                    地區:
                  </Text>
                  <District
                    p={this}
                  />
                </View>
                <View style={styles.questionCotainer}>
                  <Text>
                    時間:
                  </Text>
                  <Picker
                    mode="dropdown"
                    style={{flex:1}}
                    selectedValue={this.state.hourS}
                    onValueChange={(hourS) => this.setState({hourS})}
                  >
                    {this.Stime}
                  </Picker>
                  <Text>-
                  </Text>
                  <Picker
                    mode="dropdown"
                    style={{flex:1}}
                    selectedValue={this.state.hourE}
                    onValueChange={(hourE) => this.setState({hourE})}
                  >
                    {this.Etime}
                  </Picker>
                </View>
                <View style={styles.questionCotainer}>
                  <Text>
                    每週:
                  </Text>
                  <TextInput
                    ref="period"
                    style={{flex:1}}
                    value={this.state.period}
                    onChangeText={(period) => this.setState({period})}
                    placeholder="例如星期一三五/一至五等等"
                    placeholderTextColor="#b8b8b8"
                    returnKeyType="next"
                    onSubmitEditing={
                      () => this.refs.fee.focus()
                    }
                  />
                </View>
                <View style={styles.questionCotainer}>
                  <Text>
                    費用(每小時):
                  </Text>
                  <TextInput
                    ref="fee"
                    style={{flex:1}}
                    value={this.state.fee}
                    onChangeText={(fee) => this.setState({fee})}
                    keyboardType="numeric"
                    maxLength={5}
                    returnKeyType="next"
                    onSubmitEditing={
                      () => this.refs.requirement.focus()
                    }
                  />
                </View>
                <View style={styles.questionCotainer}>
                  <Text>
                    其他要求:
                  </Text>
                  <TextInput
                    ref="requirement"
                    multiline={false}
                    style={{flex:1}}
                    value={this.state.requirement}
                    onChangeText={(requirement) => this.setState({requirement})}
                    placeholder="如沒有其他要求請填寫沒有"
                    placeholderTextColor="#b8b8b8"
                    returnKeyType="go"
                    onSubmitEditing={
                      () => this.onPressPostCase()
                    }
                  />
                </View>

                <View style={styles.comfirmButtonContainer}>
                  <Icon.Button
                    onPress={() => this.onPressPostCase()}
                    style={{width:width*0.7,backgroundColor:"#409ee0",justifyContent:"center",borderRadius:0}}
                    name="send"
                    color="#fff"
                    size={20}
                  >
                  確定
                  </Icon.Button>
                </View>

              </ScrollView>

            </View>
          </View>
        </Modal>
    );
  }
}

class District extends Component{
  constructor(props){
    super(props)
    this.state={
      district1:-1,
      district2:-2
    }
    this.district1=[];
    this.district2=[];
    this.district1.push(<Picker.Item key={0} label="地區" value={-1} />);
    this.district2.push(<Picker.Item key={0} label="分區" value={-1} />);
  }

  getDistrictElement = (pid,items) =>{
    var itemsView = [];
    if(pid == -1)
      return itemsView;
    for(var i = 0;i < items.length;i++){
      itemsView.push(<Picker.Item label={items[i]['name']} value={items[i]['district_id']} key={i}/>);
    }
    return itemsView;
  }

  render(){
    this.district1=[];
    this.district1.push(<Picker.Item key={0} label="地區" value={-1} />);
    this.district1.push.apply(this.district1,this.getDistrictElement(0,getDistrict.filter((item) => {
      return item.parent_id == 0;
    })));
    ////console.log(this.district1);
    return(
      <View style={styles.selectionBar}>
        <View style={styles.pickerContainer}>
          <Picker
            style={styles.picker}
            selectedValue={this.state.district1}
            mode="dropdown"
            onValueChange={(district1) => {
              this.district2.length = 0;
              this.district2.push(<Picker.Item key={0} label="分區" value={-1} />);
              this.district2.push.apply(this.district2,this.getDistrictElement(district1,getDistrict.filter((item) => {
                return item.parent_id == district1;
              })));
              this.setState({district1});
            }}
          >
            {this.district1}

          </Picker>
        </View>

        <View style={styles.pickerContainer}>
          <Picker
            style={styles.picker}
            selectedValue={this.state.district2}
            mode="dropdown"
            onValueChange={(district2) => {
              this.setState({district2});
              this.props.p.setState({district:district2});
            }}
          >
            {this.district2}
          </Picker>
        </View>
      </View>
    );
  }
}

class Category extends Component{
  constructor(props){
    super(props);
    this.state={
      category1:-1,
      category2:-1,
      category3:-1,
    }
    this.category1=[];
    this.category2=[];
    this.category3=[];
    this.category1.push(<Picker.Item key={0} label="大分類" value={-1} />);
    this.category2.push(<Picker.Item key={0} label="小分類" value={-1} />);
    this.category3.push(<Picker.Item key={0} label="項目" value={-1} />);

    this.category1.push.apply(this.category1,this.getCategoryElement(0,getCategory.filter((item) => {
      return item.parent_id == 0;
    })));

    ////console.log(getCategory);

  }

  getCategoryElement = (pid,items) =>{
    var itemsView = [];
    if(pid == -1)
      return itemsView;
    for(var i = 0;i < items.length;i++){
      itemsView.push(<Picker.Item label={items[i]['name']} value={items[i]['category_id']} key={`${pid}`+`${i}`}/>);
    }
    return itemsView;
  }

  componentDidMount(){
    this.category1=[];
    this.category1.push(<Picker.Item key={0} label="大分類" value={-1} />);
    this.category1.push.apply(this.category1,this.getCategoryElement(0,getCategory.filter((item) => {
      return item.parent_id == 0;
    })));
    //console.log("sfdfd");
  }

  render(){
    return(
      <View style={styles.selectionBar}>
      <View style={styles.pickerContainer}>
        <Picker
          style={styles.picker}
          selectedValue={this.state.category1}
          mode="dropdown"
          onValueChange={(category1) => {
            this.category2.length = 0;
            this.category2.push(<Picker.Item key={0} label="小分類" value={-1} />);

            this.category2.push.apply(this.category2,this.getCategoryElement(category1,getCategory.filter((item) => {
              return item.parent_id == category1;
            })));
            this.setState({category1});
          }}
        >
          {this.category1}
        </Picker>
      </View>

      <View style={styles.pickerContainer}>
        <Picker
          style={styles.picker}
          selectedValue={this.state.category2}
          mode="dropdown"
          onValueChange={(category2) => {
            this.category3.length = 0;
            this.category3.push(<Picker.Item key={0} label="項目" value={-1} />);
            this.category3.push.apply(this.category3,this.getCategoryElement(category2,getCategory.filter((item) => {
              return item.parent_id == category2;
            })));
            this.setState({category2});
          }}
        >
          {this.category2}
        </Picker>
      </View>
      <View style={styles.pickerContainer}>
        <Picker
          style={styles.picker}
          selectedValue={this.state.category3}
          mode="dropdown"
          onValueChange={(category3) => {
            this.setState({category3});
            this.props.p.setState({event:category3});
          }}
        >
        {this.category3}
        </Picker>
      </View>
    </View>
    );
  }
}



var styles = StyleSheet.create({
  otherContainer:{
    paddingTop:10,
  },
  textContainer:{
    flexDirection:"row",
    justifyContent:"space-between",
    borderBottomWidth:1/2,
    borderColor:"#ddd9d9",
    paddingBottom:10,
  },
  caseContent:{
    width:width*0.9,
    padding:10,
    elevation:3,

  },
  caseHeader:{
    flexDirection:"row",
    justifyContent:"space-between",
    alignItems:"center",
    width:width*0.9,
    paddingLeft:10,
    borderBottomWidth:1/2,
    borderColor:"#ddd9d9"


  },
  picker:{
    flex:1,
  },
  pickerContainer:{
    flex:1,
    height:height*0.05,
    borderWidth:1/2,
    paddingTop:-10,

    marginLeft:5,
    borderColor:"#000",
  },
  selectionBar:{
    flex:1,
    flexDirection:'row',
    alignItems:'center',
    paddingTop:5,
  },
  questionCotainer:{
    width:width*0.8,
    height:height*0.08,
    flexDirection:'row',
    alignItems:'center',
  },
  closeModal:{
    height:26,
    width:26,
    borderRadius:13,
    justifyContent:'center',
    alignItems:"center",
    borderColor:"#dadada",
    borderWidth:1,
  },
  modalTitleContainer:{
    height:50,
    borderBottomWidth:1,
    borderColor:"#dadada",
    width:width*0.9,
    justifyContent:"space-between",
    alignItems:"center",
    flexDirection:"row",
    paddingLeft:15,
    paddingRight:15,

  },
  comfirmButtonContainer:{
    paddingBottom:10,
    width:width*0.7,
  },
  descriptionEntry:{
    width:width*0.9,
    height:width*0.3,
    textAlignVertical:"top",
    padding:10,
  },
  descriptionEntryContainer:{
    height:width*0.3,
    paddingTop:10,
    marginBottom:5,
    backgroundColor:"#fff",
  },
  addPhoto:{
    height:height*0.4,
    width:width*0.9,
    alignItems:'center',
    justifyContent:'center',
  },
  addModalContainer:{
    height:height*0.8,
    backgroundColor:"#fff",
  },
  addModalRootContainer:{
    flex:1,
    justifyContent:"center",
    alignItems:'center',
    backgroundColor:"rgba(0, 0, 0, 0.5)"
  },
  deleteButton:{
    height:25,
    width:25,
    backgroundColor:"red",
    justifyContent:'center',
    alignItems:"center",
  },
  caseTextContainer:{
    padding:5,
    paddingTop:0,
    flex:1,
    width:width*0.9
  },
  caseImg:{
    height:width*0.4,
    width:width*0.9,
  },
  caseRootContainer:{
    marginTop:20,
    width:width*0.9,
    alignItems:"center",
    backgroundColor:"#fff",
    elevation:3,
    justifyContent:"center",

  },
  caseContainer:{
    backgroundColor:"#fff",
    justifyContent:"center",
    alignItems:"center",
    height:width*0.6,
    width:width*0.9,
    borderWidth:1/2,
    borderColor:"#c1c1c1",



  },
  addButton:{
    position:"absolute",
    bottom:20,
    right:20,
    elevation:3,
  },
  rootContainer:{
    flex:1,
    width:width,
    marginTop:55,
    backgroundColor:"#e0e0e0",
    alignItems:"center",
    justifyContent:'center'

  }
});
