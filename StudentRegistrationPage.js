'use strict'

import  React, {Component} from 'react';
import{
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Navigator,
  Image,
  TouchableOpacity,
  Dimensions,
  TextInput,
  Picker,
  ScrollView,
  InteractionManager,
  Alert

} from 'react-native';


var {height, width} = Dimensions.get('window');
const height = height-55;
import Icon from 'react-native-vector-icons/FontAwesome';
import districtData from './getDistrict.js';
var styles = StyleSheet.create({
  container: {
    flex: 1,
    top:55,
  },
  fieldContainer:{
    flex:1,
    width:width,
    alignItems:'center'
  },
  nextPageContainer:{
    flex:1,
    width:width,
    padding:10,
    alignItems:"flex-end",
    justifyContent:"flex-end",
    paddingBottom:140,


  },
  questionContainer:{
    height:height*0.08,
    width:width*0.8,
    flexDirection:'row',
    alignItems:'center',
    margin:10,
    marginBottom:0,
  },
  textStyle:{
    fontSize:15,
  },
  textInputStyle:{
    flex:1,
  },
  picker:{
    flex:1,
  },
  pickerContainer:{
    flex:1,
    borderWidth:1/2,
    paddingTop:-10,
    borderRadius:5,
    marginLeft:5,
    borderColor:"#000",
  },
  selectionBar:{
    flex:1,
    flexDirection:'row',
    alignItems:'center',
    paddingTop:5,
  },

})

var getCategory = [];
var getDistrict = districtData;


var toQueryString = (obj)=> {
    return obj ? Object.keys(obj).sort().map(function (key) {
        var val = obj[key];
        if (Array.isArray(val)) {
            return val.sort().map(function (val2) {
                return key + '=' + val2;
            }).join('&');
        }
        return key + '=' + val;
    }).join('&') : '';
}

export default class StudentRegistrationPage extends Component{
    constructor(props){
      super(props);
      this.state ={
        chinese_full_name:"",
        eng_full_name:"",
        gender:"M",
        age:"",
        isDataLoaded:false,
        category:-1,
        district:-1,
      };
    }
    checkInput = () => {
      //console.log(this.state.avaDistrict);
      if(!this.state.chinese_full_name){
        Alert.alert("錯誤","請輸入中文全名。");
        return;
      }
      else if(!this.state.eng_full_name){
        Alert.alert("錯誤","請輸入英文全名。");
        return;
      }
      else if(!this.state.age){
        Alert.alert("錯誤","請輸入年齡。");
        return;
      }
      else if(this.state.district.length < 1){
        Alert.alert("錯誤","請選擇地區。");
        return;
      }
      else if(this.state.category<0){
        Alert.alert("錯誤","請選擇興趣。");
        return;
      }

      var pattern = /^\d{1,3}$/;
      var pattern2 = /^\d{1,5}$/;
      if(!pattern.test(this.state.age)){
        Alert.alert("錯誤","年齡只能輸入數字。");
        return;
      }

      this.sendRegisterRequest();
    }
    sendRegisterRequest = () => {
      var request = new XMLHttpRequest();
      request.onreadystatechange = (e) =>{
        if (request.readyState !== 4) {
          return;
        }
        if(request.status === 200){
          var result = request.responseText;
          if(result.includes("success")){
            Alert.alert("正確","成功填寫個人資料。");
            var logout = new XMLHttpRequest();
            logout.open('POST',"http://54.179.154.17/logout.php",true);
            logout.send();
            const currentRouteStack = this.props.p.refs.navigator.getCurrentRoutes();
            const stack = currentRouteStack[0].stack;
            stack.pop();
            this.props.p.gotoPage({title:"MainPage",navBarTitle:"主頁"},true);
          }else{
            Alert.alert("錯誤",result);
          }
        }
      }
      request.open('POST', 'http://54.179.154.17/registerStudent.php', true);
      request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      var param = toQueryString(this.state);
      //console.log(param);
      request.send(param);

    }
    componentDidMount(){
      InteractionManager.runAfterInteractions(() => {
          var request = new XMLHttpRequest();
          var result = [];
          request.onreadystatechange = (e) =>{
          ////console.log("ssfs");
            if (request.readyState !== 4) {
              return;
            }
            if(request.status === 200){
              result.push.apply(result,JSON.parse(request.responseText));
              getCategory = result;
              ////console.log( result);
              this.setState({isDataLoaded:true});
            }
          }
          request.open('POST', 'http://54.179.154.17/getCategory.php', true);
          request.send();
      });
    }
    render(){
        return(
            !this.state.isDataLoaded ?
            <View style={styles.container}>
                <Image
                  style={styles.fieldContainer}
                  resizeMode="cover"
                  source={require('./img/StudentRegistrationPage/background.png')}
                  >
                  <View style={styles.questionContainer}>
                    <Text
                      style={styles.textStyle}
                    >中文全名:</Text>
                    <TextInput
                      ref="chinese_full_name_input"
                      returnKeyType="next"
                      value={this.state.chinese_full_name}
                      onChangeText={(chinese_full_name)=>this.setState({chinese_full_name})}
                      style={styles.textInputStyle}
                      onSubmitEditing={
                        () => this.refs.eng_full_name_input.focus()
                      }
                      />
                  </View>

                  <View style={styles.questionContainer}>
                    <Text
                      style={styles.textStyle}
                    >英文全名:</Text>
                    <TextInput
                      ref="eng_full_name_input"
                      returnKeyType="next"
                      value={this.state.eng_full_name}
                      onChangeText={(eng_full_name)=>this.setState({eng_full_name})}
                      style={styles.textInputStyle}
                      onSubmitEditing={
                        () => this.refs.ageInput.focus()
                      }
                      />
                  </View>
                  <View style={styles.questionContainer}>
                    <Text
                      style={styles.textStyle}>姓別 :</Text>
                    <Picker
                      ref="gender"
                        style={{width:width*0.18}}
                        mode="dropdown"
                        onValueChange={(value)=> this.setState({gender:value})}
                        selectedValue={this.state.gender}
                      >
                      <Picker.Item label="男" value="M" />
                      <Picker.Item label='女' value="F" />
                      </Picker>
                  </View>


                  <View style={styles.questionContainer}>
                    <Text
                      style={styles.textStyle}
                    >年齡:</Text>
                    <TextInput
                      keyboardType="numeric"
                      ref="ageInput"
                      returnKeyType="next"
                      value={this.state.age}
                      onChangeText={(age)=>this.setState({age})}
                      style={styles.textInputStyle}
                      maxLength={3}

                    />
                  </View>

                    <Button
                      p={this}
                    />
              </Image>
            </View>
          :
          <View style={styles.container}>
              <Image
                style={styles.fieldContainer}
                resizeMode="cover"
                source={require('./img/StudentRegistrationPage/background.png')}
                >
                <View style={styles.questionContainer}>
                  <Text
                    style={styles.textStyle}
                  >中文全名:</Text>
                  <TextInput
                    ref="chinese_full_name_input"
                    returnKeyType="next"
                    value={this.state.chinese_full_name}
                    onChangeText={(chinese_full_name)=>this.setState({chinese_full_name})}
                    style={styles.textInputStyle}
                    onSubmitEditing={
                      () => this.refs.eng_full_name_input.focus()
                    }
                    />
                </View>

                <View style={styles.questionContainer}>
                  <Text
                    style={styles.textStyle}
                  >英文全名:</Text>
                  <TextInput
                    ref="eng_full_name_input"
                    returnKeyType="next"
                    value={this.state.eng_full_name}
                    onChangeText={(eng_full_name)=>this.setState({eng_full_name})}
                    style={styles.textInputStyle}
                    onSubmitEditing={
                      () => this.refs.ageInput.focus()
                    }
                    />
                </View>
                <View style={styles.questionContainer}>
                  <Text
                    style={styles.textStyle}>姓別 :</Text>
                  <Picker
                    ref="gender"
                      style={{width:width*0.18}}
                      mode="dropdown"
                      onValueChange={(value)=> this.setState({gender:value})}
                      selectedValue={this.state.gender}
                    >
                    <Picker.Item label="男" value="M" />
                    <Picker.Item label='女' value="F" />
                    </Picker>
                </View>


                <View style={styles.questionContainer}>
                  <Text
                    style={styles.textStyle}
                  >年齡:</Text>
                  <TextInput
                    keyboardType="numeric"
                    ref="ageInput"
                    returnKeyType="next"
                    value={this.state.age}
                    onChangeText={(age)=>this.setState({age})}
                    style={styles.textInputStyle}
                    maxLength={3}

                  />
                </View>
                <View style={[styles.questionContainer,{marginBottom:0}]}>
                  <Text
                    style={styles.textStyle}
                  >地區:</Text>
                </View>
                <View style={[styles.questionContainer,{marginTop:0}]}>
                  <District
                    p={this}
                  />
                </View>
                <View style={[styles.questionContainer,{marginBottom:0}]}>
                  <Text
                    style={styles.textStyle}
                  >興趣:</Text>
                </View>
                <View style={[styles.questionContainer,{marginTop:0}]}>
                  <Category
                    p={this}
                  />
                </View>

                  <Button
                    p={this}
                  />
            </Image>
          </View>

        )
    }
}


class Button extends Component{
      constructor(props){
        super(props);
      }

     /*handlePress =() =>{
       if(this.state.enabled){
         this.props.p.refs.pictureViwer.back();
       }
     };*/

     render(){
       return(
         <View style={styles.nextPageContainer}>
           <Icon.Button
            onPress={ () => this.props.p.checkInput()}
            name="send"
            color="#fff"
            size={25}
            style={{justifyContent:"center"}}
           >
           完成
           </Icon.Button>
         </View>

       )
     }
}

class Category extends Component{
  constructor(props){
    super(props);
    this.state={
      category1:-1,
      category2:-1,
      category3:-1,
    }
    this.category1=[];
    this.category2=[];
    this.category3=[];
    this.category1.push(<Picker.Item key={0} label="大分類" value={-1} />);
    this.category2.push(<Picker.Item key={0} label="小分類" value={-1} />);
    this.category3.push(<Picker.Item key={0} label="項目" value={-1} />);

    this.category1.push.apply(this.category1,this.getCategoryElement(0,getCategory.filter((item) => {
      return item.parent_id == 0;
    })));

    //console.log(getCategory);

  }

  getCategoryElement = (pid,items) =>{
    var itemsView = [];
    if(pid == -1)
      return itemsView;
    for(var i = 0;i < items.length;i++){
      itemsView.push(<Picker.Item label={items[i]['name']} value={items[i]['category_id']} key={`${pid}`+`${i}`}/>);
    }
    return itemsView;
  }

  render(){
    this.category1=[];
    this.category1.push(<Picker.Item key={0} label="大分類" value={-1} />);
    this.category1.push.apply(this.category1,this.getCategoryElement(0,getCategory.filter((item) => {
      return item.parent_id == 0;
    })));
    //console.log(getCategory);
    return(

      <View style={styles.selectionBar}>
      <View style={styles.pickerContainer}>
        <Picker
          style={styles.picker}
          selectedValue={this.state.category1}
          mode="dropdown"
          onValueChange={(category1) => {
            this.category2.length = 0;
            this.category2.push(<Picker.Item key={0} label="小分類" value={-1} />);

            this.category2.push.apply(this.category2,this.getCategoryElement(category1,getCategory.filter((item) => {
              return item.parent_id == category1;
            })));
            this.setState({category1});
          }}
        >
          {this.category1}
        </Picker>
      </View>

      <View style={styles.pickerContainer}>
        <Picker
          style={styles.picker}
          selectedValue={this.state.category2}
          mode="dropdown"
          onValueChange={(category2) => {
            this.category3.length = 0;
            this.category3.push(<Picker.Item key={0} label="項目" value={-1} />);
            this.category3.push.apply(this.category3,this.getCategoryElement(category2,getCategory.filter((item) => {
              return item.parent_id == category2;
            })));
            this.setState({category2});
          }}
        >
          {this.category2}
        </Picker>
      </View>
      <View style={styles.pickerContainer}>
        <Picker
          style={styles.picker}
          selectedValue={this.state.category3}
          mode="dropdown"
          onValueChange={(category3) => {
            this.setState({category3});
            this.props.p.setState({category:category3});
          }}
        >
        {this.category3}
        </Picker>
      </View>
    </View>
    );
  }
}

class District extends Component{
  constructor(props){
    super(props)
    this.state={
      district1:-1,
      district2:-2
    }
    this.district1=[];
    this.district2=[];
    this.district1.push(<Picker.Item key={0} label="地區" value={-1} />);
    this.district2.push(<Picker.Item key={0} label="分區" value={-1} />);
  }

  getDistrictElement = (pid,items) =>{
    var itemsView = [];
    if(pid == -1)
      return itemsView;
    for(var i = 0;i < items.length;i++){
      itemsView.push(<Picker.Item label={items[i]['name']} value={items[i]['district_id']} key={i}/>);
    }
    return itemsView;
  }

  render(){
    this.district1=[];
    this.district1.push(<Picker.Item key={0} label="地區" value={-1} />);
    this.district1.push.apply(this.district1,this.getDistrictElement(0,getDistrict.filter((item) => {
      return item.parent_id == 0;
    })));
    return(
      <View style={styles.selectionBar}>
        <View style={styles.pickerContainer}>
          <Picker
            style={styles.picker}
            selectedValue={this.state.district1}
            mode="dropdown"
            onValueChange={(district1) => {
              this.district2.length = 0;
              this.district2.push(<Picker.Item key={0} label="分區" value={-1} />);
              this.district2.push.apply(this.district2,this.getDistrictElement(district1,getDistrict.filter((item) => {
                return item.parent_id == district1;
              })));
              this.setState({district1});
            }}
          >
            {this.district1}

          </Picker>
        </View>

        <View style={styles.pickerContainer}>
          <Picker
            style={styles.picker}
            selectedValue={this.state.district2}
            mode="dropdown"
            onValueChange={(district2) => {
              this.setState({district2});
              this.props.p.setState({district:district2});
            }}
          >
            {this.district2}
          </Picker>
        </View>
      </View>
    );
  }
}
