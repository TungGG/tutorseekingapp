'use strict'

import  React, {Component} from 'react';
import{
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Navigator,
  Image,
  TouchableOpacity,
  Dimensions,
  TextInput,
  Alert,
  ScrollView,
} from 'react-native';


var {height, width} = Dimensions.get('window');
const height = height-55;

var styles = StyleSheet.create({
  container: {
    flex: 1,
    top:55,
    backgroundColor:"#ffffff"
  },
  subTitle:{
    fontSize:30,
    fontWeight:"600",
    padding:5,
  },
  subContent:{
    fontSize:15,
    fontWeight:"300",
    lineHeight: 22,
    padding:8,
  }


})

import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/Entypo';

export default class AboutUsPage extends Component{
    constructor(props){
      super(props);
      this.state = {
    };
    }


    render(){
        return(
          <View style={styles.container}>
            <ScrollView >
            <Text style={styles.subTitle}>背景：</Text>
            <Text style={styles.subContent}>  香港是一個繁忙的都市，急速的生活節奏令香港人感到壓力。
            在閒時不少人希望上一些興趣班，放鬆身心、舒緩壓力。
            有或者不少的朋友都想找私人的教練或導師、作一對一指導，在技巧更上一層樓。
            現時尋找私人教練和導師主要途經是透過朋友介紹或在網上找尋有網頁的教練，
            這樣令學生能選擇的導師大大減少，而且導師質素更良莠不齊，
            為了令資訊更透明，方便學生與教練互相配對，於是我們便推出了教練樂園應用程式。
            </Text>
            <Text style={styles.subTitle}>特點:</Text>
            <Text style={styles.subContent}>  我們有空間讓教練分享他的教學成就和教學特式，
            讓學生可以透過圖片和文字了解導師的個性和教學特點。而且我們有學生評分系統，有助學生更了解導師。
            
            </Text>
            </ScrollView>

          </View>

        )
    }
}
